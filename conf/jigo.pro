#
# This configuration file is used when obfuscating JiGo software.
#

# Specify the library jars, input jars, and output jar.
#
-libraryjars <java.home>/lib/rt.jar

# Preserve all public applets, and print out which ones.
#
-keep public class * extends java.applet.Applet

-keepclasseswithmembers public class * {
    public static void main(java.lang.String[]);
  }

-defaultpackage jigo

# Print out a list of what we're preserving.
#
-printseeds

