#################################################################
#
# Author : Dave Jarvis
# Date   : April 15, 2001
# Version: 1.0
#
#################################################################
#
# Given a directory full of SGF/MGT files, this script examines each one
# and stores the following information in a single file:
#
# SGF file name, White Name, White Rank, Black Name, Black Rank, SGF Result
#
# Example index file:
#
# 1.0
# games/sakata-rin1.sgf,Sakata Eio,9 dan,Rin Kaiho,9 dan,B+0.5,
# games/sakata-rin2.sgf,Sakata Eio,9 dan,Rin Kaiho,9 dan,W+R,
# games/sakata-rin3.sgf,Rin Kaiho,9 dan,Sakata Eio,9 dan,W+2.5,
# ...
#
#################################################################

use File::Copy;

# Flush all buffers, so no information is lost.
#
$| = 1;

# Get a list of all the text file names in the $GAMES_DIR directory.
#
opendir DH, ".";
@files = grep -T, map "$_", readdir DH;
closedir DH;

# For each file, parse its contents, then write its player information
# to the index file.
#
foreach $fileName( @files )
{
  @data = parse( $fileName );

  # Substitute spaces for underscores and create the directory.
  #
  $dirName = $data[0];
  $dirName =~ tr/ /_/;

  mkdir "$GAMES_DIR/$dirName";
  copy( "$fileName", "$dirName/$fileName" );
}

# Done.
#
1;

# Opens the given file, parses the contents, then returns an array
# with the information previously documented, not including the file name.
# It isn't the fastest algorithm, however it needs only be run once in
# a while, and it works well.
#
sub parse
{
  my $fileName = shift( @_ );
  my $whiteName = "Unknown";
  my $blackName = "Unknown";
  my $whiteRank = "?";
  my $blackRank = "?";
  my $gameResult = "?";

  open FH, "$fileName";

  while( <FH> )
  {
    @elements = split /]/;

    # Figure out the type of SGF token we've found.
    #
    foreach $item( @elements )
    {
      # Presumes all files are in SGF file format 3 (or greater).
      #
      $wName = index $item, "PW";
      $bName = index $item, "PB";
      $wRank = index $item, "WR";
      $bRank = index $item, "BR";
      $result = index $item, "RE";

      if( $result != -1 )
      {
        @info = split /\[/, $item;
        $gameResult = $info[1];
        $gameResult =~ s/^\s*(.*?)\s*$/$1/;  # Trim whitespace
      }

      if( $wName != -1 )
      {
        @info = split /\[/, $item;
        $whiteName = $info[1];
        $whiteName =~ s/^\s*(.*?)\s*$/$1/;  # Trim whitespace
      }

      if( $bName != -1 )
      {
        @info = split /\[/, $item;
        $blackName = $info[1];
        $blackName =~ s/^\s*(.*?)\s*$/$1/;  # Trim whitespace
      }

      if( $wRank != -1 )
      {
        @info = split /\[/, $item;
        $whiteRank = $info[1];
        $whiteRank =~ s/^\s*(.*?)\s*$/$1/;  # Trim whitespace
      }

      if( $bRank != -1 )
      {
        @info = split /\[/, $item;
        $blackRank = $info[1];
        $blackRank =~ s/^\s*(.*?)\s*$/$1/;  # Trim whitespace
      }
    }
  }

  close FH;

  return ($whiteName, $whiteRank, $blackName, $blackRank, $gameResult);
}

