#!/usr/bin/perl

#
# The Unix 'faucet' program runs as a daemon in the background, awaiting
# requests to the specified port.  When a connection occurs, this Perl script
# is invoked.  The location of Perl may vary (find using "which perl").
#
# The protocol is simple:
#   1) Read a line; this is the file name.
#   2) Read file content, save it as the given name.
#   3) Close the file.
#
# To run:
#   faucet 4242 --daemon --out --in ./snapshot.pl
#

# Read the buffer in chunks.
$BUFFER_SIZE = 16384;

# Flush buffers.
$| = 1;

open FROM_APPLET, "-";

# Read the name of the file (everything up to the first newline).
while( read( FROM_APPLET, $buffer, 1 ) )
{
  if( $buffer eq "\n" ) { last; }

  $name = $name . $buffer;
}

open SAVE_IMAGE, ">screens/$name\.png";

while( read( FROM_APPLET, $buffer, $BUFFER_SIZE ) )
{
  print SAVE_IMAGE $buffer;
}

# C'est tout!
close FROM_APPLET;
close SAVE_IMAGE;

1;

