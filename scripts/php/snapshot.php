<?PHP
  /*
   * The Unix 'faucet' program runs as a daemon in the background, awaiting
   * requests to the verify port.  When a connection occurs, this PHP script
   * is invoked.
   *
   * The protocol is simple:
   *   1) Read a line; this is the file name.
   *   2) Read file content, save it as the given name.
   *   3) Close the file.
   *
   * Run using:
   *   faucet 4242 --daemon --out --in php snapshot.php
   */
  $fromApplet = @fopen( "php://stdin", "r" );

  // Read the name of the file.
  //
  $fileName = fscanf( $fromApplet, "%s\n" );

  $name = $fileName[0];

  $date = strftime( "%d%b%Y-%T" );

  $saveImage = @fopen( "screens/$name.png", "w" );

  do
  {
    // Read the file in chunks of 8192 bytes.
    //
    $contents = fread( $fromApplet, 8192 );
    fwrite( $saveImage, $contents );
  }
  while( strlen( $contents ) > 0 );

  // C'est tout!
  //
  @fclose( $fromApplet );
  @fclose( $saveImage );
?>

