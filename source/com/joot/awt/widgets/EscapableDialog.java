/*
 * Copyright (C) 2005 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.awt.widgets;

import java.awt.*;
import java.awt.event.*;

public class EscapableDialog extends Dialog
  implements ContainerListener, KeyListener
{
  private static final boolean ADD = true;
  private static final boolean REMOVE = false;

  public EscapableDialog( Frame owner, String title, boolean modal )
  {
    super( owner, title, modal );

    addContainerListeners( this );
  }

  /**
   * Called when a container is added to this dialog.
   */
  public void componentAdded( ContainerEvent event )
  {
    addContainerListeners( event.getChild() );
  }

  /**
   * Called when a container is removed from this dialog.
   */
  public void componentRemoved( ContainerEvent event )
  {
    removeContainerListeners( event.getChild() );
  }

  /**
   * Called when any component of this Dialog gets a KEY_PRESSED event.
   */
  public void keyPressed( KeyEvent event )
  {
    switch( event.getKeyCode() )
    {
      case KeyEvent.VK_ESCAPE:
        performCancelAction();
        break;

      case KeyEvent.VK_ENTER:
        performAcceptAction();
        break;
    }
  }

  /**
   * Subclasses should override this method to add behaviour when the cancel
   * action is desired.
   */
  public void performCancelAction()
  {
    performDefaultAction();
  }

  /**
   * Subclasses should override this method to add behaviour when the accept
   * action is desired.
   */
  public void performAcceptAction()
  {
    performDefaultAction();
  }

  /**
   * Modal dialogs tend to do only one thing when you press ESC or ENTER,
   * and that is to close themselves. This is used by both performCancelAction
   * and performAcceptAction.
   */
  protected void performDefaultAction()
  {
    setVisible( false );
  }

  /**
   * KeyListener requirement.
   */
  public void keyReleased( KeyEvent event )
  {
  }

  /**
   * KeyListener requirement.
   */
  public void keyTyped( KeyEvent event )
  {
  }

  /**
   * Adds this class as a listener to all the containers that make up the
   * given component.
   */
  private void addContainerListeners( Component component )
  {
    updateContainerListeners( component, ADD );
  }

  /**
   * Removes this class as a listener from all the containers that make up the
   * given component.
   */
  private void removeContainerListeners( Component component )
  {
    updateContainerListeners( component, REMOVE );
  }

  /**
   * Helper method for addContainerListeners and removeContainerListeners.
   *
   * @param component - The component to adjust listeners.
   * @param update - true (or ADD) means to add this dialog as a listener.
   * @param update - false (or ADD) means to remove this dialog as a listener.
   */
  private void updateContainerListeners( Component component, boolean update )
  {
    component.removeKeyListener( this );

    if( update == ADD )
    {
      component.addKeyListener( this );
    }

    if( component instanceof Container )
    {
      Container container = (Container)component;

      container.removeContainerListener( this );

      if( update == ADD )
      {
        container.addContainerListener( this );
      }

      Component[] children = container.getComponents();

      for( int i = children.length - 1; i >= 0; i-- )
        updateContainerListeners( children[ i ], update );
    }
  }
}

