/*
 * Copyright (C) 2003 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.awt.widgets;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

/**
 * The ImageComponent class wraps an Image so it can be placed within
 * a GUI.
 */
public class ImageComponent extends Component
{
  private Image image;
  private Image offscreen;

  /**
   * Constructs an image container with specified image.  Don't pass in a null
   * image.
   *
   * @param image - The graphic you want to display.
   */
  public ImageComponent( Image image )
  {
    setImage( image );
  }

  public ImageComponent( Image image, int width, int height )
  {
    this( image.getScaledInstance( width, height, Image.SCALE_SMOOTH ) );
  }

  /**
   * Paints the component. The "paint" method should simply call update in
   * order to reduce flicker.  Most people get this wrong by having update
   * call paint.
   */
  public void update( Graphics graphics )
  {
    // Blammo!  Very fast.
    //
    if( graphics != null )
    {
      graphics.drawImage( getOffscreen(), 0, 0, null );
    }
  }

  /**
   * Called to update the offscreen image.
   */
  public void redraw()
  {
    getOffscreen().getGraphics().drawImage( getImage(), 0, 0, this );
  }

  /**
   * Simply calls update to refresh the dirty graphics context.
   */
  public void paint( Graphics graphics )
  {
    update( graphics  );
  }

  public Dimension getSize()
  {
    return new Dimension(
      getImage().getWidth( this ), getImage().getHeight( this ) );
  }

  public Dimension getPreferredSize() { return getSize(); }
  public Dimension getMinimumSize() { return getSize(); }

  /**
   * Create an offscreen image upon which we can draw pictures.
   */
  protected Image createOffscreen()
  {
    return createImage( getWidth(), getHeight() );
  }

  protected Image getOffscreen()
  {
    if( this.offscreen == null )
    {
      // The first time we get the offscreen, make sure that we have something
      // there!
      //
      this.offscreen = createOffscreen();
      redraw();
    }
    
    return this.offscreen;
  }

  /**
   * Returns the offscreen image upon which an image will be drawn.
   */
  private void setOffscreen( Image offscreen )
  {
    this.offscreen = offscreen;
  }

  protected Image getImage() { return this.image; }

  protected void setImage( Image image )
  {
    this.image = image;
  }
}

