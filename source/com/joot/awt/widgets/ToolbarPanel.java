/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.awt.widgets;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.Rectangle;

import java.awt.event.ActionListener;

/**
 * A frame for holding a toolbar. A toolbar consists of a whack of
 * ImageButtons (and/or ToggleImageButtons); just keep adding them and
 * they'll line themselves up. Eventually this should take an alignment
 * (horizontal vs. vertical) parameter, as well as being able to set the
 * amount of spacing between each button that gets added.
 */
public class ToolbarPanel extends Panel
{
	public ToolbarPanel()
	{
		// Create a new GridLayout with one row and any number of columns.
		//
		GridLayout gl = new GridLayout( 1, 0 );

		gl.setVgap( 4 );
		gl.setHgap( 4 );

		setLayout( gl );
		super.setSize( 0, 0 );
	}

	public Component add( Component toAdd )
	{
		Component component = super.add( toAdd );

		Dimension addSize = toAdd.getSize(),
							currSize = this.getSize();

		currSize.height = Math.max( currSize.height, addSize.height + 2 );
		currSize.width += (addSize.width + 6);

		setSize( currSize.width, currSize.height );

		return component;
	}

	public Component addButton( Component toAdd )
	{
		return add( toAdd );
	}

	public void addActionListener( ActionListener al )
	{
		Component components[] = this.getComponents();
	
		for( int i = components.length - 1; i >= 0; i-- )
			if( components[i] instanceof ImageButton )
				((ImageButton)components[i]).addActionListener( al );
	}
}

