/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.awt.util;

import java.awt.AWTEvent;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.StringTokenizer;

import com.joot.awt.widgets.EscapableDialog;

/**
 * A simple dialog that displays a single multi-line message and an OK button.
 */
public class AlertBox extends EscapableDialog
{
	private Button okButton;
  private String message = "";

  /**
   * Creates a new alertbox based on a given frame and title. This will
   * not display util setVisible is called.
   */
	public AlertBox( Frame frame, String title, String text )
	{
		super( frame, title, true );
    setPosition( frame );
    setMessage( text );
		init();
	}

  private void init()
  {
    String message = getMessage();
    StringTokenizer st = new StringTokenizer( message, "\n" );

    Panel buttonPanel = new Panel();
    Panel messagePanel = new Panel( new GridLayout( st.countTokens(), 1 ) );

    buttonPanel.setLayout( new FlowLayout( FlowLayout.CENTER ) );
    buttonPanel.add( getOKButton() );

    // Add more lines to the panel.
    //
    while( st.hasMoreTokens() )
    {
      messagePanel.add( new Label( st.nextToken() ) );
    }

    add( "Center", messagePanel );
    add( "South", buttonPanel );

    enableEvents( Event.WINDOW_DESTROY );
    setResizable( false );
    setModal( true );
    pack();
  }

  protected void setPosition( Frame parent )
  {
    Point p = parent.getLocation();
    Dimension dim = parent.getSize();

    setLocation( p.x + dim.width / 2, p.y + dim.height / 2 );
  }

  /**
   * Processing the event only handles what happens when the window is
   * requested to be closed.
   */
  public void processEvent( AWTEvent event )
  {
    if( event.getID() == Event.WINDOW_DESTROY ) {
      close();
    }
  }

  /**
   * Clears all resources used by this alert box from memory.
   */
  protected void close() {
    setVisible( false );
    dispose();
  }

  protected String getOKButtonText()
  {
    return "OK";
  }

  protected Button createOKButton()
  {
    Button button = new Button( getOKButtonText() );

    button.addActionListener(
      new ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          close();
        }
      }
    );

    return button;
  }

  private void setMessage( String message )
  {
    this.message = message;
  }

  private String getMessage()
  {
    return this.message == null ? "" : this.message;
  }

  private Button getOKButton()
  {
    if( this.okButton == null )
      this.okButton = createOKButton();

    return this.okButton;
  }
}

