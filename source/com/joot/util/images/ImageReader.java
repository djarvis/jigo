/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.util.images;

import java.awt.Component;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;

import java.io.IOException;

import java.net.URL;
import java.net.MalformedURLException;

/**
 * A class that allows java.awt.Image instances to be created given a web
 * address.
 * <P>
 * After ImageReader has been instantiated once, any number of calls can
 * be made to the static loadImage methods. You must create an instance
 * of this class before reading an image!
 */
public final class ImageReader
{
	// Initialized upon creation of a new ImageReader.
	//
	private static MediaTracker TRACKER;

	/**
	 * Creates a new reader of images; this must be called once before trying
	 * to get an image.
	 */
	public ImageReader( Component parent )
	{
		// The MediaTracker must have a valid parent.
		//
		TRACKER = new MediaTracker( parent );
	}

	/**
	 * Returns the image (JPG/GIF) specified at the given URL address.
	 */
	public final static Image get( String urlAddress )
		throws IOException, MalformedURLException, InterruptedException
	{
		return get( new URL( urlAddress ) );
	}

	/**
	 * Returns the image (JPG/GIF) specified at the given URL.
	 */
	public final static Image get( URL address )
		throws IOException, InterruptedException
	{
		Image image = Toolkit.getDefaultToolkit().getImage( address );

		TRACKER.addImage( image, 0 );
		TRACKER.waitForID( 0 );
		return TRACKER.statusID( 0, true ) != TRACKER.ERRORED ? image : null;
	}
}

