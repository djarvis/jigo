/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.util.images;

import java.awt.Image;

import java.io.IOException;
import java.io.OutputStream;

import com.joot.util.encoder.PNGEncoder;

/**
 * A class that allows java.awt.Image instances to be saved as files on
 * a server, provided an Image, filename, and OutputStream.  The image
 * is saved in PNG format.
 *
 * <P>
 * This should be made a bit more generic, in that it should allow any
 * stream of bytes to be saved to a file on the server.
 *
 * <P>
 * A daemon which understands the protocol must be listening on the server
 * for files.  The protocol follows:
 * <PRE>
 *   1. Write "filename\n"
 *   2. Write binary file content
 *   3. Flush and close stream
 * </PRE>
 *
 * <P>
 * All this happens in its own thread so the application sees no delay.
 * Eventually it would be nice to add a listener hook so that the application
 * can be notified on successful/erroneous saves.
 */
public final class ImageWriter implements Runnable
{
  private Image myImage;
  private String myFileName;
  private OutputStream myOutputStream;

  private ImageWriter( Image image, String fileName, OutputStream out )
  {
    setImage( image );
    setFileName( fileName + '\n' );
    setOutputStream( out );
  }

  /**
   * Since the ImageWriter works in its own thread, it needs a run method.
	 * This method will write out the image passed into the static writeImage
	 * method.
   */
  public void run()
  {
    try
    {
      // Create a new PNG encoder with a compression level of 6.
      //
      PNGEncoder encoder = new PNGEncoder( getImage(), true, 0, 6 );

      byte imageBytes[] = encoder.pngEncode();

      if( imageBytes != null )
      {
        getOutputStream().write( getFileName().getBytes() );
        getOutputStream().write( imageBytes );
      }

      getOutputStream().close();
    }
    catch( Exception e )
		{
			e.printStackTrace();
		}
  }

  /**
   * Writes an image to a stream in PNG format, in a separate thread.
   *
   * @param image - The image to write to the stream in PNG format.
   * @param fileName - The name under which the image will be written.
   * @param out - Where to write the image.
   */
  public static void writeImage(
    Image image, String fileName, OutputStream out )
  {
    (new Thread( new ImageWriter( image, fileName, out ) )).start();
  }

  private void setImage( Image image ) { myImage = image; }
  private Image getImage() { return myImage; }

  private void setFileName( String fn ) { myFileName = fn; }
  private String getFileName() { return myFileName; }

  private void setOutputStream( OutputStream out ) { myOutputStream = out; }
  private OutputStream getOutputStream() { return myOutputStream; }
}

