/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

/**
 * Information about a Stone (such as width, height, image). You'll notice
 * a Stone doesn't have a location on a Goban. Only the Goban knows where
 * its Stones are located. (This isn't entirely true; any object that
 * observes the Goban for stone placements can keep track of stone locations
 * itself.)
 */
public abstract class RenderedStone extends Stone
{
	/**
	 * Creates a new stone of default width and height.
	 */
	public RenderedStone() { }

	/**
	 * Creates a stone with a given width and height (in pixels), but no
	 * associated image. It's best to give even values for the width and
	 * height of the stones, with the height being about 2 pixels larger
	 * than the width (so as to avoid a square shape).
	 */
	public RenderedStone( int width, int height )
	{
    super( width, height );
	}

	/**
	 * Used to draw the stone at a given location on the graphics context.
	 * Notice that the stone does not centre itself about this point; the
	 * Goban adjusts accordingly.
	 *
	 * @param graphics - The graphics context on which to draw.
	 * @param x - The x location to draw the stone.
	 * @param y - The y location to draw the stone.
	 */
	public void draw( Graphics graphics, int x, int y )
	{
	}
}

