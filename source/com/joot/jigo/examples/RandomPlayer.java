/*
 * Copyright (C) 2002 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.examples;

import java.awt.Frame;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import com.joot.awt.util.AlertBox;

import com.joot.jigo.BlackStone;
import com.joot.jigo.Goban;
import com.joot.jigo.GobanHighlighter;
import com.joot.jigo.JiGoApplet;
import com.joot.jigo.Markup;
import com.joot.jigo.Rules;
import com.joot.jigo.Stone;
import com.joot.jigo.Point;
import com.joot.jigo.WhiteStone;

/**
 * A very simple Go opponent -- plays random, but legal, moves.
 */
public class RandomPlayer extends JiGoApplet implements MouseListener
{
  private static final int GOBAN_SIZE = 9;

	private Goban goban;
	private Rules rules;

	public void init()
	{
		setGoban( new Goban( GOBAN_SIZE, getWhiteStone() ) );

 		goban.setHoshiPixelSize( 4 );

		// Make sure the Goban appears in the browser.
		//
		add( goban );

		// Add the highlighting.
		//
		GobanHighlighter gh = new GobanHighlighter( getGoban() );

		// Listen for clicks.
		//
		goban.addMouseListener( this );

		// Let the rules watch over the Goban.
		//
		setRules( new Rules( getGoban() ) );

    // Black makes the first move.
    //
    makeMove();
	}

	public void mouseClicked( MouseEvent me )
	{
		Goban goban = getGoban();
		Rules rules = getRules();
		Point point = new Point();

		// Drop a stone where the mouse clicked.
		//
		if( goban.translateCoord( me.getX(), me.getY(), point ) )
    {
			if( rules.canPlay( point ) )
      {
        getGoban().placeStone( getStone(), point );
        makeMove();
      }
    }
	}

  private void makeMove()
  {
    boolean
      moved = false,
      passed = false;
    int attempts = GOBAN_SIZE * GOBAN_SIZE;
    Point point = new Point();
		Rules rules = getRules();

    getGoban().removeAllMarks();

    while( !moved && (--attempts > 0) )
    {
      point.x = (int)(Math.random() * GOBAN_SIZE);
      point.y = (int)(Math.random() * GOBAN_SIZE);

			if( moved = rules.canPlay( point ) )
      {
        getGoban().placeStone( getStone(), point );
        getGoban().placeMark( new Markup( Markup.TRIANGLE ), point );
      }
    }

    if( !moved )
    {
      rules.togglePlayer();
      new AlertBox( new Frame(), "Game Over", "RandomPlayer Passes" );
    }
  }

	private Stone getStone()
	{
		return getRules().isWhiteToPlay() ?
      (Stone)getWhiteStone() : (Stone)getBlackStone();
	}

	protected Goban getGoban()
	{
		return this.goban;
	}

	private void setGoban( Goban goban )
	{
		this.goban = goban;
	}

	private void setRules( Rules rules )
	{
		this.rules = rules;
	}

	private Rules getRules()
	{
		return this.rules;
	}

	public void mouseEntered( MouseEvent me ) { }
	public void mouseExited( MouseEvent me ) { }
	public void mousePressed( MouseEvent me ) { }
	public void mouseReleased( MouseEvent me ) { }
}

