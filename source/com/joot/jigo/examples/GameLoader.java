/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.examples;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Vector;

import com.joot.jigo.dataformats.*;
import com.joot.jigo.dataformats.tokens.*;
import com.joot.jigo.dataformats.exceptions.DataFormatException;

/**
 * Example program showing how to read and parse a GoGame file.
 */
public class GameLoader
{
	private final static String UNKNOWN = "Unknown";
	private final static String DEFAULT_KOMI = "0.5";

	public static boolean show( String fileName )
	{
		try
		{
			GameReader.registerDriver( new com.joot.jigo.dataformats.sgf.SGFReaderDriver() );

			// Loads up the SGF file, given a particular file name.
			//
			long t1 = System.currentTimeMillis();
			System.out.println( t1 );
			GoGame game = GameReader.read(
				new FileInputStream( new File( fileName ) ) );
			System.out.println( System.currentTimeMillis() - t1 );

			//showInfoTokens( game );

			//showTree( game.getTree(), "" );
		}
		catch( IOException ioe )
		{
			ioe.printStackTrace();
			return false;
		}
		catch( DataFormatException dfe )
		{
			dfe.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Given an GameTree, this method will attempt to display its entire
	 * tree, including variations.
	 *
	 * @param tree - The GameTree containing an Game variation tree.
	 * @param spaces - Used for indentation.
	 */
	private static void showTree( GameTree tree, String spaces )
	{
		Enumeration
			trees = tree.getTrees(),
			leaves = tree.getLeaves(),
			tokens;

		while( (leaves != null) && leaves.hasMoreElements() )
		{
			GameToken token;
			tokens = ((GameLeaf)(leaves.nextElement())).getTokens();
			
			// Display the moves; check the token with instanceof to extract
			// move comments, board mark-up, etc.
			//
			while( (tokens != null) && tokens.hasMoreElements() )
			{
				token = (GameToken)(tokens.nextElement());

				if( token instanceof MoveToken )
				{
					MoveToken moveToken = (MoveToken)token;

					System.out.print( spaces );

					if( moveToken.isWhite() )
						System.out.print( "W: " );
					else
						System.out.print( "B: " );

					System.out.println( moveToken.getX() + ", " + moveToken.getY() );
				}
			}
		}

		// Display the variations ...
		//
		while( (trees != null) && trees.hasMoreElements() )
			showTree( (GameTree)(trees.nextElement()), " " + spaces );
	}

	private static void showInfoTokens( GoGame game )
	{
		String
			white		= UNKNOWN,
			black		= UNKNOWN,
			place		= UNKNOWN,
			date		= UNKNOWN,
			event		= UNKNOWN,
			round		= UNKNOWN,
			komi		= DEFAULT_KOMI,
			result	= UNKNOWN,
			comment	= "",
			system	= UNKNOWN;
	
		for( Enumeration e = game.getInfoTokens(); e.hasMoreElements(); )
		{
			InfoToken it = (InfoToken)(e.nextElement());

			if( it instanceof WhiteNameToken )
				white = it.toString();
			else if( it instanceof BlackNameToken )
				black = it.toString();
			else if( it instanceof ResultToken )
				result = it.toString();
			else if( it instanceof GameCommentToken )
				comment = ((GameCommentToken)it).getGameComment();
			else if( it instanceof KomiToken )
				komi = it.toString();

			// Keep using instanceof to pluck out additional game information
			// (such as place, date, round, event, and such).
			//
		}

		System.out.println( "White : " + white );
		System.out.println( "Black : " + black );
		System.out.println( "Result: " + result );
		System.out.println( "Komi  : " + komi );

		// ... etc. ...
	}

	public static void main( String args[] )
	{
		if( args.length == 1 )
		{
			if( !GameLoader.show( args[0] ) )
				System.out.println( "ERROR: Could not open " + args[0] );
		}
		else
			showUsage();
	}

	private static void showUsage()
	{
		System.out.println( "java com.joot.jigo.loader.GameLoader <filename>" );
	}
}

