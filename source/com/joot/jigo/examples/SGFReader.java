/*
 * Copyright (C) 2002 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.examples;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Vector;

import com.joot.jigo.dataformats.GameReader;

/**
 * Example program showing how to read and parse an SGF file using JiGo.  It
 * is also used to test reading of all the files in the JiGo SGF collection.
 */
public class SGFReader
{
	static
	{
		GameReader.registerDriver(
			new com.joot.jigo.dataformats.sgf.SGFReaderDriver() );
	}

	private static Vector readNames( String fileName )
		throws IOException
	{
		Vector names = new Vector( 100, 10 );
		String fromFile;

		BufferedReader br = new BufferedReader( new InputStreamReader(
			new FileInputStream( fileName ) ) );

		while( (fromFile = br.readLine()) != null )
			names.addElement( fromFile );

		br.close();

		return names;
	}

	/**
	 * args[0] must contain the name of a file that lists names of files
	 * to read.
	 */
	public static void main( String args[] )
		throws IOException
	{
		Vector fileNames = readNames( args[0] );
		int
			index = 0,
			MAX_NAMES = fileNames.size();

		while( index < MAX_NAMES )
		{
			try
			{
				String name = (String)(fileNames.elementAt( index++ ));
				System.out.println( "-- " + name + " ------------------------" );
				GameReader.read( new FileInputStream( name ) );
			}
			catch( Exception e )
			{
				// Got an oopsies; stop the test.
				//
				e.printStackTrace();
				System.exit( 1 );
			}
		}
	}
}

