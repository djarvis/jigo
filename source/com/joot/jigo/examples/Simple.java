/*
 * Copyright (C) 2002 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.examples;

import com.joot.jigo.Goban;
import com.joot.jigo.Stone;
import com.joot.jigo.Point;
import com.joot.jigo.WhiteStone;

import java.awt.Color;

/**
 * Creates a Goban with three stones.  This is perhaps the simplest task
 * you could possibly desire -- the equivalent of "Hello, World!"
 */
public class Simple extends java.applet.Applet {
  private Stone stone = new WhiteStone( 10, 12 );
  private Goban goban = new Goban( 9, new Color( 184, 115, 51 ), stone );

  public void init() {
    Goban goban = getGoban();
    Stone stone = getStone();

    goban.setHoshiPixelSize( 4 );
    goban.placeStone( stone, new Point( 0, 0 ) );
    goban.placeStone( stone, new Point( 4, 4 ) );
    goban.placeStone( stone, new Point( 8, 8 ) );

    // Make sure the Goban appears in the browser.
    //
    add( goban );
  }

  protected Goban getGoban() {
    return goban;
  }

  protected Stone getStone() {
    return stone;
  }
}

