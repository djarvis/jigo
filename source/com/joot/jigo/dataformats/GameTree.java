/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats;

import java.util.Enumeration;
import java.util.Vector;

import com.joot.jigo.dataformats.tokens.GameToken;
import com.joot.jigo.dataformats.tokens.InfoToken;

/**
 * This class contains a list of leaves, which in turn contain a list of
 * moves.  A GoGame may have many game trees which typically denote different
 * variations.
 */
public class GameTree
{
	/**
	 * Typical number of variations found in a file. Games should be analyzed
	 * to find optimal values.
	 */
	private final static int AVERAGE_VARIATIONS = 2;

	/**
	 * How many leaves (i.e., moves) are in a variation? This is small because
	 * variations tend to have a few moves. If more than 5 moves are present,
	 * the container for leaves is increased to hold much more.
	 */
	private final static int AVERAGE_LEAVES = 5;

	/**
	 * This is a largish number because if a variation contains more than the
	 * average number of leaves, it's probably not a variation, rather the
	 * actual moves in a game, which has a practical upper limit of 400 moves
	 * (professional games average around 230 moves).
	 */
	private final static int AVERAGE_LEAF_INCR = 50;

	/**
	 * When there are information tokens, this is about how many we expect
	 * there to be, on average.
	 */
	private final static int AVERAGE_INFO_TOKENS = 10;

	private Vector<GameLeaf> myLeaves;
	private Vector<GameTree> myTrees;

	/**
	 * No leaves, no variations, and rather useless by itself.
	 */
	public GameTree() { }

	/**
	 * Adds a leaf to the end of this tree's list of leaves. Remember that
	 * an GameLeaf contains moves, comments, board markup, etc. Do not add
	 * a null leaf.
	 *
	 * @param leaf - The leaf to add to the end of this tree's leaves list.
	 */
	public void add( GameLeaf leaf )
	{
		if( leaf != null )
		{
			// Have the myLeaves dynamic array increase for lots more moves
			// if it gets filled up. Typically, there aren't a lot of moves in a
			// variation -- so if there ARE a lot of moves, chances are it isn't
			// a variation, thus the array/vector should jump significantly in size.
			// This makes the code more efficient in both speed and memory usage.
			//
			if( myLeaves == null ) {
				myLeaves = new Vector<GameLeaf>( AVERAGE_LEAVES, AVERAGE_LEAF_INCR );
      }

			myLeaves.addElement( leaf );
		}
	}

	/**
	 * Please use add( GameLeaf ) - removes naming redundancy.
	 *
	 * @deprecated
	 */
	public void addLeaf( GameLeaf leaf )
	{
		add( leaf );
	}

	/**
	 * Returns the list of leaves at this level of the game tree.
	 *
	 * @return A list of leaves at this variation level, or null if no leaves
	 * have been added.
	 */
	public Enumeration getLeaves()
	{
		return (myLeaves == null) ? null : myLeaves.elements();
	}

	/**
	 * Returns any information tokens found in this tree, or null if no tokens
	 * are found.
	 */
	public Enumeration getInfoTokens()
	{
		Vector<GameToken> v = null;
		Enumeration leaves = getLeaves();

		// For all the leaves in the tree, get each leaf.  For each leaf,
		// get all its tokens.  Check all the tokens and slam the info tokens
		// into the vector.
		//
		if( leaves != null ) {
			while( leaves.hasMoreElements() ) {
				GameLeaf leaf = (GameLeaf)(leaves.nextElement());

				for( Enumeration tokens = leaf.getTokens(); tokens.hasMoreElements(); ) {
					GameToken token = (GameToken)tokens.nextElement();

					if( token instanceof InfoToken ) {
						if( v == null ) { v = new Vector<GameToken>( 10 );
            }

						v.addElement( token );
					}
				}
			}
		}

		return v == null ? null : v.elements();
	}

	/**
	 * Returns the number of leaves at this level of the game tree. This
	 * is not the same as the total number of leaves in the game, which
	 * can only be acquired by enumerating through all the trees in the game
	 * and tallying each of their own leaf counts.
	 * <P>
	 * However, most game files rarely show variations, and when they do they
	 * are few and far between. (This applies chiefly to professional-level
	 * games.)
	 *
	 * @return The number of leaves at this level of the game tree.
	 */
	public int getLeafCount()
	{
		return (myLeaves == null) ? 0 : myLeaves.size();
	}
	
	/**
	 * Returns the number of subtrees at this level of the game tree. This
	 * is not the same as the total number of variations in the game.
	 *
	 * @return The subtree count at this level of the game tree.
	 */
	public int getTreeCount()
	{
		return (myTrees == null) ? 0 : myTrees.size();
	}

	/**
	 * Please use getTreeCount() -- a GameTree should know nothing about
	 * variations.
	 *
	 * @deprecated
	 */
	public int getVariationCount()
	{
		return getTreeCount();
	}
	
	/**
	 * Appends a sub-tree to this tree's list of variations. Do not add a null
	 * tree.
	 *
	 * @param tree - The tree to add to the end of this tree's variation list.
	 */
	public void add( GameTree tree )
	{
		if( myTrees == null ) {
			myTrees = new Vector<GameTree>( AVERAGE_VARIATIONS );
    }

		myTrees.addElement( tree );
	}

	/**
	 * Please use add( GameTree ) -- this removes naming redundancy.
	 *
	 * @deprecated
	 */
	public void addTree( GameTree tree )
	{
		add( tree );
	}

	/**
	 * Returns the list of sub-trees at this level of the game tree.
	 *
	 * @return A list of sub-trees at this variation level, or null if none.
	 */
	public Enumeration getTrees()
	{
		return (myTrees == null) ? null : myTrees.elements();
	}
}

