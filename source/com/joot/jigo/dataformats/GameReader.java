/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Vector;

import com.joot.jigo.dataformats.exceptions.DataFormatException;
import com.joot.jigo.dataformats.exceptions.UnknownFileFormatException;

/**
 * The Game Reader is responsible for loading up a game of Go, and returning
 * it as a GoGame. For example:
 * <PRE>
 * import com.joot.jigo.dataformats.GoGame;
 * import com.joot.jigo.dataformats.GameReader;
 *
 * import com.joot.jigo.dataformats.sgf.SGFReaderDriver;
 *
 * private GoGame readGame( InputStream gameSource )
 *   throws IOException
 * {
 *   GameReader.registerDriver( new SGFReaderDriver() );
 *   return GameReader.read( gameSource );
 * }
 * </PRE>
 *
 * Any number of drivers can be registered.  No checks are made to see if
 * a driver is being re-registered.  Just don't register the same class
 * twice.  Also note that nothing precludes a single driver from knowing
 * how to parse multiple data formats ... it's just more modular to have a
 * one-to-one relationship.
 */
public class GameReader
{
	/**
	 * Number of bytes needed to determine the type of data format for any
	 * Go file (input stream).  It is presumed that all GameReaderDrivers
	 * can fully read the file if they don't find a problem within this
	 * many bytes.
	 */
	private final static int HEADER_BYTES = 4096;

	/** The list of drivers used for reading Go games. */
	private static Vector<GameReaderDriver> myDrivers =
    new Vector<GameReaderDriver>();

	/** No instances allowed. */
	private GameReader() { }

	/**
	 * Adds a driver from the list of drivers used when trying to load
	 * a game of Go. Do not pass in a null value.
	 *
	 * @param driver - A class that can parse a particular Go data format.
	 */
	public static void registerDriver( GameReaderDriver driver )
	{
		myDrivers.addElement( driver );
	}

	/**
	 * This will fail silently if the class name was not found.  This is
	 * a helper method so that the actual class need not be hardcoded --
	 * just its package and classname.
	 *
	 * @param driver - Name of a class that can parse Go data.
	 */
	public static void registerDriver( String driver )
	{
		try
		{
			Class driverClass = Class.forName( driver );
			registerDriver( (GameReaderDriver)(driverClass.newInstance()) );
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Removes a driver from the list of drivers used when trying to load
	 * a game of Go. Do not pass in a null value. If the given driver isn't
	 * in the list, nothing happens.
	 */
	public static void deregisterDriver( GameReaderDriver driver )
	{
		myDrivers.removeElement( driver );
	}

	/**
	 * Returns the list of drivers used to load Go games (any format).
	 */
	private static Enumeration getDrivers()
	{
		return myDrivers.elements();
	}

	/**
	 * Asks the registered drivers, in registration order, to parse the given
	 * InputStream.  If the data format cannot be parsed, the corresponding
	 * GameReaderDriver should throw a DataFormatException to indicate that
	 * there was a problem.  At the moment the first 4096 bytes are crucial
	 * to the success of reading the file.  If a GameReaderDriver reads more
	 * than that number of bytes and THEN finds an error, the buffer will
	 * not be reset to the beginning and thus cause failure for subsequent
	 * reads.
	 * <P>
	 * Given an input stream, this method will parse its contents and return
	 * a GoGame, complete with board markings, comments, and variations.
	 * On some minor errors (like syntax problems with the file), the parsing
	 * may stop and return whatever was parsed to that point.
	 *
	 * @param is - The InputStream from which contains a Go game in a format
	 * that should be recognized by at least one of the registered drivers.
	 *
	 * @return A complete game of Go with tokens and variations.
	 *
	 * @throws IOException - an error occured reading the stream.
	 * @throws DataFormatException - a driver-based error (probably bad data).
	 */
	public static synchronized GoGame read( InputStream is )
		throws IOException, DataFormatException
	{
		GoGame game = null;
		BufferedInputStream bis = new BufferedInputStream( is );
		bis.mark( HEADER_BYTES );

		// Give each driver a turn at trying to load the given stream.
		//
		for(
			Enumeration drivers = getDrivers();
			drivers.hasMoreElements() && (game == null); )
		{
			// Snag the next driver ... with any luck, it'll recognise the data
			// format and read the game.
			//
			GameReaderDriver driver = (GameReaderDriver)(drivers.nextElement());

			// If the driver is able to parse the data stream, then let it!
			//
			try
			{
				// Pass along the stream to the game reader driver.  We hope
				// at least one of the drivers will be able to read the file.
				//
				game = driver.read( bis );
			}
			catch( DataFormatException dfe )
			{
				// If debugging, display the exception.  It can be ignored since
				// there are probably other drivers ready and waiting to read the
				// contents.
				//
				bis.reset();
			}
		}

		// If we don't get a valid game read, then let the calling application
		// know something went awry.
		//
		if( game == null )
			throw new UnknownFileFormatException();

		return game;
	}
}

