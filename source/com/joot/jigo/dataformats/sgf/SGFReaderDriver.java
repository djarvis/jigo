/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.sgf;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.StreamTokenizer;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.joot.jigo.dataformats.GoGame;
import com.joot.jigo.dataformats.GameReaderDriver;
import com.joot.jigo.dataformats.exceptions.DataFormatException;
import com.joot.jigo.dataformats.tokens.GameToken;

import com.joot.jigo.dataformats.sgf.tokens.*;

/**
 * A way to load SGF files. This class handles SGF File Formats 3 and 4.
 */
public final class SGFReaderDriver implements GameReaderDriver
{
	private final static String INVALID_SGF_FILE = "Invalid SGF File.";

	/**
	 * Instead of a giant if-else mesh, use a hashtable to get the
	 * Class from the name.
	 */
	private static Hashtable<String, Class> tokenClasses =
    new Hashtable<String, Class>( 128 );

	/**
	 * Initialise the hashtable once; it can be static because they are
	 * always the same tokens.
	 */
	static
	{
		tokenClasses.put( "B", BlackMoveTokenImpl.class );
		tokenClasses.put( "BLACK", BlackMoveTokenImpl.class );
		tokenClasses.put( "W", WhiteMoveTokenImpl.class );
		tokenClasses.put( "WHITE", WhiteMoveTokenImpl.class );
		tokenClasses.put( "C", CommentTokenImpl.class );
		tokenClasses.put( "CP", CopyrightTokenImpl.class );
		tokenClasses.put( "COMMENT", CommentTokenImpl.class );
		tokenClasses.put( "N", NodeNameTokenImpl.class );
		tokenClasses.put( "NAME", NodeNameTokenImpl.class );

		tokenClasses.put( "FG", FigureTokenImpl.class );
		tokenClasses.put( "AR", ArrowTokenImpl.class );
		tokenClasses.put( "CR", CircleTokenImpl.class );
		tokenClasses.put( "SQ", SquareTokenImpl.class );
		tokenClasses.put( "LB", LetterTokenImpl.class );
		tokenClasses.put( "LETTER", LetterTokenImpl.class );
		tokenClasses.put( "MN", MoveNumberTokenImpl.class );
		tokenClasses.put( "LN", LineTokenImpl.class );
		tokenClasses.put( "DD", DimTokenImpl.class );
		tokenClasses.put( "SL", SelectedListTokenImpl.class );
		tokenClasses.put( "VW", ViewTokenImpl.class );
		tokenClasses.put( "VIEW", ViewTokenImpl.class );

		tokenClasses.put( "AB", AddBlackTokenImpl.class );
		tokenClasses.put( "ADDBLACK", AddBlackTokenImpl.class );
		tokenClasses.put( "AW", AddWhiteTokenImpl.class );
		tokenClasses.put( "ADDWHITE", AddWhiteTokenImpl.class );
			
		tokenClasses.put( "TE", TesujiTokenImpl.class );
		tokenClasses.put( "BM", BadMoveTokenImpl.class );
		tokenClasses.put( "GB", GoodBlackMoveTokenImpl.class );
		tokenClasses.put( "GW", GoodWhiteMoveTokenImpl.class );
		tokenClasses.put( "DM", EvenPositionTokenImpl.class );
		tokenClasses.put( "UC", UnclearPositionTokenImpl.class );
		tokenClasses.put( "IT", InterestingMoveTokenImpl.class );
		tokenClasses.put( "DO", DoubtfulMoveTokenImpl.class );
		tokenClasses.put( "KO", KoTokenImpl.class );

		tokenClasses.put( "FF", FileFormatTokenImpl.class );
		tokenClasses.put( "CA", CharacterEncodingTokenImpl.class );
		tokenClasses.put( "GM", GameTypeTokenImpl.class );
		tokenClasses.put( "GAME", GameTypeTokenImpl.class );
		tokenClasses.put( "SZ", SizeTokenImpl.class );
		tokenClasses.put( "SIZE", SizeTokenImpl.class );
		tokenClasses.put( "KM", KomiTokenImpl.class );
		tokenClasses.put( "KOMI", KomiTokenImpl.class );
		tokenClasses.put( "PW", WhiteNameTokenImpl.class );
		tokenClasses.put( "PLAYERWHITE", WhiteNameTokenImpl.class );
		tokenClasses.put( "PB", BlackNameTokenImpl.class );
		tokenClasses.put( "PLAYERBLACK", BlackNameTokenImpl.class );
		tokenClasses.put( "WR", WhiteRankTokenImpl.class );
		tokenClasses.put( "WHITERANK", WhiteRankTokenImpl.class );
		tokenClasses.put( "BR", BlackRankTokenImpl.class );
		tokenClasses.put( "BLACKRANK", BlackRankTokenImpl.class );
		tokenClasses.put( "DT", DateTokenImpl.class );
		tokenClasses.put( "DATE", DateTokenImpl.class );
		tokenClasses.put( "RE", ResultTokenImpl.class );
		tokenClasses.put( "RESULT", ResultTokenImpl.class );
		tokenClasses.put( "TM", TimeLimitTokenImpl.class );
		tokenClasses.put( "TIME", TimeLimitTokenImpl.class );
		tokenClasses.put( "WL", WhiteTimeTokenImpl.class );
		tokenClasses.put( "BL", BlackTimeTokenImpl.class );
		tokenClasses.put( "OW", WhiteStonesLeftTokenImpl.class );
		tokenClasses.put( "OB", BlackStonesLeftTokenImpl.class );
		tokenClasses.put( "PM", PrintModeTokenImpl.class );
		tokenClasses.put( "ST", SystemTokenImpl.class );
		tokenClasses.put( "PC", PlaceTokenImpl.class );
		tokenClasses.put( "PLACE", PlaceTokenImpl.class );
		tokenClasses.put( "EV", EventTokenImpl.class );
		tokenClasses.put( "EVENT", EventTokenImpl.class );
		tokenClasses.put( "RO", RoundTokenImpl.class );
		tokenClasses.put( "ROUND", RoundTokenImpl.class );
		tokenClasses.put( "SO", SourceTokenImpl.class );
		tokenClasses.put( "SOURCE", SourceTokenImpl.class );
		tokenClasses.put( "US", UserTokenImpl.class );
		tokenClasses.put( "GC", GameCommentTokenImpl.class );
		tokenClasses.put( "RU", RuleSetTokenImpl.class );
		tokenClasses.put( "WS", WhiteSpeciesTokenImpl.class );
		tokenClasses.put( "BS", BlackSpeciesTokenImpl.class );
		tokenClasses.put( "GN", GameNameTokenImpl.class );
		tokenClasses.put( "GAMENAME", GameNameTokenImpl.class );
		tokenClasses.put( "ID", GameIDTokenImpl.class );
	}

	public SGFReaderDriver() { }

	/**
	 * Returns a new GoGame, provided a valid (and open) InputStream.
	 * <P>
	 * Given an input stream, this method will parse its contents and return
	 * a full GoGame, complete with board markings, comments, and variations.
	 * On some minor errors (like syntax problems with the file), the parsing
	 * will stop and return whatever was parsed to that point.
	 * <P>
	 * It is the responsibility of the calling object to close the stream.
	 *
	 * @param is - Stream containing SGF 3 or SGF 4 data.
	 *
	 * @return A complete GoGame, or null on unrecoverable errors.
	 */
	public GoGame read( InputStream is )
		throws IOException, DataFormatException
	{
		// Create and initialize a new StreamTokenizer, to make parsing simple.
		//
		StreamTokenizer st = new StreamTokenizer(
			new BufferedReader(
			new InputStreamReader( is ) ) );

		resetTokenizer( st );

		return readGame( st );
	}

	/**
	 * Reads the entire game into a set of SGFTokens organized into a tree,
	 * which represent variations in the game. Does not handle multiple
	 * games in a single file, yet (why this was made part of the SGF
	 * specification is beyond me).
	 *
	 * @param st - The StreamTokenizer used to read text-based tokens from
	 * the input stream.
	 *
	 * @return A fully parsed SGFGame, or null on unrecoverable errors.
	 */
	private SGFGame readGame( StreamTokenizer st )
		throws IOException, DataFormatException
	{
		// Create an instance of SGFGame based on the tree read from the stream.
		//
		return new SGFGame( readTree( st ) );
	}

	/**
	 * Reads the contents of an SGFTree, provided a StreamTokenizer to help
	 * with parsing the textual tokens into SGFTokens.
	 * <P>
	 * A tree is a semi-colon followed by any number of leaves, followed by
	 * either a new tree (indicated by an opening parenthesis) or the end
	 * the current tree (indicated by a closing parenthesis).
	 *
	 * @param st - The StreamTokenizer from which to read an SGF File.
	 *
	 * @return An SGFTree representing the contents of the given stream.
	 */
	private SGFTree readTree( StreamTokenizer st )
		throws IOException, SGFException
	{
		SGFTree tree = new SGFTree();
		int token = 0;

		// Continue reading in from the file while the next token is one of:
		// leaf, sub-tree, or end of current tree.
		//
		// The StreamTokenizer will ignore whitespace and blank lines for us.
		//
		while( (token = st.nextToken()) != StreamTokenizer.TT_EOF )
		{
			switch( token )
			{
				// We found a new leaf, so read it.
				//
				case (int)';':
					tree.add( readLeaf( st ) );
					break;

				// Read an entire sub-tree (variation).
				//
				case (int)'(':
					tree.add( readTree( st ) );
					break;

				// This variation has no more leaves (or sub-variations).
				// Let's get out of dodge.  You wouldn't believe how long it
				// took to track down the StreamTokenizer bug ... [shudder]
				//
				case (int)')':
					return tree;

				// Ignore these ...
				//
				case (int)0x09:		// Tab
				case (int)0x0A:		// Line feed
				case (int)0x0D:		// Carriage return
				case (int)0x20:		// Space
					break;
					
				// A major parsing error has occured. We have to bail.
				//
				default:
					throw new SGFException( INVALID_SGF_FILE );
			}
		}

		return tree;
	}

	/**
	 * Reads the contents of an SGF node, provided a StreamTokenizer to help
	 * with parsing the textual tokens into SGFTokens.
	 * <P>
	 * A leaf (node) starts at a semicolon contains tokens up until another
	 * semicolon or closing parenthesis occurs.
	 *
	 * @param st - The StreamTokenizer from which to read an SGF node.
	 *
	 * @return An SGFLeaf representing the contents of the node.
	 */
	private SGFLeaf readLeaf( StreamTokenizer st )
		throws IOException, SGFException
	{
		int token = 0;
		SGFLeaf leaf = null;

		// Keep reading in tokens until the end of the file, the start of a new
		// leaf, start of a new tree, or end of the current tree/game. These
		// are a semi-colon, open parenthesis, and close parenthesis respectively.
		//
		while( ((token = st.nextToken()) != StreamTokenizer.TT_EOF) &&
						(token != (int)';') &&
						(token != (int)'(') &&
						(token != (int)')') )
		{
			if( token == StreamTokenizer.TT_WORD )
			{
				SGFTokenImpl sgfToken = readToken( st );

				if( sgfToken != null )
				{
					// If we don't have a leaf, create a new leaf with the given token.
					//
					if( leaf == null )
						leaf = new SGFLeaf( sgfToken );
					else
						leaf.addToken( sgfToken );
				}
			}
		}

		// We found something that couldn't be an SGFToken, so let the calling
		// method handle whatever we read. (End of game/variation/leaf, etc.)
		//
		st.pushBack();
		return leaf;
	}

	/**
	 * Reads an SGF token, provided a StreamTokenizer to help with parsing the
	 * text into SGFTokens.
	 * <P>
	 * @param st - The StreamTokenizer from which to read an SGF token.
	 *
	 * @return An SGFToken representing a piece of information about the game,
	 * or null if the token doesn't exist by that name.
	 */
	private SGFTokenImpl readToken( StreamTokenizer st )
		throws IOException, SGFException
	{
		SGFTokenImpl token = getTokenForName( st.sval );

		if( token != null )
		{
			// Now that we know what type of token we have, ask it to parse itself.
			// Most of the parsing is done by the TextToken class. All tokens are
			// subclasses of SGFToken (though TextToken subclasses are rampant).
			//
			token.setStreamTokenizer( st );
			token.parse();
		}

		return token;
	}

	/**
	 * Changes the settings of the given StreamTokenizer to make parsing the
	 * SGF easier.
	 *
	 * @param st - The StreamTokenizer to alter.
	 */
	private void resetTokenizer( StreamTokenizer st )
	{
		// Configure the StreamTokenizer to enhance parsing.
		//
		st.lowerCaseMode( false );
		st.eolIsSignificant( false );

		// There's a feature in StreamTokenizer: parsing of numbers is turned on
		// automagically (how nice, eh?). The only way to avoid this is to
		// make all numeric-related characters ordinary, then make them word
		// characters. Only by looking at the source to StreamTokenizer can it be
		// discerned that "ordinaryChars( ... )" removes all numeric properties
		// of the characters in question.
		//
		// To be a bit more efficient about this, we're going to make ALL relevant
		// characters ordinary, and then make them ALL word characters, and then
		// make only the special tokens ordinary (again). Otherwise, we'd have
		// to specifically select ranges out of the ASCII table and make several
		// calls to "wordChars" in order to make word characters out of everything
		// except the characters we have to "jump" over. This way we just make
		// everything equal except for exactly the characters we want to tokenize.
		//
		st.ordinaryChars( (int)0x00, (int)0xFF );
		st.wordChars( (int)'!', (int)'~' );

		st.ordinaryChar( (int)';' );
		st.ordinaryChar( (int)'\\' );
		st.ordinaryChars( (int)'[', (int)']' );
		st.ordinaryChars( (int)'(', (int)')' );
	}

	/**
	 * Looks into the token classes hashtable to find the class associated
	 * with the SGF token name.  This will return an instance of the
	 * corresponding class.  If the token name is not found it will return
	 * null.
	 *
	 * @param name - The name of the token whose class must be instantiated.
	 * @return The instance of token equivalent to the given token name.
	 */
	private SGFTokenImpl getTokenForName( String name )
	{
		Class tokenClass = (Class)(this.tokenClasses.get( name.toUpperCase() ));
		
		try
		{
			return (SGFTokenImpl)(tokenClass.newInstance());
		}
		catch( Exception e )
		{
			// If we couldn't instantiate the token for any reason (null
			// pointer, invalid access, etc.) then token will remain null
			// and thus we'll return a TextTokenImpl instance.
			//
		}

		return null;
	}
}

