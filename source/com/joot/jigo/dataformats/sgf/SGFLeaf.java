/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.sgf;

import java.io.OutputStream;
import java.io.IOException;

import com.joot.jigo.dataformats.GameLeaf;
import com.joot.jigo.dataformats.exceptions.DataFormatException;

import com.joot.jigo.dataformats.sgf.tokens.SGFTokenImpl;

/**
 * An SGFLeaf knows how to write itself out to a stream.
 */
public class SGFLeaf extends GameLeaf
{
	/**
	 * Creates a new SGFLeaf, with at least one token.
	 *
	 * @param token - The first token in this leaf.
	 */
	public SGFLeaf( SGFTokenImpl token )
	{
		super( token );
	}

	public void write( OutputStream bos )
		throws IOException, DataFormatException
	{
	}
}

