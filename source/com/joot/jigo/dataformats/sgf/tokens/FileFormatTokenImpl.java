/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.sgf.tokens;

import java.io.StreamTokenizer;
import java.io.IOException;

import com.joot.jigo.dataformats.sgf.SGFException;

import com.joot.jigo.dataformats.tokens.FileFormatToken;

/**
 * The version of SGF file.	Versions 3 and 4 are supported.
 */
public final class FileFormatTokenImpl extends NumberTokenImpl
	implements FileFormatToken
{
	private final static String INVALID_VERSION =
		"Invalid SGF File Version: ";

	public FileFormatTokenImpl() { }

	/**
	 * Presume SGF File Format 3 by default.
	 */
	protected float getDefault() { return 3; }

	protected boolean parseContent( StreamTokenizer st )
		throws IOException, SGFException
	{
		if( !super.parseContent( st ) )
			return false;

		if( getVersion() > 4 )
			throw new SGFException( INVALID_VERSION + getVersion() );

		return true;
	}

	public int getVersion() { return (int)getNumber(); }
}

