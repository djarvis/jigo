/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.sgf.tokens;

import java.io.StreamTokenizer;
import java.io.IOException;

import com.joot.jigo.Point;

/**
 * A token that contains the point at which is was played, or the point
 * to which it refers.
 * <P>
 * Most often, this class will be used by BlackMoveToken and WhiteMoveToken.
 */
public class PlacementTokenImpl extends SGFTokenImpl
{
	private Point point = new Point();

	public PlacementTokenImpl() { }

	protected boolean parseContent( StreamTokenizer st )
		throws IOException
	{
		return parsePoint( st );
	}

	/**
	 * Parses a point, sets the X and Y values of the PlacementToken
	 * accordingly.	This can be called repeatedly for Tokens which take
	 * any number of points (see: PointListToken).
	 * <P>
	 * The first opening '[' must have already been read; thus leaving two
	 * letters and a closing ']'.	This method reads everything up to and
	 * including ']'.	If a ']' follows immediately after the '[', then this
	 * move is considered a pass in FF[4].
	 * </P>
	 * <P>
	 * The letters are from 'a' through 'Z', inclusive, to represent row
	 * (or column) 1 through 52, respectfully.
	 * </P>
	 * <P>
	 * Note that 'tt' is not treated as a Pass, in accordance to
	 * SGF FF[3].	It is treated as a move at 20, 20.	It is up to the
	 * application (which knows the size of the board) to interpret (20, 20)
	 * as a pass--if the board size happens to be 19 lines or fewer.
	 * </P>
   *
	 * @return true - The point was perfectly parsed.
	 */
	protected boolean parsePoint( StreamTokenizer st )
		throws IOException
	{
		int token = st.nextToken();

		if( (token == (int)']') || (token != StreamTokenizer.TT_WORD) )
			return true;

		try
		{
      StringBuffer value = new StringBuffer();

      // Sometimes the SGF file might be corrupted a bit (extra spaces in the
      // moves). This makes a simple attempt at fixing the corruption.
      //
      do
      {
        if( token == StreamTokenizer.TT_WORD )
        {
          value.append( st.sval );
        }

        token = st.nextToken();
      }
      while( token != ']' );

      String result = value.toString();

			this.point.x = coordFromChar( result.charAt( 0 ) );
			this.point.y = coordFromChar( result.charAt( 1 ) );
		}
		catch( Exception e ) { }

		return (token == (int)']');
	}

	/**
	 * Given a token whose value ranges between 'a' through 'z', or 'A'
	 * through 'Z', this method returns the appropriate row/column value.	If
	 * the token isn't between 'a' and 'z', or 'A' and 'Z', this returns 0;
	 */
	private byte coordFromChar( int ch )
	{
		if( (ch >= 'a') && (ch <= 'z') )
			return (byte)(ch - 'a' + 1);

		if( (ch >= 'A') && (ch <= 'A') )
			return (byte)(ch - 'A' + 1);

		return 0;
	}

	/**
	 * Only subclasses (and classes in this package) may get at this class's
	 * Point variable.	Everybody else must use getX() and getY(), so as to
	 * protect the values from change.
	 */
	protected Point getPoint() { return this.point; }

	/**
	 * @return The X coordinate of the placement.
	 */
	public int getX() { return this.point.x; }

	/**
	 * @return The Y coordinate of the placement.
	 */
	public int getY() { return this.point.y; }
}

