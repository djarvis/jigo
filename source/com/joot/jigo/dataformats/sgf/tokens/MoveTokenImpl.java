/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.sgf.tokens;

import com.joot.jigo.Point;

import com.joot.jigo.dataformats.tokens.MoveToken;

/**
 * A generic move.	The superclass for BlackMoveToken and WhiteMoveToken.
 */
public abstract class MoveTokenImpl extends PlacementTokenImpl
	implements MoveToken
{
  private Point point;

	public MoveTokenImpl() { }

  public boolean pointEquals( Point p )
  {
    return p.equals( getPoint() );
  }

  protected Point getPoint()
  {
    if( this.point == null )
    {
      this.point = new Point( getX(), getY() );
    }

    return this.point;
  }

  public boolean isPass( int boardSize )
  {
    return isPass() || (getX() > boardSize) || (getY() > boardSize);
  }

	private boolean isPass() { return (getX() == 0) && (getY() == 0); }

	public abstract boolean isWhite();

  public String toString()
  {
    return (isWhite() ? "W" : "B") + " @ " + getPoint();
  }
}

