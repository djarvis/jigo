/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.sgf;

import java.io.IOException;
import java.io.OutputStream;

import java.util.Enumeration;
import java.util.Vector;

import com.joot.jigo.dataformats.GoGame;

import com.joot.jigo.dataformats.exceptions.DataFormatException;

/**
 * Represents a full Go game; typically after having been parsed from
 * an InputStream conforming to the SGF (Smart-Game Format) file standard.
 * <P>
 * An SGFGame contains an SGFTree which houses every node (that is, a leaf)
 * of the game. Leaves contain information such as moves, markup, and comments.
 * <P>
 * All tokens related to information about the game are also contained
 * by this class. This makes the tokens easy to find and use (rather than
 * making them part of the first leaf of the first sub-tree of the game).
 * <P>
 * Eventually this class will wrap the functionality for saving an SGF
 * game, so the idea of allowing anyone add InfoTokens is valid (rather
 * than keeping it protected in the family, so to speak).
 */
public class SGFGame extends GoGame
{
	/**
	 * Creates a new SGFGame given a tree which comprises the game's nodes
	 * (and sub-trees representing variations).
	 *
	 * @param tree - The root tree for this SGFGame.
	 */
	public SGFGame( SGFTree tree )
	{
		super( tree );
	}

	/**
	 * Writes this SGFGame out to the given stream.
	 */
	public void write( OutputStream os )
		throws IOException, DataFormatException
	{
	}
}

