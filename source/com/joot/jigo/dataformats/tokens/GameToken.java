/*
 * Copyright (C) 2002 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.tokens;

import java.io.IOException;

import com.joot.jigo.dataformats.exceptions.DataFormatException;

/**
 * All game tokens must be subinterfaceed from this interface. All of
 * the tokens must know how to parse themselves.
 * <P>
 * Different game formats will use unique sources of data: sometimes
 * an input stream, sometimes pre-parsed XML (JDOM) documents, or perhaps
 * even SAX data. In any case, each token shall know how to extract the
 * required information from within its context. See the SGF classes for
 * an example of a parser that applies these interfaces.
 */
public interface GameToken
{
	/**
	 * Called to parse the token.
	 */
	public boolean parse()
	  throws IOException, DataFormatException;
}

