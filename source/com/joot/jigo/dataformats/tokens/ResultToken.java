/*
 * Copyright (C) 2002 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.tokens;

import com.joot.jigo.dataformats.exceptions.DataFormatException;

/**
 * The result for this game (jigo, winner: score, resignation, time).
 * <P>
 * The constants should be obvious. The "toString" method returns an
 * English-readable string of the game's result.
 */
public interface ResultToken extends InfoToken
{
	public final static int
		WHITE_WINS =  1,
	  BLACK_WINS =  2,
	  JIGO       =  3,
	  SUSPENDED  =  4;

	public final static int
		BY_SCORE       = 1,
	  BY_RESIGNATION = 2,
	  BY_TIME        = 3,
	  BY_FORFEIT     = 4,
	  BY_UNKNOWN     = 5,
	  BY_JIGO        = 6;

	/**
	 * Returns one of the "winner" constants (WHITE_WINS, BLACK_WINS,
	 * JIGO, or SUSPENDED).
	 *
	 * @return Who won, tie game, or suspended (check against constants).
	 */
	public int getWinner();

	/**
	 * Returns one of the "reason" constants (BY_SCORE, BY_TIME, BY_FORFEIT,
	 * BY_RESIGNATION or BY_UNKNOWN).
	 *
	 * @return The reason for winning.
	 */
	public int getReason();

	/**
	 * Provided getReason returns BY_SCORE, this method will return how much
	 * the winner won by.
	 *
	 * @return How many points the winner obtained.
	 */
	public float getScore();

	/**
	 * Returns a human-readable string of the game's result.
	 *
	 * @return Text stating the game's result.
	 */
	public String toString();
}

