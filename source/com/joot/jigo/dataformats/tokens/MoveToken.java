/*
 * Copyright (C) 2002 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats.tokens;

import com.joot.jigo.Point;

/**
 * A generic move.
 */
public interface MoveToken extends GameToken
{
  public boolean pointEquals( Point p );

	/**
	 * Indicates if this move was a pass. The requirement for the board size is
   * because some games use TT to indicate a pass. The TT is a valid move on
   * board sizes that are larger than 19x19, but some data formats (SGF) can
   * use TT to indicate a pass on a 19x19 board.
   *
   * @param boardSize - The size of the board for the game.
	 *
	 * @return true - The move indicates the player passes.
	 */
	public boolean isPass( int boardSize );

	/**
	 * Indicates if the colour of the player who made the move played white.
	 *
	 * @return true - White made the current move.
	 */
	public boolean isWhite();

	public int getX();

	public int getY();
}

