/*
 * Copyright (C) 2002 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats;

import java.io.InputStream;
import java.io.IOException;

import com.joot.jigo.dataformats.exceptions.DataFormatException;

/**
 * This interface allows multiple drivers for reading Go game data
 * transparently to JiGo applets and applications.  Implementing this
 * interface requires the driver make its best attempt at reading the
 * contents of the game.  If it fails, it should throw DataFormatException.
 * <P>
 * When writing a GameReaderDriver, you msut be able to determine if
 * the format is valid within the first 4096 characters.  Otherwise,
 * throw a DataFormatException when the file is known to be incompatible.
 * Reading more than 4096 characters will cause all subsequent Go Game
 * Readers to fail when trying to auto-detect and load Go game data.
 * <P>
 * In other words, be kind: please rewind.
 */
public interface GameReaderDriver
{
	/**
	 * Given an input stream pointing to the start of a Go game, whose data
	 * format is understood by this driver, this method will read the contents
	 * of the stream and return a valid GoGame instance. Any problems with
	 * the content should cause a DataFormatException to be thrown.
	 *
	 * @return A valid GoGame never null.
	 */
	public GoGame read( InputStream is )
		throws IOException, DataFormatException;
}

