/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats;

import java.io.IOException;
import java.io.OutputStream;

import java.util.Enumeration;
import java.util.Vector;

import com.joot.jigo.dataformats.tokens.InfoToken;
import com.joot.jigo.dataformats.tokens.GameToken;

import com.joot.jigo.dataformats.exceptions.DataFormatException;

/**
 * Represents a game of Go, normally after parsed from an InputStream.
 * <P>
 * All tokens related to information about the game are also contained
 * by this class. This makes the tokens easy to find and use (rather than
 * making them part of the first leaf of the first sub-tree of the game).
 */
public abstract class GoGame
{
	private GameTree _tree;

	/**
	 * Creates a new GoGame given a tree which of the game's nodes
	 * (and sub-trees representing variations).
	 *
	 * @param tree - The root tree for this GoGame.
	 */
	public GoGame( GameTree tree )
	{
		setTree( tree );
	}

	/**
	 * Returns the game tree, at its zeroth variation (i.e., root level).
	 *
	 * @return An GameTree which contains all moves played in the game.
	 */
	public GameTree getTree() { return _tree; }

	/**
	 * Used to set this game's game tree.
	 *
	 * @param gameTree - The game's game tree.
	 */
	private void setTree( GameTree gameTree ) { _tree = gameTree; }

	/**
	 * This will never return null.
	 */
	public Enumeration getInfoTokens( GameTree tree )
	{
		Vector<GameToken> v = new Vector<GameToken>( 10 );
		Enumeration leaves = tree.getLeaves();

		// For all the leaves in the tree, get each leaf.  For each leaf,
		// get all its tokens.  Check all the tokens and slam the info tokens
		// into the vector.
		//
		if( leaves != null )
			while( leaves.hasMoreElements() )
			{
				GameLeaf leaf = (GameLeaf)(leaves.nextElement());

				for( Enumeration tokens = leaf.getTokens(); tokens.hasMoreElements(); )
				{
					GameToken token = (GameToken)tokens.nextElement();

					if( token instanceof InfoToken ) {
						v.addElement( token );
          }
				}
			}

		return v.elements();
	}
	
	/**
	 * Returns the list of tokens that contain information about this game.
	 * It will never return null.
	 *
	 * @return An Enumeration of type com.joot.jigo.dataformats.tokens.InfoToken.
	 */
	public Enumeration getInfoTokens()
	{
		return getInfoTokens( getTree() );
	}

	/**
	 * Saves the contents of this Go game to the given output stream.
	 */
	public abstract void write( OutputStream bos )
		throws IOException, DataFormatException;
}

