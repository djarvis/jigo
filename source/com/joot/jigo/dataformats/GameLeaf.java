/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.dataformats;

import java.util.Enumeration;
import java.util.Vector;

import com.joot.jigo.dataformats.tokens.CommentToken;
import com.joot.jigo.dataformats.tokens.GameToken;
import com.joot.jigo.dataformats.tokens.MoveToken;

/**
 * Contains all tokens for a particular node; usually it's a player's move.
 * Since a move can have comments, board markup, and board settings, this class
 * stores everything associated with a move under one roof. This does not
 * mean a move must always be associated with a leaf, that's just what happens
 * more often than not. 
 */
public class GameLeaf
{
	private Vector<GameToken> myTokens = new Vector<GameToken>();

	/**
	 * Creates a new GameLeaf; all leaves need at least one token. So if you
	 * add this leaf to a tree without having any tokens inside, you can
	 * potentially hose the system. That's why it's protected.
	 */
	protected GameLeaf()
	{
	}

	/**
	 * Creates a new GameLeaf, provided the first game token for the leaf.
	 * This constructor is the preferred way to create a GameLeaf.
	 *
	 * @param token - The first token in this leaf.
	 */
	public GameLeaf( GameToken token )
	{
		addToken( token );
	}

	/**
	 * Returns the first token representing a move played on the board.
	 *
	 * @return null - no moves for this leaf. 
	 */
	public MoveToken getMoveToken()
	{
		GameToken token;

		for( Enumeration e = getTokens(); e.hasMoreElements(); )
			if( (token = (GameToken)(e.nextElement())) instanceof MoveToken )
				return (MoveToken)token;

		return null;
	}

	/**
	 * Returns the first token representing a comment for this leaf.
	 *
	 * @return null - no comment for this leaf. 
	 */
	public CommentToken getCommentToken()
	{
		GameToken token;

		for( Enumeration e = getTokens(); e.hasMoreElements(); )
			if( (token = (GameToken)(e.nextElement())) instanceof CommentToken )
				return (CommentToken)token;

		return null;
	}

	/**
	 * Adds a token to this leaf; usually a player's move, but includes comments,
	 * markup and so forth.
	 *
	 * @param token - A game token that must be added to the leaf.
	 */
	public void addToken( GameToken token )
	{
		if( token != null )
			myTokens.addElement( token );
	}

	/**
	 * Removes a token from this leaf; useful for deleting comments and board
	 * markup.
	 *
	 * @param token - A game token that must be removed from the leaf.
	 */
	public void removeToken( GameToken token )
	{
		if( token != null )
			myTokens.removeElement( token );
	}
	
	/**
	 * Returns the tokens (moves, comments, etc.) contained by this leaf.
	 *
	 * @return An Enumeration of type com.joot.jigo.dataformats.tokens.GameToken.
	 */
	public Enumeration getTokens()
	{
		return myTokens.elements();
	}
}

