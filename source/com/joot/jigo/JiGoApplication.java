/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.awt.Image;

import java.util.Properties;

import javax.imageio.ImageIO;

import javax.swing.JFrame;

import com.joot.jigo.dataformats.GameReader;

import com.joot.jigo.dataformats.sgf.SGFReaderDriver;

/**
 * Used as a framework for writing JiGo Applications.  Does all the basic
 * tasks required for setting up a Goban.  This includes loading up the
 * images from a properties file, housing a Goban, and so forth.  It has
 * a main that gets the show on the road.
 * <P>
 * By default it loads properties from a file whose name is the same as
 * the main class application plus ".conf".
 */
public class JiGoApplication extends JFrame
{
	private final static String DEFAULT_W_STONE	= "images/stones/white40x40.gif";
	private final static String DEFAULT_B_STONE	= "images/stones/black40x40.gif";
	private final static String DEFAULT_GRAIN		= "images/wood/woodgrain-01.jpg";

	private final static int DEFAULT_STONE_HEIGHT = 22;
	private final static int DEFAULT_STONE_WIDTH = 20;

	private final static String PROP_W_STONE		= "WHITE_STONE";
	private final static String PROP_B_STONE 		= "BLACK_STONE";
	private final static String PROP_GRAIN	= "GRAIN";

	private final static String CONFIG_FILE_EXT = ".conf";

	private Stone
		_whiteStone,
		_blackStone;

	// _grain = goban's grain image (typically wood grain)
	//
	private Image _grain;

	private Goban _goban;
	private Rules _rules;

	private File _config;

	static
	{
		GameReader.registerDriver( new SGFReaderDriver() );
	}

	public JiGoApplication()
		throws IOException
	{
		// From the name of the class we derive the name of the configuration
		// file.  For example, "JiGoApplication.conf".
		//
		String name = getClass().getName();
		name = name.substring( name.lastIndexOf( '.' ) + 1 ) + CONFIG_FILE_EXT;

		setConfig( new File( name ) );
	}

	public JiGoApplication( String config )
		throws IOException
	{
		this( new File( config ) );
	}

	public JiGoApplication( File config )
	{
		setConfig( config );
	}

	/**
	 * Given a list of properties, this will reset the Presenter to
	 * those values.  Subclasses should override this to initialise their own
	 * parameters.  This automatically calls init( Properties ) followed by
	 * init().  init( Properties ) should assign values from the config file
	 * to class-scope variables.  Then init() should do whatever setup is
	 * needed to get the show on the road.
	 */
	protected void load()
		throws IOException
	{
		init( loadProperties() );
		init();
	}

	/**
	 * Assigns the values loaded from a config file to class-scope variables.
 	 */
	protected void init( Properties properties )
		throws IOException
	{
		setWhiteStone( properties.getProperty( PROP_W_STONE, DEFAULT_W_STONE ) );
		setBlackStone( properties.getProperty( PROP_B_STONE, DEFAULT_B_STONE ) );
		setGrain( properties.getProperty( PROP_GRAIN, DEFAULT_GRAIN ) );
	}

	/**
	 * After init( Properties ), this method may safely use any of the
	 * variables assigned during the aforementioned method.
	 */
	protected void init()
	{
		Stone stone = new WhiteStone( getStoneWidth(), getStoneHeight() );
		setGoban( new Goban( getGrain(), stone ) );
		setRules( new Rules( getGoban() ) );
	}

	/**
	 * Loads the properties from the config file.
	 */
	private Properties loadProperties()
		throws IOException
	{
		Properties properties = new Properties();
		properties.load( new FileInputStream( getConfig() ) );
		return properties;
	}

	protected int getStoneWidth()
	{
		return DEFAULT_STONE_WIDTH;
	}

	protected int getStoneHeight()
	{
		return DEFAULT_STONE_HEIGHT;
	}

	private Image readStone( String file )
		throws IOException
	{
		return readImage( file ).getScaledInstance(
			getStoneWidth(), getStoneHeight(), Image.SCALE_SMOOTH );
	}

	private void setWhiteStone( String file )
		throws IOException
	{
		_whiteStone = new WhiteStone( readStone( file ) );
	}

	private void setBlackStone( String file )
		throws IOException
	{
		_blackStone = new BlackStone( readStone( file ) );
	}

	protected WhiteStone getWhiteStone()
	{
		return (WhiteStone)_whiteStone;
	}

	protected BlackStone getBlackStone()
	{
		return (BlackStone)_blackStone;
	}

	protected Image readImage( String file )
		throws IOException
	{
		return ImageIO.read( new File( file ) );
	}

	/**
	 * Sets the image to use as the grain (wood grain, typically).
	 */
	protected void setGrain( String file )
		throws IOException
	{
		_grain = readImage( file );
	}

	protected int getGobanSize()
	{
		return Goban.DEFAULT_SIZE;
	}

	protected Image getGrain()
	{
		return _grain;
	}

	protected void setGoban( Goban goban )
	{
		_goban = goban;
	}

	protected Goban getGoban()
	{
		return _goban;
	} 

	private void setRules( Rules rules )
	{
		_rules = rules;
	}

	private void setConfig( File config )
	{
		_config = config;
	}

	private File getConfig()
	{
		return _config;
	}
}

