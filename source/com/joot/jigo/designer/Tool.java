/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.designer;

import com.joot.jigo.Goban;

import com.joot.awt.widgets.Toolbar;

/**
 * This class encompasses the knowledge of what tools are available and what
 * they do upon clicking.  It needs to know what Goban instance it will be
 * acting upon.
 */
public final class Tool
{
  public Tool( Goban goban, Toolbar toolbar )
  {
  }
}

