/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.designer;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Rectangle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;

import java.io.IOException;
import java.io.IOException;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;

import java.util.Calendar;

import com.joot.awt.widgets.Toolbar;
import com.joot.awt.widgets.ImageButton;
import com.joot.awt.widgets.ToggleImageButton;

import com.joot.jigo.BlackStone;
import com.joot.jigo.Goban;
import com.joot.jigo.GobanPanel;
import com.joot.jigo.GobanHighlighter;
import com.joot.jigo.JiGoApplet;
import com.joot.jigo.Point;
import com.joot.jigo.Rules;
import com.joot.jigo.WhiteStone;

import com.joot.util.images.ImageWriter;

/**
 * The frame that houses a Goban.
 */
public class DesignerPanel extends GobanPanel
	implements ActionListener, ComponentListener,
						 MouseListener, MouseMotionListener
{
	// JIGO = 0xA97F = 43391
	//
	private final static int DEFAULT_DESIGNER_PORT = 43391;

	private final static String DESIGNER_PORT_PARAM = "Designer Port";

	private final static String TB_DIRECTORY = "images/toolbar/";

	private final static String
		CHECKMARK		= "chk-",
		NO_CHECKMARK = "tb-";

	/** Action commands for the action event; numbers faster than strings. */
	private final static int
		BSTONE		= 1,
		WSTONE		= 2,
		CIRCLE		= 3,
		SQUARE		= 4,
		TRIANGLE	= 5,
		NUMBERS		= 6,
		LETTERS		= 7,
		SAVE			= 8;

	private final static String
		BSTONE_FILE		= "bstone.gif",
		WSTONE_FILE		= "wstone.gif",
		CIRCLE_FILE		= "circle.gif",
		SQUARE_FILE		= "square.gif",
		TRIANGLE_FILE	= "triangle.gif",
		NUMBERS_FILE	= "numbers.gif",
		LETTERS_FILE	= "letters.gif",
		SAVE_FILE			= "save.gif";

	private Toolbar myToolbar = new Toolbar();

	/** The button that was last toggled when the user toggled another button. */
	private ToggleImageButton currToggleButton;

	/** Intersection where the mouse clicked. */
	private Point myClickPoint = new Point( 0, 0 );

	public DesignerPanel( JiGoApplet applet )
	{
		super( applet );

		try
		{
			createToolbar();
		}
		catch( Exception e ) { }

		addComponentListener( this );

		// Stay in touch of the Goban's mouse clicks, and mouse drags.
		//
		getGoban().addMouseListener( this );
		getGoban().addMouseMotionListener( this );

		GobanHighlighter gh = new GobanHighlighter( getGoban() );
	}

	protected void initGUI()
	{ 
		add( getGoban() );
	}

	private void createToolbar()
		throws IOException, MalformedURLException, InterruptedException
	{
		Toolbar tb = getToolbar();

		// The first button on the toolbar (the white stone) should be set to
		// our currently toggled button. Furthermore, it should be toggled on
		// immediately.
		//
		currToggleButton = new ToggleImageButton(
			loadImage( TB_DIRECTORY + CHECKMARK + WSTONE_FILE ),
			loadImage( TB_DIRECTORY + NO_CHECKMARK + WSTONE_FILE ),
			Integer.toString( WSTONE ) );

		currToggleButton.toggleOn();

		tb.addButton( currToggleButton );

		tb.addButton( new ToggleImageButton(
			loadImage( TB_DIRECTORY + CHECKMARK + BSTONE_FILE ),
			loadImage( TB_DIRECTORY + NO_CHECKMARK + BSTONE_FILE ),
			Integer.toString( BSTONE ) ) );

		tb.addButton( new ToggleImageButton(
			loadImage( TB_DIRECTORY + CHECKMARK + CIRCLE_FILE ),
			loadImage( TB_DIRECTORY + NO_CHECKMARK + CIRCLE_FILE ),
			Integer.toString( CIRCLE ) ) );

		tb.addButton( new ToggleImageButton(
			loadImage( TB_DIRECTORY + CHECKMARK + SQUARE_FILE ),
			loadImage( TB_DIRECTORY + NO_CHECKMARK + SQUARE_FILE ),
			Integer.toString( SQUARE ) ) );

		tb.addButton( new ToggleImageButton(
			loadImage( TB_DIRECTORY + CHECKMARK + TRIANGLE_FILE ),
			loadImage( TB_DIRECTORY + NO_CHECKMARK + TRIANGLE_FILE ),
			Integer.toString( TRIANGLE ) ) );

		tb.addButton( new ToggleImageButton(
			loadImage( TB_DIRECTORY + CHECKMARK + NUMBERS_FILE ),
			loadImage( TB_DIRECTORY + NO_CHECKMARK + NUMBERS_FILE ),
			Integer.toString( NUMBERS ) ) );

		tb.addButton( new ToggleImageButton(
			loadImage( TB_DIRECTORY + CHECKMARK + LETTERS_FILE ),
			loadImage( TB_DIRECTORY + NO_CHECKMARK + LETTERS_FILE ),
			Integer.toString( LETTERS ) ) );

		tb.addButton( new ImageButton(
			loadImage( TB_DIRECTORY + NO_CHECKMARK + SAVE_FILE ),
			Integer.toString( SAVE ) ) );

		tb.addActionListener( this );
	}

  private String getHostName()
  {
		try
		{
			return InetAddress.getLocalHost().getHostName();
		}
		catch( Exception e ) { }

    return "localhost";
  }

	/**
	 * Creates a unique file name based on the local system date.  It's hokey,
	 * insecure, and fairly easy to change so it overwrites files already on
	 * the server.  The server should check for such nasty behaviours.
	 */
	private String createFileName()
	{
		Calendar calendar = Calendar.getInstance();
		int hour	 = calendar.get( Calendar.HOUR	 ),
				minute = calendar.get( Calendar.MINUTE ),
				second = calendar.get( Calendar.SECOND );

		String systemDate =
			((hour	 < 10) ? ("0" + hour	) : "" + hour	) + "-" +
			((minute < 10) ? ("0" + minute) : "" + minute) + "-" +
			((second < 10) ? ("0" + second) : "" + second);

		try
		{
			return getHostName() + "-" + systemDate;
		}
		catch( Exception e ) { }

		return null;
	}

	private void saveImage()
	{
		Dimension d = getGoban().getSize();
		Image screenShot = createImage( d.width, d.height );

		getGoban().copyGraphics( screenShot.getGraphics() );

		try
		{
			// Get the name of the host computer, get the port for saving
			// connections, and let it rip.
			//
			Socket socket = new Socket(
				getHostName(), getParam( DESIGNER_PORT_PARAM, DEFAULT_DESIGNER_PORT ) );

			ImageWriter.writeImage(
				screenShot,
				createFileName(), socket.getOutputStream() );

			socket.close();
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	private void showDefaultCursor()
	{
		Cursor cursor = Cursor.getPredefinedCursor( Cursor.DEFAULT_CURSOR );
		getToolbar(). setCursor( cursor );
		setCursor( cursor );
	}

	private void showWaitCursor()
	{
		Cursor cursor = Cursor.getPredefinedCursor( Cursor.WAIT_CURSOR );
		getToolbar(). setCursor( cursor );
		setCursor( cursor );
	}

	public void actionPerformed( ActionEvent ae )
	{
		int action = Integer.parseInt( ae.getActionCommand() );

		// The save button isn't a toggle button, so we have to treat it
		// special.
		//
		if( action == SAVE )
		{
			showWaitCursor();
			saveImage();
			showDefaultCursor();
			return;
		}

		ToggleImageButton toggledButton = (ToggleImageButton)(ae.getSource());

		// If the button that was just pressed has gone into the On state, then
		// we want to turn our current button Off.
		//
		if( toggledButton.isToggledOn() )
			currToggleButton.toggleOff();
		else
			currToggleButton.toggleOn();

		// Save the currently On button for the next time the user clicks.
		//
		currToggleButton = toggledButton;
	}

	/**
	 * State Machine for Mouse Clicks. Legend:
	 * <PRE>
	 * N = No Modifier
	 * S = Shift
	 * C = Control
	 * A = Alt
	 * </PRE>
	 * 
	 * <PRE>
	 * Modifier | Stone/Mark | Action
	 * ---------+------------+------------------
	 * N				| Yes	/No	 | Remove Stone
	 * N				| Yes	/Yes	| Remove Stone
	 * N				| No	 /No	 | Add White Stone
	 * N				| No	 /Yes	| Add White Stone
	 * S				| No	 /Yes	| Add Black Stone
	 * S				| No	 /No	 | Add Black Stone
	 * S				| Yes	/No	 | Toggle Stone Colour
	 * S				| Yes	/Yes	| Toggle Stone Colour
	 * C				| No	 /No	 | Add Circle
	 * C				| No	 /Yes	| Toggle Mark: SQ -> TR -> Circle -> SQ
	 *
	 * </PRE>
	 */
	public void mousePressed( MouseEvent me )
	{
		Goban goban = getGoban();
		WhiteStone wStone = getWhiteStone();
		BlackStone bStone = getBlackStone();

		// Figure out the intersection where the user clicked.
		//
		if( !goban.translateCoord( me.getX(), me.getY(), myClickPoint ) )
			return;

		// Get the modifiers at the time of the click.
		//
		int mods = me.getModifiers();
		boolean shift = (mods & MouseEvent.SHIFT_MASK) == MouseEvent.SHIFT_MASK,
						control = (mods & MouseEvent.CTRL_MASK) == MouseEvent.CTRL_MASK;

		boolean hasStone = goban.hasStone( myClickPoint );

		// Nothing was held during the click; this is a Stone operation.
		//
		if( !shift && !control )
		{
			// If there is a stone, then remove it, otherwise add a white stone.
			//
			if( hasStone )
				goban.removeStone( myClickPoint );
			else
				goban.placeStone( wStone, myClickPoint );

			goban.forceRepaint();
		}
		else if( shift )
		{
			// Shift was held ... place a white stone, or toggle the current.
			//
			if( hasStone )
			{
				if( goban.hasWhiteStone( myClickPoint ) )
					goban.placeStone( bStone, myClickPoint );
				else
					goban.placeStone( wStone, myClickPoint );
			}
			else
				goban.placeStone( bStone, myClickPoint );

			goban.forceRepaint();
		}
	}

	public void mouseDragged( MouseEvent me )
	{
		Goban goban = getGoban();
		WhiteStone wStone = getWhiteStone();
		BlackStone bStone = getBlackStone();

		if( !goban.translateCoord( me.getX(), me.getY(), myClickPoint ) )
			return;

		boolean hasStone = goban.hasStone( myClickPoint );

		if( hasStone )
			return;

		if( (me.getModifiers() & MouseEvent.SHIFT_MASK) == MouseEvent.SHIFT_MASK )
			goban.placeStone( bStone, myClickPoint );
		else
			goban.placeStone( wStone, myClickPoint );

		goban.forceRepaint();
	}

	public void mouseMoved( MouseEvent me ) { }

	public void mouseClicked( MouseEvent me ) { }
	public void mouseReleased( MouseEvent me ) { }
	public void mouseExited( MouseEvent me ) { }
	public void mouseEntered( MouseEvent me ) { }

	/**
	 * Helper method. Used to attach the toolbar frame to this frame.
	 */
	private void attachToolbar()
	{
		Toolbar tb = getToolbar();
		tb.setVisible( true );
		tb.toFront();
	}

	public void componentMoved( ComponentEvent compEvent )
	{
		attachToolbar();
	}

	public void componentResized( ComponentEvent compEvent )
	{
		attachToolbar();
	}

	public void componentHidden( ComponentEvent compEvent )
	{
		getToolbar().setVisible( false );
	}

	public void componentShown( ComponentEvent compEvent )
	{
		attachToolbar();
	}

	private void setToolbar( Toolbar tb ) { myToolbar = tb; }
	private Toolbar getToolbar() { return myToolbar; }
}

