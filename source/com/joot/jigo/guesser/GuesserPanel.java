/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.guesser;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.io.IOException;
import java.io.InputStream;

import java.net.URL;

import com.joot.awt.util.AlertBox;

import com.joot.jigo.Goban;
import com.joot.jigo.GobanHighlighter;
import com.joot.jigo.JiGoApplet;
import com.joot.jigo.Point;

import com.joot.jigo.dataformats.GoGame;
import com.joot.jigo.dataformats.GameReader;

import com.joot.jigo.replayer.ReplayerPanel;

import com.joot.jigo.dataformats.tokens.MoveToken;
import com.joot.jigo.dataformats.tokens.WhiteMoveToken;

/**
 * The frame that houses a Goban.
 */
public class GuesserPanel extends ReplayerPanel
	implements MouseListener
{
  private static String PARAM_TALLY = "Tally Title";

	/** Indicates if there are moves remaining to the end. */
	private boolean myHasNextMove;

	/** Indicates if there are moves remaining to the beginning. */
	private boolean myHasPrevMove;

	private int
		goodGuess,
		badGuess;

	public GuesserPanel( JiGoApplet applet )
	{
		super( applet );

		// Stay in touch of the Goban's mouse clicks, and mouse drags.
		//
		getGoban().addMouseListener( this );

		new GobanHighlighter( getGoban() );
	}

	/**
	 * Called when the user has clicked to guess where the next move in the
	 * game was played.	The click struck one, the mouse went down,
	 * hickory-dickory dock!
	 */
	public void mousePressed( MouseEvent me )
	{
		Goban goban = getGoban();
		Point click = new Point();

		// Figure out the intersection where the user clicked.
		//
		if( !goban.translateCoord( me.getX(), me.getY(), click ) )
			return;

		// If the user didn't click on a valid spot, then don't show the next
		// move.	Might want to show a static "Illegal Move" box (it knows if
		// the user has told it to never show again).
		//
		if( invalidMove( click ) )
			return;

		// Show the next move (the guessed move).  This will automagically
		// disable the forward and back buttons when no moves remain.
		//
		showNextMove();

		// If there are any moves remaining, we can tally the guess.
		//
		if( hasNextMove() )
		{
			setGuess( getCurrentMove().pointEquals( click ) );
		}
		else
    {
			showTally();
    }
	}

	/**
	 * If the guess was good, then this increases the number of good moves
	 * guessed; otherwise the bad guesses go up.
	 *
	 * @param good - true means the guess was good.
	 */
	private void setGuess( boolean good )
	{
		if( good )
			incrGoodGuess();
		else
			incrBadGuess();
	}

	/**
	 * Shows the results to the user, resets the game.
	 */
	private void showTally()
	{
    StringBuffer result = new StringBuffer( 256 );

    result.append( "Correct: " );
    result.append( getGoodGuess() );
    result.append( "\n" );
    result.append( "Incorrect: " );
    result.append( getBadGuess() );
    result.append( "\n" );

    AlertBox tally = createAlertBox( PARAM_TALLY, result.toString() );
    tally.setVisible( true );

		setGoodGuess( 0 );
		setBadGuess( 0 );
		resetGame();
	}

	/**
	 * Asks the rules if the given location is not quite valid.
	 */
	private boolean invalidMove( Point point )
	{
		return false;
	}

	/**
	 * Called when the next move has been played.
	 */
	protected void hasNextMove( boolean enable )
	{
		super.hasNextMove( enable );
		myHasNextMove = enable;
	}

	/**
	 * Called when the previous move has been played.
	 */
	protected void hasPrevMove( boolean enable )
	{
		super.hasPrevMove( enable );
		myHasPrevMove = enable;
	}

	/**
	 * Tells the calling object if the Replayer has another move that can
	 * be displayed.
	 *
	 * @return true - There are no next moves to be played.
	 */
	public boolean hasNextMove()
	{
		return myHasNextMove;
	}

	/**
	 * Tells the calling object if the Replayer has a previous move that can
	 * be displayed.
	 *
	 * @return true - There are no previous moves to be played.
	 */
	public boolean hasPrevMove()
	{
		return myHasPrevMove;
	}

	public void setGame( GoGame game )
	{
		getGoban().removeAll();
	}

	private int getGoodGuess()
	{
		return this.goodGuess;
	}

	private int getBadGuess()
	{
		return this.badGuess;
	}

	private void setGoodGuess( int i )
	{
		this.goodGuess = i;
	}

	private void setBadGuess( int i )
	{
		this.badGuess = i;
	}

	private void incrGoodGuess()
	{
		setGoodGuess( getGoodGuess() + 1 );
	}

	private void incrBadGuess()
	{
		setBadGuess( getBadGuess() + 1 );
	}

	public void mouseClicked( MouseEvent me ) { }
	public void mouseReleased( MouseEvent me ) { }
	public void mouseExited( MouseEvent me ) { }
	public void mouseEntered( MouseEvent me ) { }
}

