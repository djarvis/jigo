/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionListener;

/**
 * Used to ask a Goban to highlight an empty intersection over which the mouse
 * is hovering.  Example usage:
 * <PRE>
 * Goban goban = new Goban();
 * GobanHighlighter highlighter = new GobanHighlighter( goban );
 * </PRE>
 *
 * A new feature is that the GobanHighlighter can now highlight legal moves,
 * provided it is instantiated with an instance of the Rules that watch
 * over the Goban that is to be highlighted.  For example:
 *
 * <PRE>
 * Rules rules = new Rules( getGoban() );
 * GobanHighlighter highlighter = new GobanHighlighter( rules );
 * </PRE>
 *
 * It is presumed that the Rules track the current player so as to establish
 * proper Ko highlighting behaviour.
 */
public class GobanHighlighter extends MouseAdapter
	implements MouseMotionListener
{
	// If the internal handle on the Rules points to null, then we cannot
	// ask the Rules if that's a legal move.  Instead it means a constructor
	// with only the Goban was called.  In order to maintain backwards
	// compatibility -- and other reasons -- we have to allow the Goban to
	// be highlighted without rules.  This means that this class will
	// behave as it did in the past: it highlights all intersections except
	// those with a stone.
	//
	private Rules rules;
	private Goban goban;

	private Point myHighlight = new Point();

	/**
	 * Creates a new GobanHighlighter, provided a Goban to highlight. The
	 * Goban must not be null.
	 *
	 * @param goban - The Goban to highlight on mouse moves.
	 */
	public GobanHighlighter( Goban goban )
	{
		setGoban( goban );
		startListening();
	}

	/**
	 * Creates a new GobanHighlighter, provided the Rules that watch over
	 * a Goban.  The Rules must not be null.  The Goban being observed by
	 * the Rules must also not be null.
	 *
	 * @param rules - The Rules whose Goban we want to begin highlighting
	 * when the mouse hovers over a legal move.
	 */
	public GobanHighlighter( Rules rules )
	{
		setRules( rules );
		startListening();
	}

	/**
	 * Starts listening for mouse clicks and mouse movements on the given
	 * Goban.
	 */
	private void startListening()
	{
		getGoban().addMouseListener( this );
		getGoban().addMouseMotionListener( this );
	}

	/**
	 * Stops listening for mouse clicks and mouse movements on the given
	 * Goban.
	 */
	private void stopListening()
	{
		getGoban().removeMouseListener( this );
		getGoban().removeMouseMotionListener( this );
	}

	/**
	 * Simply calls mouseMoved to simulate that the mouse has been moved.
	 */
	public void mouseDragged( MouseEvent me ) { mouseMoved( me ); }

	/**
	 * Simply calls mouseMoved to simulate that the mouse has been moved,
	 * which then adjusts the highlight to the proper position.
	 */
	public void mouseEntered( MouseEvent me ) { mouseMoved( me ); }

	/**
	 * Removes the highlighting.
	 */
	public void mouseExited( MouseEvent me ) { getGoban().clearHighlight(); }

	/**
	 * Called when the mouse has changed its position. If the mouse has moved
	 * to a spot that is already highlighted, nothing happens. Otherwise,
	 * the old highlighted spot is removed and the Goban is told to highlight
	 * a new location (directly translated from the given MouseEvent's
	 * coordinates).
	 * <P>
	 * Doesn't highlight a spot if a stone is there.
	 */
	public void mouseMoved( MouseEvent me )
	{
		Goban goban = getGoban();

		// If the boundaries were accepted, and the point translated, then
		// proceed to see if we should actually perform a highlight.
		//
		if( goban.translateCoord( me.getX(), me.getY(), myHighlight ) )
		{
			boolean highlight = canHighlight( myHighlight );

			// If the position has a stone, don't draw any highlight and remove the
			// highlight if present. Otherwise, if the highlighted spot is already
			// highlighted, then don't do anything. Finally, set the highlight if
			// nothing is highlighted and no stone is underneath.
			//
			if( highlight )
			{
				if( !goban.isHighlighted( myHighlight ) )
					goban.setHighlight( myHighlight );
			}
			else if( goban.isHighlighted() )
				goban.clearHighlight();
		}
	}

	/**
	 * Removes the highlighting if the Goban has a stone where the mouse
	 * was clicked. Otherwise it adds highlighting.  Of course, if the
	 * mouse clicks outside of the Goban (somehow!) then we don't need to
	 * add/remove highlighting.
	 */
	public void mouseClicked( MouseEvent me )
	{
		Goban goban = getGoban();

		if( goban.translateCoord( me.getX(), me.getY(), myHighlight ) )
		{
			if( canHighlight( myHighlight ) )
				goban.setHighlight( myHighlight );
			else
				goban.clearHighlight();
		}
	}

	/**
	 * Answers true if the given move may be highlighted.  This first checks
	 * to see if there are Rules for the Goban.  If so, then it asks the Rules
	 * if the given intersection is a legal play.  If there are no Rules, it
	 * simply asks the Goban if there's a stone at the location already.  If
	 * a stone exists, the intersection may not be highlighted.
	 *
	 * @param p - The intersection that highlighting is desired.
	 * @return true - The intersection represented by the given point may be
	 * highlighted.
	 */
	private boolean canHighlight( Point p )
	{
		Rules rules = getRules();
		return rules == null ? !getGoban().hasStone( p ) : rules.canPlay( p );
	}

	/**
	 * The Goban will always be set.  To determine how highlighting is to
	 * function, the internal Rules instance ('rules') will either be null
	 * or not null.
	 */
	private void setGoban( Goban goban )
	{
		this.goban = goban;
	}

	/**
	 * This will never return null -- it will either return the Goban that
	 * was created using the respective constructor, or return the Goban from
	 * the Rules that were given at construction time.
	 */
	private Goban getGoban()
	{
		return this.goban;
	}

	/**
	 * Only used once to set which Rules determine how the highlighting works.
	 */
	private void setRules( Rules rules )
	{
		this.rules = rules;
		setGoban( rules.getGoban() );
	}

	/**
	 * This will return null if the Goban is to be highlighted without
	 * taking into consideration the rules of the game.
	 */
	private Rules getRules()
	{
		return this.rules;
	}
}

