/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.presenter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.Toolkit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import com.joot.awt.widgets.ImageComponent;

import com.joot.jigo.BlackStone;
import com.joot.jigo.Goban;
import com.joot.jigo.JiGoApplication;
import com.joot.jigo.Markup;
import com.joot.jigo.Point;
import com.joot.jigo.Stone;
import com.joot.jigo.WhiteStone;

import com.joot.jigo.dataformats.GameLeaf;
import com.joot.jigo.dataformats.GameTree;
import com.joot.jigo.dataformats.GameReader;
import com.joot.jigo.dataformats.GoGame;

import com.joot.jigo.dataformats.tokens.GameToken;
import com.joot.jigo.dataformats.tokens.MoveToken;

import com.joot.jigo.dataformats.exceptions.DataFormatException;

import com.joot.jigo.dataformats.sgf.SGFReaderDriver;

import com.joot.util.encoder.PNGEncoder;

/**
 * Replays a Go game, from start to finish, taking up the entire screen.  There
 * is a custom delay between each move, as well as a longer delay when the
 * game ends.  Use the arrow keys to traverse forward and back through the
 * game; the space bar will toggle pausing.
 */
public class Presenter extends JiGoApplication
{
	private final static String DEFAULT_TITLE	= "JiGo Presenter by Dave Jarvis";
	private final static String DEFAULT_GAME	= "games/game-001.sgf";
	private final static String DEFAULT_SHOW_IMAGE = "images/picture.png";

	private final static int DEFAULT_PLAY_DELAY		= 7;
	private final static int DEFAULT_REPLAY_DELAY	= 21;

	private final static Markup MARK = new Markup( Markup.TRIANGLE );

	// A fair amount of space (kind of in pixels, but not quite) that surrounds
	// the top and bottom of the board.  Divide this number in two, and that's
	// roughly the mount.  But you have to take into account the size of the
	// window's frame itself, so it's kind of a futzy, fuzzy number.
	//
	private final static int PADDING = 100;

	private final static String PROP_TITLE				= "WINDOW_TITLE";
	private final static String PROP_GAME					= "SGF_FILE";
	private final static String PROP_PLAY_DELAY		= "PLAY_DELAY";
	private final static String PROP_REPLAY_DELAY	= "REPLAY_DELAY";
	private final static String PROP_SHOW_IMAGE		= "SHOW_IMAGE";

	private final static int STARTING_INDEX = 0;

	// Represents the current (most recently played) move on the board--it acts
	// as an index into the "myLeaves" Vector.
	//
	private int _moveIndex = STARTING_INDEX;

	// _playDelay = Length of time between moves.
	// _replayDelay = Length of time at the end of the game before restart.
	//
	private int
		_playDelay = DEFAULT_PLAY_DELAY,
		_replayDelay = DEFAULT_REPLAY_DELAY;

	// The moves to play (approx. 200 for a professional game).
	//
	private Vector<GameToken> _moves = new Vector<GameToken>( 200 );

	// Image to show on the screen, to add a splash of colour.
	//
	private Image _showImage;

	private boolean
		_running = true,
		_paused = false;

	private Thread _pauseThread = null;

	protected Presenter()
		throws IOException
	{
	}

	protected Presenter( String configFile )
		throws IOException
	{
		super( configFile );
	}

	/**
	 * Once everything is laoded and we're ready to rock, this method does
	 * the displaying of the game.
	 */
	private void run()
	{
		boolean interrupted = false;

		while( isRunning() )
		{
			if( isPaused() )
				interrupted = sleep( 500 );

			// If the game was interrupted, then we should play the next move.
			// This way pause remains in effect regardless of requesting the
			// next or previous move.
			//
			if( !isPaused() || interrupted )
			{
				if( playMove() )
					sleep( getPlayDelay() );
				else
				{
					sleep( getReplayDelay() );
					reset();
				}
			}
		}
	}

	/**
	 * Pauses for the given delay, in seconds.
	 *
	 * @param delay - Number of seconds to delay.
 	 */
	private boolean sleep( int delay )
	{
		try
		{
			setPauseThread( Thread.currentThread() );
			getPauseThread().sleep( delay * 1000 );
		}
		catch( InterruptedException ie )
		{
			return true;
		}

		return false;
	}

	private void wakeUp()
	{
		getPauseThread().interrupt();
	}

	protected void playNextMove()
	{
		wakeUp();
	}

	protected void playPrevMove()
	{
		// Reset the board and then play up to the previous move.
		//

		// setMoveIndex( getMoveIndex() - 2 );
		wakeUp();
	}

	private MoveToken getNextMove()
	{
		MoveToken move = null;

		try {
			move = (MoveToken)(getMoves().elementAt( getMoveIndex() ));
			setMoveIndex( getMoveIndex() + 1 );
		}
		catch( Exception e ) {
		}

		return move;
	}

	/**
	 * Resets the game to the first move.
 	 */
	private void reset()
	{
		setMoveIndex( STARTING_INDEX );
		getGoban().removeAllMarks();
		getGoban().removeAllStones();
	}

	/**
	 * Set up the user interface.  Basically, this slaps the Goban down and
	 * plasters the name of the game along the side bar.
	 */
	protected void initGUI() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds( 0, 0, screenSize.width, screenSize.height );
		setBackground( java.awt.Color.black );

		Container content = getContentPane();
		Goban goban = getGoban();
		Image image = getShowImage();

		// From the preferred size of the Goban, we know how much space
		// remains at the left and right side.  From that, we scale the
		// image we want to show with an appropriate aspect ratio.
		//
		// Take the width of the frame and substract off the Goban's width.
		// Divide this by two to figure out how much space is on either side.
		//
		int width = (getSize().width - goban.getSize().width) / 2;

		// Now that we know how much space we can play with, figure out
		// the ratio of the width we have to the width the image requires.
		//
		float ratio = (float)width / (float)image.getWidth( this );
		int height = (int)(image.getHeight( this ) * ratio);

		Component showImage = new ImageComponent( image, width, height );

    Panel imagePanel = new Panel();
    imagePanel.setBackground( Color.black );
    imagePanel.setPreferredSize( new Dimension( width, height ) );
    imagePanel.setMaximumSize( imagePanel.getPreferredSize() );
    imagePanel.add( showImage );

    Panel gobanPanel = new Panel();
    gobanPanel.setMaximumSize( goban.getPreferredSize() );
		gobanPanel.setBackground( Color.black );
    gobanPanel.add( goban );

    content.setBackground( Color.black );
    content.add( java.awt.BorderLayout.WEST, imagePanel );
    content.add( java.awt.BorderLayout.CENTER, gobanPanel );

		setDefaultCloseOperation( EXIT_ON_CLOSE );
		setResizable( false );
		setVisible( true );
	}

	/**
	 * Given a list of properties, this will reset the Presenter to
	 * those values.
	 */
	protected void init( Properties properties )
		throws IOException, DataFormatException
	{
		super.init( properties );

		setTitle( properties.getProperty( PROP_TITLE, DEFAULT_TITLE ) );
		extractMoves( properties.getProperty( PROP_GAME, DEFAULT_GAME ) );

		try
		{
			setPlayDelay( Integer.parseInt( properties.getProperty(
					PROP_PLAY_DELAY, "" + DEFAULT_PLAY_DELAY ) ) );
			setReplayDelay( Integer.parseInt( properties.getProperty(
					PROP_REPLAY_DELAY, "" + DEFAULT_REPLAY_DELAY ) ) );
		}
		catch( NumberFormatException nfe ) { }

		setShowImage( properties.getProperty(
			PROP_SHOW_IMAGE, DEFAULT_SHOW_IMAGE ) );
	}

	/** 
	 * Plasters the given move on the goban. MoveTokens contain the X and Y
	 * location of the move as well as in indication of their colour.
	 *
	 * @param move - A move that should be placed on the board.
	 * @return true - A move was played.
	 */
	private boolean playMove()
	{
		MoveToken move = getNextMove();
		boolean result = true;

		if( move != null )
		{
			Point point = new Point();

			// Cheat a little bit by directly accessing the member variable, since
			// the "placeStone" method requires a Point object to indicate where
			// a stone should be placed.
			//
			point.x = move.getX() - 1;
			point.y = move.getY() - 1;

			Goban goban = getGoban();

			goban.removeAllMarks();

			goban.placeStone(
				move.isWhite() ? (Stone)getWhiteStone() : (Stone)getBlackStone(),
				point );

    	MARK.setColour( move.isWhite() ? Color.black : Color.white );
    	goban.placeMark( MARK, point );

			goban.forceRepaint();
		}
		else
			result = false;

		return result;
	}

	private void extractMoves( String file )
		throws IOException, DataFormatException
	{
		GoGame game = GameReader.read( new FileInputStream( file ) );
		extractMoves( game.getTree() );
	}

	private void extractMoves( GameTree tree )
	{
		Vector<GameToken> moves = getMoves();
		Enumeration leaves = tree.getLeaves();

		while( (leaves != null) && leaves.hasMoreElements() )
		{
			GameLeaf leaf = (GameLeaf)(leaves.nextElement());

			// Slap the list of moves from this leaf into the vector.
			//
			for( Enumeration tokens = leaf.getTokens(); tokens.hasMoreElements(); )
			{
				GameToken token = (GameToken)(tokens.nextElement());

				if( token instanceof MoveToken ) {
					moves.addElement( token );
        }
			}
		}

		// If there are variations in this file, extract only the first of each
		// branch.  That will yield the first set of moves.
		//
		Enumeration trees = tree.getTrees();

		if( (trees != null) && trees.hasMoreElements() )
			extractMoves( (GameTree)(trees.nextElement()) );
	}

	private void setMoveIndex( int index )
	{
		if( index < 0 )
			index = 0;

		_moveIndex = index;
	}

	private int getMoveIndex()
	{
		return _moveIndex;
	}

	private void setPlayDelay( int delay )
	{
		_playDelay = delay;
	}

	private void setReplayDelay( int delay )
	{
		_replayDelay = delay;
	}

	private int getPlayDelay()
	{
		return _playDelay;
	}

	private int getReplayDelay()
	{
		return _replayDelay;
	}

	protected int getStoneWidth()
	{
		return getStoneHeight() - 2;
	}

	protected int getStoneHeight()
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		return (int)((screenSize.getHeight() - PADDING) / getGobanSize());
	}

	private void setMoves( Vector<GameToken> moves )
	{
		_moves = moves;
	}

	private Vector<GameToken> getMoves()
	{
		return _moves;
	}

	private void setShowImage( String showImage )
		throws IOException
	{
		_showImage = readImage( showImage );
	}

	private Image getShowImage()
	{
		return _showImage;
	}

	private Thread getPauseThread()
	{
		return _pauseThread;
	}

	private void setPauseThread( Thread thread )
	{
		_pauseThread = thread;
	}

	protected boolean isPaused()
	{
		return _paused;
	}

	protected void setPaused( boolean paused )
	{
		_paused = paused;
	}

	protected void togglePause()
	{
		if( isPaused() )
			wakeUp();

		setPaused( !isPaused() );
	}

	private boolean isRunning()
	{
		return _running;
	}

	protected void stopRunning()
	{
		_running = false;
		setPaused( false );
		wakeUp();
	}

	protected void setGoban( Goban goban )
	{
		goban.setForceRepaint( false );
		super.setGoban( goban );
	}

	/**
	 * Runs JiGo Presenter.
	 */
	public static void main( String args[] )
		throws IOException, InterruptedException
	{
		Presenter presenter;

		if( args.length == 0 )
			presenter = new Presenter();
		else
			presenter = new Presenter( args[0] );

		new KeyboardControls( presenter );

		presenter.load();
		presenter.initGUI();
		presenter.run();

		presenter.setVisible( false );
		presenter.dispose();
	}
}

