
/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.presenter;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyboardControls extends KeyAdapter
{
	private Presenter _presenter;

	public KeyboardControls( Presenter presenter )
	{
		setPresenter( presenter );
		presenter.addKeyListener( this );
	}

	public void keyReleased( KeyEvent event )
	{
		switch( event.getKeyCode() )
		{
			case KeyEvent.VK_Q:
			case KeyEvent.VK_ESCAPE:
				getPresenter().stopRunning();
				break;

			case KeyEvent.VK_SPACE:
				getPresenter().togglePause();
				break;

			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_KP_LEFT:
				getPresenter().playPrevMove();
				break;

			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_KP_RIGHT:
				getPresenter().playNextMove();
				break;
		}
	}

	private void setPresenter( Presenter presenter )
	{
		_presenter = presenter;
	}

	private Presenter getPresenter()
	{
		return _presenter;
	}
}

