/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.replayer;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.Rectangle;

import com.joot.jigo.JiGoApplet;

import com.joot.awt.util.CloseableFrame;

/**
 * Used to replay a game of Go. Useful for reviewing professional games, or
 * going over one's own games.
 */
public class Replayer extends JiGoApplet
{
	/** A frame to be displayed for showing the contents of a Go game. */
	private Frame frame;

  /** The panel that will go into the frame. */
  private Panel panel;

  public Replayer()
  {
  }

	/**
	 * Start the ball rolling, get the show on the road, the browser is about
	 * to rev the applet up into full speed.
	 */
	public void init()
	{
		super.init();
    initGUI();
	}

  /**
   * Once the applet is ready, automatically load up a game (if present).
   */
  public void start()
  {
    ((ReplayerPanel)getPanel()).autoLoadGame();
  }

	/**
	 * The browser has left the building.
	 */
	public void stop()
	{
    if( !embed() )
    {
      Frame frame = getFrame();

      if( frame.isVisible() )
      {
        frame.setVisible( false );
        frame.dispose();
      }
    }

		super.stop();
	}

  /**
   * Determines whether or not the applet should be embedded into the web
   * page. If embedded, be sure to set the width and height applet atg
   * attributes accordingly.
   */
  private void initGUI()
  {
    Container c = getContainer();

    initGUI( c );

    if( !embed() )
    {
      Frame f = getFrame();
      f.pack();
      f.setResizable( false );
    }

    c.setVisible( true );
  }

  private Container getContainer() {
    return embed() ? (Container)this : (Container)getFrame();
  }

  private void initGUI( Container c )
  {
    c.add( getPanel() );
  }

  public void setVisible( boolean visible )
  {
    super.setVisible( visible );
  }

  public Dimension getPreferredSize() { return getPanel().getSize(); }

  public Dimension getMinimumSize() { return getPanel().getMinimumSize(); }

  public Dimension getMaximumSize() { return getPanel().getMaximumSize(); }

	/**
	 * Subclasses may override this method in order to return a different
	 * type of Frame.
	 */
	protected Frame createFrame()
	{
		return new CloseableFrame( getTitle() );
	}

	/**
	 * Uses lazy initialisation to return a valid frame.
	 */
	protected Frame getFrame()
	{
		if( this.frame == null )
    {
			this.frame = createFrame();
    }

		return frame;
	}

	/**
   * Subclasses can override this to return a different ReplayerPanel subclass.
	 */
	protected Panel createPanel()
	{
		return new ReplayerPanel( this );
	}

	/**
	 * Uses lazy initialisation to return a valid frame.
	 */
	protected Panel getPanel()
	{
		if( this.panel == null )
    {
			this.panel = createPanel();
    }

		return panel;
	}

	/**
	 * Returns the text that appears in the title bar for the frame.
	 *
	 * @return A string that is shown in the title bar when the frame is visible.
	 */
	protected String getTitle()
	{
		return "JiGo Replayer";
	}

	/**
	 * Run in application mode.
	 */
	public static void main( String args[] )
	{
		Replayer replayer = new Replayer();
		replayer.init();
	}
}

