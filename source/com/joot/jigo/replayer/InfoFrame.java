/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.replayer;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.TextArea;

import java.util.Enumeration;
import java.util.Observable;
import java.util.Observer;

import com.joot.jigo.Rules;

import com.joot.jigo.dataformats.GoGame;

import com.joot.jigo.dataformats.tokens.*;

/**
 * Shows information about a game; keeps up to date on the number of
 * captures by observing the given Rules class.
 */
public class InfoFrame extends Frame implements Observer
{
	/** Text shown when something isn't known about a game ... */
	public final static String UNKNOWN = "Unknown";
	
	/** When the komi is unknown, presume 0.0. */
	public final static String DEFAULT_KOMI = "0.0";

	private TextArea myCommentArea =
			new TextArea( "", 5, 50, TextArea.SCROLLBARS_VERTICAL_ONLY );

	private Label
		myPlace					= new Label( UNKNOWN ),
		myDate					= new Label( UNKNOWN ),
		myEvent					= new Label( UNKNOWN ),
		myRound					= new Label( UNKNOWN ),
		myResult				= new Label( UNKNOWN ),
		myKomi					= new Label( UNKNOWN ),
		myWhiteCaptures = new Label( UNKNOWN ),
		myBlackCaptures = new Label( UNKNOWN );

	/**
	 * An InfoFrame shows information about a game of Go loaded from a file.
	 *
	 * @param game - The GoGame whose information should be shown.
	 */
	public InfoFrame( GoGame game )
	{
		// The very first thing we have to do is lay out the component, so that
		// when we display information about the game, we're accessing valid
		// widgets.
		//
		initGUI();

		// Now we can set information about the game.
		//
		setGoGame( game );
	}

	/**
	 * <PRE>
	 * +-----------------------------------+
	 * | White vs Black              _ H X |
	 * +-----------------------------------+
	 * | Place:             Date:          |
	 * | Event:             Round:         |
	 * | Result:            Komi:          |
	 * |                                   |
	 * | White Captures:                   |
	 * | Black Captures:                   |
	 * |                                   |
	 * | Comment:                          |
	 * | +-------------------------------+ |
	 * | |															 | |
	 * | |															 | |
	 * | |															 | |
	 * | |															 | |
	 * | |															 | |
	 * | +-------------------------------+ |
	 * +-----------------------------------+
	 * </PRE>
	 *
	 * The title bar shows the player names (white always first). The next
	 * eight fields are fairly obvious. The Comment field serves a dual
	 * purpose. The beginning of each game may contain a game comment. Before
	 * the first move is played, the game comment is revealed here. After the
	 * first move is played, the text changes to reveal comments related to
	 * the currently played move (including moves in variations).
	 */	
	private void initGUI()
	{
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		gbc.gridwidth = 1;
		gbc.ipadx = 4;
		gbc.ipady = 2;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.WEST;
		setLayout( gbl );

		// Add the labels ...
		//
		Label label = new Label( "Place:" );

		gbc.gridheight = 1;
		gbl.setConstraints( label, gbc );
		add( label );
		gbl.setConstraints( myPlace, gbc );
		add( myPlace );
		
		label = new Label( "Date:" );
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbl.setConstraints( label, gbc );
		add( label );
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbl.setConstraints( myDate, gbc );
		add( myDate );
		
		label = new Label( "Event:" );
		gbc.gridwidth = 1;
		gbl.setConstraints( label, gbc );
		add( label );
		gbl.setConstraints( myEvent, gbc );
		add( myEvent );
		
		label = new Label( "Round:" );
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbl.setConstraints( label, gbc );
		add( label );
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbl.setConstraints( myRound, gbc );
		add( myRound );
		
		label = new Label( "Result:" );
		gbc.gridwidth = 1;
		gbl.setConstraints( label, gbc );
		add( label );
		gbl.setConstraints( myResult, gbc );
		add( myResult );
		
		label = new Label( "Komi:" );
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbl.setConstraints( label, gbc );
		add( label );
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbl.setConstraints( myKomi, gbc );
		add( myKomi );

		label = new Label( "White Captures:" );
		gbc.gridwidth = 1;
		gbl.setConstraints( label, gbc );
		add( label );
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbl.setConstraints( myWhiteCaptures, gbc );
		add( myWhiteCaptures );

		label = new Label( "Black Captures:" );
		gbc.gridwidth = 1;
		gbl.setConstraints( label, gbc );
		add( label );
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbl.setConstraints( myBlackCaptures, gbc );
		add( myBlackCaptures );

		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridwidth = 4;
		gbc.gridheight = GridBagConstraints.REMAINDER;
		gbc.anchor = GridBagConstraints.SOUTH;
		gbc.fill = GridBagConstraints.BOTH;

		myCommentArea.setEditable( false );
		gbl.setConstraints( myCommentArea, gbc );
		add( myCommentArea );

		// Make it big so that the packing works.
		//
		setSize( 600, 600 );
		pack();
	}

	/**
	 * We get notified when the number of captures changes.
	 */
	public void update( Observable observedRules, Object unused )
	{
		Rules rules = (Rules)observedRules;
		
		setWhiteDead( rules.getWhiteCaptures() );
		setBlackDead( rules.getBlackCaptures() );
	}

	/**
	 * Called to change the game we're currently watching. Since the
	 * captures are reset when the game is refreshed, and since we're
	 * getting notified of changes to the number of captures by the Rules
	 * class itself, we needn't reset the number of captures here, as it
	 * will be done automagically in the update method.
	 */
	public void setGoGame( GoGame game )
	{
		Enumeration e = game.getInfoTokens();

		String
			white = UNKNOWN,
			black = UNKNOWN;

		while( e.hasMoreElements() )
		{
			InfoToken it = (InfoToken)(e.nextElement());

			if( it instanceof WhiteNameToken )
				white = it.toString();
			else if( it instanceof BlackNameToken )
				black = it.toString();
			else if( it instanceof ResultToken )
				myResult.setText( it.toString() );
			else if( it instanceof CommentToken )
				setComment( it.toString() );
			else if( it instanceof KomiToken )
				myKomi.setText( it.toString() );
			else if( it instanceof PlaceToken )
				myPlace.setText( it.toString() );
			else if( it instanceof DateToken )
				myDate.setText( it.toString() );
			else if( it instanceof EventToken )
				myEvent.setText( it.toString() );
			else if( it instanceof RoundToken )
				myRound.setText( it.toString() );
		}

		// Change the title bar to reflect W vs B player.
		//
		setTitle( white + " (W) vs " + black + " (B)" );

		invalidate();
		repaint();
	}
	
	/**
	 * Updates the comment area with the given text string.
	 *
	 * @param comment - The new text to display in the comment text area.
	 */
	public void setComment( String comment )
	{
		if( !myCommentArea.getText().equals( comment ) )
			myCommentArea.setText( comment );
	}
	
	private void setWhiteDead( double dead )
	{
		myWhiteCaptures.setText( (new Double( dead )).toString() );
	}
	
	private void setBlackDead( double dead )
	{
		myBlackCaptures.setText( (new Double( dead )).toString() );
	}
}
