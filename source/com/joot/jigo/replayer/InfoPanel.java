
package com.joot.jigo.replayer;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Rectangle;

import java.io.File;
import java.io.IOException;

import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;

import com.joot.jigo.Rules;

import com.joot.awt.widgets.ImageComponent;

/**
 * Given a background image and its layout, this creates a nice template for
 * displaying important aspects about a game (namely its prisoners and
 * result).  Further, it contains a marquee for displaying just about anything
 * else.
 */
public class InfoPanel extends ImageComponent implements Runnable, Observer
{
	private final static int INVALID = 666;

	// Text displayed by default when the InfoPanel is created.
	//
	private final static String DEFAULT_TEXT =
		"JiGo by Dave Jarvis ... http://www.joot.com/jigo";

	private byte _marqueeBytes[];

	private Thread _marqueeThread = new Thread( this );

	private boolean _running = true;

	private Rectangle
		_whiteCapturesRect = new Rectangle( 195, 15, 35, 20 ),
		_blackCapturesRect = new Rectangle( 195, 55, 35, 20 ),
		_resultRect = new Rectangle( 185, 95, 40, 20 ),
		_marqueeRect = new Rectangle( 25, 140, 195, 20 );

  private int wCaptures = 0;
  private int bCaptures = 0;

	// To speed things up, keep only the original background for the marquee's
	// area, rather than the whole background image.
	//
	private Image _originalBackground;

	private int
		_horizIndex = INVALID;

	/**
	 * @param image - The background image for the panel.
	 */
	public InfoPanel( Image image )
	{
		super( image );
		setMarqueeText( DEFAULT_TEXT );
	}

	public void run()
	{
		while( isRunning() )
		{
			scroll( getGraphics() );
			sleep( getMarqueeDelay() );
		}
	}

	/**
	 * Called when we should update the number of captures.
	 */
	public void update( Observable observedRules, Object unused )
	{
		Rules rules = (Rules)observedRules;
		setWhiteCaptures( (int)rules.getWhiteCaptures() );
		setBlackCaptures( (int)rules.getBlackCaptures() );
	}

  protected int getMarqueeDelay() 
  {
    return 40;
  }

	/**
	 * Used to display a string within a specific region.
	 */
	private void display( Rectangle rect, String toDisplay )
	{
		Graphics g = getOffscreen().getGraphics();
		FontMetrics fm = g.getFontMetrics();
		byte text[] = toDisplay.getBytes();

		// Take the original image and bit blast the white prisoners area
		// onto the offscreen.
		//
		g.setClip( rect.x, rect.y, rect.width, rect.height );
		g.drawImage( getOriginalBackground(), 0, 0, null );
		g.drawBytes( text, 0, text.length,
			rect.x + fm.getLeading(),
			rect.y + fm.getAscent() );

		getGraphics().drawImage( getOffscreen(), 0, 0, null );

		g.setClip( 0, 0, getWidth(), getHeight() );
	}

	/**
	 * Updates the information image so that it shows the number of white
	 * prisoners.
	 */
	public void setWhiteCaptures( int count )
	{
    if( this.wCaptures != count )
    {
      this.wCaptures = count;
      display( getWhiteCapturesRect(), Integer.toString( count ) );
    }
	}

	/**
	 * Updates the information image so that it shows the number of black
	 * prisoners.
	 */
	public void setBlackCaptures( int count )
	{
    if( this.bCaptures != count )
    {
      this.bCaptures = count;
      display( getBlackCapturesRect(), Integer.toString( count ) );
    }
	}

	public void setResult( String result )
	{
		display( getResultRect(), result );
		repaint();
	}

	/**
	 * Scrolls the text within the marquee rectangle.
	 */
	private void scroll( Graphics g )
	{
    // Make it scroll ...
    //
    if( g != null )
    {
      // We need to limit where we draw.
      //
      Rectangle marquee = getMarqueeRect();

      // Get a handle on the offscreen graphics context.
      //
      Image image = getOffscreen();
      Graphics offscr = image.getGraphics();

      // Bit blast the marquee area onto our original background image.
      // This only roasts the area under the marquee; keeping the rest of
      // the image intact.
      //
      offscr.setClip( marquee.x, marquee.y, marquee.width, marquee.height );
      offscr.drawImage( getOriginalBackground(), 0, 0, null );
      drawText( offscr, marquee );

      g.drawImage( image, 0, 0, null );

      offscr.setClip( 0, 0, getWidth(), getHeight() );
    }
	}

  // Precaculated; only used in drawText method below.
  //
	private int textWidth;

	private void drawText( Graphics g, Rectangle marquee )
	{
		FontMetrics fm = g.getFontMetrics();
		byte text[] = getMarqueeBytes();
		int horizIndex = getHorizontalIndex();

		if( horizIndex == INVALID )
		{
			horizIndex = marquee.width;
			textWidth = fm.bytesWidth( text, 0, text.length );
		}

		g.drawBytes( text, 0, text.length,
			marquee.x + fm.getLeading() + horizIndex,
			marquee.y + fm.getAscent() );

		if( --horizIndex + marquee.width + textWidth < 0 )
			horizIndex = marquee.width;

		setHorizontalIndex( horizIndex );
	}

	private void sleep( int ms )
	{
		try
		{
			getMarqueeThread().sleep( ms );
		}
		catch( InterruptedException ie ) { }
	}

	/**
	 * Causes the text area to begin scrolling some words.
	 */
	public void startMarquee()
	{
		setRunning( true );

		if( !getMarqueeThread().isAlive() )
			getMarqueeThread().start();
	}

	/**
	 * Causes the text area to stop scrolling.
	 */
	public void stopMarquee()
	{
		setRunning( false );
	}

	private int getHorizontalIndex() { return _horizIndex; }
	private void setHorizontalIndex( int index ) { _horizIndex = index; }

	private boolean isRunning() { return _running; }
	private void setRunning( boolean status ) { _running = status; }

	private void setResultRect( Rectangle r ) { _resultRect = r; }
	private Rectangle getResultRect() { return _resultRect; }

	private void setWhiteCapturesRect( Rectangle r ) { _whiteCapturesRect = r; }
	private Rectangle getWhiteCapturesRect() { return _whiteCapturesRect; }

	private void setBlackCapturesRect( Rectangle r ) { _blackCapturesRect = r; }
	private Rectangle getBlackCapturesRect() { return _blackCapturesRect; }

	private void setMarqueeRect( Rectangle r ) { _marqueeRect = r; }
	private Rectangle getMarqueeRect() { return _marqueeRect; }

	/**
	 * Use this method to change the text displayed in the marquee.
	 */
	public void setMarqueeText( String s )
	{
		setMarqueeBytes( s.getBytes() );
		setHorizontalIndex( INVALID );
	}

	private void setMarqueeBytes( byte b[] ) { _marqueeBytes = b; }
	private byte[] getMarqueeBytes() { return _marqueeBytes; }

	private void setOriginalBackground( Image i ) { _originalBackground = i; }
	private Image getOriginalBackground()
	{
		if( _originalBackground == null )
		{
			_originalBackground = createImage( getWidth(), getHeight() );
			_originalBackground.getGraphics().drawImage( getImage(), 0, 0, null );
		}

		return _originalBackground;
	}

	private void setMarqueeThread( Thread t ) { _marqueeThread = t; }
	private Thread getMarqueeThread() { return _marqueeThread; }
}

