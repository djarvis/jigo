/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo.replayer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.TextArea;
import java.awt.Toolkit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.io.InputStream;
import java.io.IOException;

import java.net.URL;
import java.net.MalformedURLException;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.joot.awt.widgets.ToolbarPanel;
import com.joot.awt.widgets.ImageButton;

import com.joot.awt.util.AlertBox;
import com.joot.awt.util.FileDialog;
import com.joot.awt.util.PnutsLayout;

import com.joot.jigo.BlackStone;
import com.joot.jigo.Goban;
import com.joot.jigo.GobanPanel;
import com.joot.jigo.JiGoApplet;
import com.joot.jigo.Point;
import com.joot.jigo.Markup;
import com.joot.jigo.Rules;
import com.joot.jigo.Stone;
import com.joot.jigo.WhiteStone;

import com.joot.jigo.dataformats.GoGame;
import com.joot.jigo.dataformats.GameLeaf;
import com.joot.jigo.dataformats.GameReader;
import com.joot.jigo.dataformats.GameReaderDriver;
import com.joot.jigo.dataformats.GameTree;
import com.joot.jigo.dataformats.exceptions.DataFormatException;

import com.joot.jigo.dataformats.tokens.*;

/**
 * Houses the Goban that shows the game which is being replayed.
 */
public class ReplayerPanel extends GobanPanel
  implements ActionListener, KeyListener
{
  private final static String IMG_EXT = ".png";

  private final static String TB_DIRECTORY    = "images/toolbar/";
  private final static String INFO_PANEL      = "images/info-panel" + IMG_EXT;

  private final static String PARAM_OPEN_GAME_TITLE   = "Open Game Title";
  private final static String PARAM_OPEN_GAME_FILE    = "Open Game File";
  private final static String PARAM_OPEN_GAME_ENABLED = "Open Game Enabled";
  private final static String PARAM_OPEN_GAME_FAILED  = "Open Game Failed";

  private final static String PARAM_OPEN_GAME_SUPPRESS_ERROR = "Open Game Suppress Error";
  private final static String PARAM_OPEN_GAME_FAILED_TITLE = "Open Game Failed Title";

  private final static String PARAM_ROOT_DIR          = "Root Directory";
  private final static String PARAM_EMBED_APPLET      = "Embed Applet";

  private final static String PARAM_MARQUEE_TEXT      = "Marquee Text";

  private final static char STARTING_LETTER_CHAR = 'a';

  /** Value for the _moveIndex to reset it. */
  private final static int STARTING_INDEX = 0;

  /** Action commands for the action event; numbers faster than strings. */
  private final static int
    OPEN  = 1,
    NEXT  = 2,
    BACK  = 3,
    START = 4,
    END   = 5;

  private final static String
    OPEN_ENABLED_ICON   = "tb-open" + IMG_EXT,
    START_ENABLED_ICON  = "tb-start" + IMG_EXT,
    BACK_ENABLED_ICON   = "tb-back" + IMG_EXT,
    NEXT_ENABLED_ICON   = "tb-next" + IMG_EXT,
    END_ENABLED_ICON    = "tb-end" + IMG_EXT,
    OPEN_DISABLED_ICON  = "tb-open-disabled" + IMG_EXT,
    START_DISABLED_ICON = "tb-start-disabled" + IMG_EXT,
    END_DISABLED_ICON   = "tb-end-disabled" + IMG_EXT,
    NEXT_DISABLED_ICON  = "tb-next-disabled" + IMG_EXT,
    BACK_DISABLED_ICON  = "tb-back-disabled" + IMG_EXT;

  private final static Markup CIRCLE_MARK = new Markup( Markup.CIRCLE );

  /**
   * This variable is used only for optimization purposes. It's used for
   * two things: placing a move at a given location and placing board markup
   * at that same location. When a move is played on the board, this
   * variable contains the X and Y values for that move. It then is used
   * immediately after a call to "playMove" (which is where the values get
   * set in the first place) to put a board markup on the most recently
   * played move.
   */
  private static Point point = new Point();

  private Rules _rules;

  /** A place to hold the controlling buttons. */
  private ToolbarPanel _toolbar;

  /** The list of leaves for the entire game, without variations. */
  private Vector<GameLeaf> myLeaves = new Vector<GameLeaf>( 200, 5 );

  /** Used to open a game; remembers the last game that was opened. */
  private FileDialog fileDialog;

  /**
   * Represents the current (most recently played) move on the board--it acts
   * as an index into the "myLeaves" Vector.
   */
  private int _moveIndex = STARTING_INDEX;

  /** Controlling buttons. */
  private ImageButton
    _openButton,
    _startButton,
    _nextButton,
    _backButton,
    _endButton;

  private AddBlackToken _blackStones;
  private AddWhiteToken _whiteStones;

  /** Displays information about the game; including comments. */
  private InfoPanel _infoPanel;

  private TextArea commentArea;

  public ReplayerPanel( JiGoApplet applet )
  {
    super( applet );

    // The rules have to apply to the board in order for stones to be
    // removed when captured.
    //
    setRules( new Rules( getGoban() ) );

    // Register the SGF Game Reader with the Game Reader Driver, and
    // ensure all token classes get compiled.
    //
    GameReader.registerDriver(
      new com.joot.jigo.dataformats.sgf.SGFReaderDriver() );
  }

  protected void initGUI()
  {
    Image infoPanel = null;

    try
    {
      infoPanel = loadImage( INFO_PANEL );
      setInfoPanel( createInfoPanel( infoPanel ) );
      setToolbar( createToolbarPanel() );
      initToolbar();
    }
    catch( Exception e ) { }

    setLayout( new BorderLayout() );
    add( getGoban(), BorderLayout.WEST );

    Panel p = new Panel();
    p.setLayout( new PnutsLayout( "cols=1, halign=fill, valign=fill" ) );

    p.add( getInfoPanel() );
    p.add( getCommentArea(), "expand=xy" );
    p.add( getToolbar(), "expand=x" );
    add( p, BorderLayout.EAST );

    // Centre the goban on the screen.
    //
    //centreGoban();

    getGoban().addKeyListener( this );
    getToolbar().addKeyListener( this );
    getInfoPanel().addKeyListener( this );
    getCommentArea().addKeyListener( this );
  }

  private void centreGoban()
  {
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    Rectangle r = getBounds();
    r.x = (d.width >> 1) - (r.width >> 1);
    r.y = (d.height >> 1) - (r.height >> 1);
    setBounds( r );
  }

  protected InfoPanel createInfoPanel( Image panel )
  {
    return new InfoPanel( panel );
  }

  protected ToolbarPanel createToolbarPanel()
  {
    return new ToolbarPanel();
  }

  /**
   * By default Gobans are highlighted, so we want to disable that behaviour.
   *
   * @return false - Do not enable highlighting of the Goban for the mouse.
   */
  protected boolean shouldHighlight() { return false; }

  private void initToolbar()
    throws IOException, MalformedURLException, InterruptedException
  {
    ToolbarPanel tb = getToolbar();

    tb.setBackground( Color.lightGray );

    if( openEnabled() )
    {
      _openButton = new ImageButton(
        loadImage( TB_DIRECTORY + OPEN_ENABLED_ICON ),
        loadImage( TB_DIRECTORY + OPEN_DISABLED_ICON ),
        Integer.toString( OPEN ) );
      _openButton.setEnabled( true );
      tb.addButton( _openButton );
    }

    _startButton = new ImageButton(
      loadImage( TB_DIRECTORY + START_ENABLED_ICON ),
      loadImage( TB_DIRECTORY + START_DISABLED_ICON ),
      Integer.toString( START ) );
    _startButton.setEnabled( false );
    tb.addButton( _startButton );

    _backButton = new ImageButton(
      loadImage( TB_DIRECTORY + BACK_ENABLED_ICON ),
      loadImage( TB_DIRECTORY + BACK_DISABLED_ICON ),
      Integer.toString( BACK ) );
    _backButton.setEnabled( false );
    tb.addButton( _backButton );

    _nextButton = new ImageButton(
      loadImage( TB_DIRECTORY + NEXT_ENABLED_ICON ),
      loadImage( TB_DIRECTORY + NEXT_DISABLED_ICON ),
      Integer.toString( NEXT ) );
    _nextButton.setEnabled( false );
    tb.addButton( _nextButton );

    _endButton = new ImageButton(
      loadImage( TB_DIRECTORY + END_ENABLED_ICON ),
      loadImage( TB_DIRECTORY + END_DISABLED_ICON ),
      Integer.toString( END ) );
    _endButton.setEnabled( false );
    tb.addButton( _endButton );

    tb.addActionListener( this );
  }

  private void showDefaultCursor()
  {
    setCursor( Cursor.getPredefinedCursor( Cursor.DEFAULT_CURSOR ) );
  }

  private void showWaitCursor()
  {
    setCursor( Cursor.getPredefinedCursor( Cursor.WAIT_CURSOR ) );
  }

  public void keyTyped( KeyEvent e ) { } 
  public void keyReleased( KeyEvent e ) { } 

  public void keyPressed( KeyEvent event )
  {
    switch( event.getKeyCode() )
    {
      case KeyEvent.VK_O    : openFile();     break;
      case KeyEvent.VK_UP   : showStart();    break;
      case KeyEvent.VK_DOWN : showEnd();      break;

      case KeyEvent.VK_KP_LEFT:
      case KeyEvent.VK_LEFT : showPrevMove(); break;

      case KeyEvent.VK_KP_RIGHT:
      case KeyEvent.VK_RIGHT: showNextMove(); break;
    }
  }

  public void actionPerformed( ActionEvent event )
  {
    if( event.getSource() instanceof ImageButton )
    {
      switch( Integer.parseInt( event.getActionCommand() ) )
      {
        case OPEN : openFile();     break;
        case START: showStart();    break;
        case END  : showEnd();      break;
        case BACK : showPrevMove(); break;
        case NEXT : showNextMove(); break;
      }
    }
    else
    {
      loadGame( event.getActionCommand() );
      getGoban().requestFocus();
    }
  }

  /**
   * Loads up the Go file, given a particular file name. The file must be
   * on the web somewhere ...
   */
  private void loadGame( String file ) {
    Exception error = null;

    try {
      showWaitCursor();

      URL sgfURL = getURL( file );
      InputStream is = sgfURL.openStream();
      GoGame game = GameReader.read( is );
      is.close();

      // Clear the current game only after a successful load.
      //
      getGoban().removeAllStones();
      getGoban().removeAllMarks();

      // Place the board into its initial state (handis and such).
      //
      setGoGame( game );

      // Only if we load the game do we want to enable the toolbar icons.
      //
      hasNextMove( true );
      hasPrevMove( false );

      // Start the scrolling of the game's player information.
      //
      getInfoPanel().startMarquee();
    }
    catch( Exception exception ) {
      error = exception;
      exception.printStackTrace();
    }
    finally {
      // Put the cursor back to normal.
      //
      showDefaultCursor();
    }

    // If there was an exception, bring it to someone's attention.
    //
    if( (error != null) && !suppressFailure() ) {
      showError( file, error );
    }

    // Empty out the text area, in case the new file has no initial comment.
    //
    getCommentArea().setText( null );
  }

  /**
   * Displays an error regarding the file that could not be opened.
   */
  protected void showError( String file, String error ) {
    StringBuffer sb = new StringBuffer( 128 );

    sb.append( getErrorText() );
    sb.append( file );
    sb.append( "\n" );
    sb.append( error );

    AlertBox ab = createAlertBox( PARAM_OPEN_GAME_FAILED_TITLE, sb.toString() );
    ab.setVisible( true );
  }

  /**
   * Displays an error to an end-user stating what happened with the file
   * that could not be opened.
   *
   * @param file - The name of the file that was not opened.
   * @param error - The exception that prevented the file from being opened.
   */
  protected void showError( String file, Exception error ) {
    showError( file, error.toString() );
  }

  /**
   * Returns the text prefix to indicate that the loading of a game failed.
   */
  protected String getErrorText()
  {
    return getParam( PARAM_OPEN_GAME_FAILED );
  }

  /**
   * Returns the AlertBox that shows a load has failed.
   *
   * @param param - Used to obtain the title from an HTML parameter.
   * @param text - The text message to display in the alert box.
   */
  protected AlertBox createAlertBox( String param, String text )
  {
    return new AlertBox( new Frame(), getParam( param ), text );
  }

  /**
   * Returns true if open is enabled. The default is to be enabled.
   */
  protected boolean openEnabled()
  {
    return getBooleanParam( PARAM_OPEN_GAME_ENABLED, true );
  }

  /**
   * Indicates whether or not an alert box should be displayed on errors. The
   * default is to show errors.
   *
   * @return true - The error messages should not be shown.
   */
  protected boolean suppressFailure()
  {
    return getBooleanParam( PARAM_OPEN_GAME_SUPPRESS_ERROR, true );
  }

  /**
   * Returns the file dialog used to prompt the user for a file. Subclasses
   * can override this method to change the type of FileDialog that is
   * returned.
   */
  protected FileDialog createFileDialog()
    throws IOException {
    /*return new FileDialog(
      new Frame(),
      getParam( PARAM_OPEN_GAME_TITLE ),
      "",
      getURL( getParam( PARAM_ROOT_DIR ) ).toString() );*/

    return new FileDialog(
      new Frame(),
      getParam( PARAM_OPEN_GAME_TITLE ),
      getURL( getParam( PARAM_ROOT_DIR ) ) );
  }

  /**
   * We observe the FileDialog; it'll let us know if a file was opened.
   */
  private void openFile() {
    if( openEnabled() ) {
      FileDialog fd = null;

      try {
        fd = getFileDialog();
        fd.setVisible( true );
      }
      catch( IOException ioe ) {
        if( fd != null ) {
          fd.setVisible( false );
        }
      }
    }
  }

  private FileDialog getFileDialog()
    throws IOException {
    if( this.fileDialog == null ) {
      this.fileDialog = createFileDialog();
      this.fileDialog.addActionListener( this );
    }

    return this.fileDialog;
  }

  /**
   * When the applet starts, this attempts to load up a file specified by
   * the given name.
   */
  protected void autoLoadGame()
  {
    String file = getParam( PARAM_OPEN_GAME_FILE );

    if( file.length() > 0 ) {
      loadGame( file );
    }
  }

  /**
   * Shows both the comments and markup for this move.
   */
  protected void showNotes()
  {
    showComment();
    showMarkup();
  }

  /**
   * Shows the comment associated with the current move -- if one
   * so happens to exist.
   */
  private void showComment()
  {
    CommentToken ct = getCommentToken();
    setCommentArea( ct == null ? "" : ct.getComment() );
  }

  private void showMarkup()
  {
    GameLeaf leaf = getCurrentLeaf();

    for( Enumeration tokens = leaf.getTokens(); tokens.hasMoreElements(); )
    {
      GameToken gt = (GameToken)(tokens.nextElement());

      if( gt instanceof LetterToken )
      {
        char letter = STARTING_LETTER_CHAR;

        // Show all the Letter tokens ...
        //
        for( Enumeration points = ((LetterToken)gt).getPoints();
          points.hasMoreElements(); )
        {
          Markup markup = new Markup( Markup.LETTER );
          markup.setLetter( letter );

          // Markup is ZERO-based.
          //
          Point p = (Point)(points.nextElement());
          getGoban().placeMark( markup, p.x - 1, p.y - 1 );
          letter++;
        }
      }
    }
  }

  /** 
   * Plasters the given move on the goban. MoveTokens contain the X and Y
   * location of the move as well as in indication of their colour.
   *
   * @param mt - A move that should be placed on the board.
   */
  protected void playMove( MoveToken mt )
  {
    Goban goban = getGoban();
    Point p = this.point;

    // Cheat a little bit by directly accessing the member variable, since
    // the "placeStone" method requires a Point object to indicate where
    // a stone should be placed.
    //
    p.x = mt.getX() - 1;
    p.y = mt.getY() - 1;

    goban.placeStone(
      mt.isWhite() ? (Stone)getWhiteStone() : (Stone)getBlackStone(), p );
  }

  /**
   * Plays the next move for replaying this game of Go.
   */
  public void showNextMove()
  {
    MoveToken mt = getNextMove();
    Goban goban = getGoban();

    // If there is a move token left and it doesn't represent
    // a pass, then we want to play the move.
    //
    if( isPlayable( mt, goban.getBoardSize() ) )
    {
      playMove( mt );
      hasPrevMove( true );

      goban.removeAllMarks();
      markCurrentPoint();
    }

    // If no moves remain then disable the forward buttons.
    //
    hasNextMove( getMoveIndex() < getLeaves().size() );
    showNotes();
  }

  private void markCurrentPoint()
  {
    MoveToken mt = getCurrentMove();
    CIRCLE_MARK.setColour( mt.isWhite() ? Color.black : Color.white );
    getGoban().placeMark( CIRCLE_MARK, this.point );
  }

  /**
   * Resets the game to just before the first move and then shows the
   * empty Goban.
   */
  public void showStart()
  {
    resetGame();
    showNotes();
  }

  /**
   * Plays from the current move to the given move index.
   *
   * @param moveIndex - The move number that moves are played until.
   */
  private void playMovesTo( int moveIndex ) {
    Goban goban = getGoban();
    goban.setForceRepaint( false );

    InfoPanel info = getInfoPanel();

    getRules().deleteObserver( info );

    int size = goban.getBoardSize();

    // Zoooom!  Full speed ahead!  Display all the moves until the
    // given move index.  This goes quite rapidly since we aren't
    // refreshing the display (or comments, or captures) until the
    // last requested move has been placed on the Goban.
    //
    while( getMoveIndex() != moveIndex )
    {
      MoveToken mt = getNextMove();

      if( isPlayable( mt, size ) )
      {
        playMove( mt );
      }
    }

    goban.setForceRepaint( true );
    goban.removeAllMarks();
    goban.forceRepaint();

    getRules().addObserver( info );
  }

  private boolean isPlayable( MoveToken mt, int size )
  {
    return (mt != null) && !mt.isPass( size );

  }
  
  /**
   * Replays the game to the end and then shows the finished product.
   */
  public void showEnd()
  {
    playMovesTo( getLeaves().size() );
    markCurrentPoint();
    hasPrevMove( true );
    hasNextMove( false );
    showNotes();
  }

  /**
   * In order to show the previous move, we simply replay the game up to,
   * but not including, the current move.
   */
  public void showPrevMove()
  {
    int prevMoveIndex = getMoveIndex() - 1;

    resetGame();

    if( prevMoveIndex > STARTING_INDEX )
    {
      playMovesTo( prevMoveIndex );
      markCurrentPoint();
    }

    hasPrevMove( prevMoveIndex > STARTING_INDEX );
    showNotes();
    showMarkup();
  }

  /**
   * Resets the Goban to empty, points to the first move, disables the
   * previous buttons, and finally sets the captures to ziltch.
   */
  public void resetGame()
  {
    getGoban().initializeBoard();
    setMoveIndex( STARTING_INDEX );
    hasNextMove( true );
    hasPrevMove( false );
    getRules().resetCaptures();

    // Some games have handicap, or are set at a specific position before
    // the rest of the moves come out.  So this will place them on the
    // board.
    //
    placeStartingStones();
  }

  /**
   * Called to add initial starting stones to the board. These are usually
   * handicap stones, but it could equally well be a board in mid-game.
   * Also handy for tsume-go.
   */
  private void placeStartingStones()
  {
    Goban goban = getGoban();
    BlackStone bs = getBlackStone();
    WhiteStone ws = getWhiteStone();
    Enumeration e;

    goban.setForceRepaint( false );

    if( (e = getBlackSetup()) != null )
      while( e.hasMoreElements() )
      {
        Point p = (Point)e.nextElement();
        this.point.x = p.x - 1;
        this.point.y = p.y - 1;
        goban.placeStone( bs, this.point );
      }

    if( (e = getWhiteSetup()) != null )
      while( e.hasMoreElements() )
      {
        Point p = (Point)e.nextElement();
        this.point.x = p.x - 1;
        this.point.y = p.y - 1;
        goban.placeStone( ws, this.point );
      }

    goban.setForceRepaint( true );
    goban.forceRepaint();
  }

  /**
   * Given a GameTree this method weeds the leaves from the GameTree into
   * a single Vector of leaves, with one move per leaf. Variations are
   * discarded.
   *
   * @param tree - The GameTree containing a variation tree.
   */
  private void extractMoves( GameTree tree )
  {
    Enumeration
      trees = tree.getTrees(),
      leaves = tree.getLeaves();
    Vector<GameLeaf> moves = getLeaves();

    while( (leaves != null) && leaves.hasMoreElements() )
    {
      GameLeaf leaf = (GameLeaf)(leaves.nextElement());
      MoveToken move = leaf.getMoveToken();

      // If there is a move token somewhere in the leaf add it (and any
      // comment if it exists).
      //
      if( move != null )
        moves.addElement( leaf );
    }

    // If there are variations, dive through the first variation, which is
    // the entire game, without extraneous variations.
    //
    if( (trees != null) && trees.hasMoreElements() )
    {
      GameTree deepTree = (GameTree)(trees.nextElement());
      Enumeration infoTokens = deepTree.getInfoTokens();

      if( infoTokens != null )
        extractSetup( infoTokens );

      extractMoves( deepTree );
    }
  }

  /**
   * Gets the initial black and white moves that should be displayed at
   * the start of a game.
   */
  private void extractSetup( Enumeration infoTokens )
  {
    String
      whiteName = null,
      blackName = null,
      placeName = null,
      eventName = null,
      blackRank = null,
      whiteRank = null,
      komi = null,
      date = null;

    getInfoPanel().setResult( "  ?" );

    // Travel through the list of information tokens seeking out the
    // tokens that represent initial black and white moves.  This will
    // ensure that handicap stones are displayed (and other things).
    //
    while( infoTokens.hasMoreElements() )
    {
      Object o = infoTokens.nextElement();

      if( o instanceof AddBlackToken )
        setBlackStones( (AddBlackToken)o );
      else if( o instanceof AddWhiteToken )
        setWhiteStones( (AddWhiteToken)o );
      else if( o instanceof ResultToken )
        getInfoPanel().setResult( o.toString() );
      else if( o instanceof WhiteNameToken )
        whiteName = o.toString();
      else if( o instanceof BlackNameToken )
        blackName = o.toString();
      else if( o instanceof PlaceToken )
        placeName = o.toString();
      else if( o instanceof EventToken )
        eventName = o.toString();
      else if( o instanceof DateToken )
        date = o.toString();
      else if( o instanceof WhiteRankToken )
        whiteRank = o.toString();
      else if( o instanceof BlackRankToken )
        blackRank = o.toString();
      else if( o instanceof KomiToken )
        komi = o.toString();
    }

    StringBuffer info = new StringBuffer( 100 );

    if( whiteName != null )
    {
      info.append( "(W) " );
      info.append( whiteName );
    }

    if( whiteRank != null )
    {
      info.append( " " );
      info.append( whiteRank );
    }

    if( whiteName != null && blackName != null )
    {
      info.append( " vs. " );
    }

    if( blackName != null )
    {
      info.append( "(B) " );
      info.append( blackName );
    }

    if( blackRank != null )
    {
      info.append( " " );
      info.append( blackRank );
    }

    if( komi != null )
    {
      info.append( " ... " );
      info.append( " Komi: " + komi );
    }

    if( placeName != null )
    {
      info.append( " ... " );
      info.append( placeName );
    }

    if( eventName != null )
    {
      info.append( " ... " );
      info.append( eventName );
    }

    if( date != null )
    {
      info.append( " ... " );
      info.append( date );
    }

    info.append( getParam( PARAM_MARQUEE_TEXT ) );

    getInfoPanel().setMarqueeText( info.toString() );
  }
  
  private void setBlackStones( AddBlackToken bMoves )
  {
    _blackStones = bMoves;
  }

  private void setWhiteStones( AddWhiteToken wMoves )
  {
    _whiteStones = wMoves;
  }

  /**
   * Returns a list of black moves that should be set at the start of the
   * game.
   *
   * @return null - There are no initial black moves.
   */
  private Enumeration getBlackSetup()
  {
    return _blackStones == null ? null : _blackStones.getPoints();
  }

  /**
   * Returns a list of white moves that should be set at the start of the
   * game.
   *
   * @return null - There are no initial white moves.
   */
  private Enumeration getWhiteSetup()
  {
    return _whiteStones == null ? null : _whiteStones.getPoints();
  }

  /**
   * This will return the comment of the current leaf.
   *
   * @return null - The leaf of the current move has no associated comment.
   */
  protected CommentToken getCommentToken()
  {
    GameLeaf leaf = getCurrentLeaf();
    return leaf == null ? null : leaf.getCommentToken();
  }

  private void setGoGame( GoGame game )
  {
    getLeaves().setSize( 0 );
    setBlackStones( null );
    setWhiteStones( null );
    extractMoves( game.getTree() );
    showGameInfo( game );

    resetGame();
  }

  protected GameLeaf getCurrentLeaf()
  {
    try
    {
      return (GameLeaf)(getLeaves().elementAt( getMoveIndex() - 1 ));
    }
    catch( Exception ex ) { }

    return null;
  }

  /**
   * Returns the MoveToken at the current move index.
   *
   * @return null - No current move exists.
   */
  protected final MoveToken getCurrentMove()
  {
    GameLeaf leaf = getCurrentLeaf();
    return leaf == null ? null : leaf.getMoveToken();
  }

  /**
   * Returns the next move in the sequence of game moves.  Basically
   * this increases the move index by one [1] if it hasn't fallen off
   * the end of the move list.
   */
  private MoveToken getNextMove()
  {
    int moveIndex = getMoveIndex();

    // If we're out of moves, then return null.
    //
    if( moveIndex == getLeaves().size() )
    {
      return null;
    }

    setMoveIndex( moveIndex + 1 );
    return getCurrentMove();
  }

  /**
   * Displays information about the game, but does not display any
   * regular comments (the GameCommentToken is displayed if it exists).
   *
   * @param game - A game of Go that has been read and parsed into a game
   * tree.
   */
  protected void showGameInfo( GoGame game )
  {
    InfoPanel info = getInfoPanel();

    // Make sure we see what the game is doing and reflect that in the
    // captures and comments area.
    //
    getRules().deleteObserver( info );
    getRules().addObserver( info );
  }

  /**
   * Enables, or disables, the next button to indicate if there are more
   * moves left to replay in the game.
   */
  protected void hasNextMove( boolean enable )
  {
    _nextButton.setEnabled( enable );
    _endButton.setEnabled( enable );
  }

  /**
   * Enables, or disables, the previous button to indicate if the game is at
   * the very first move.
   */
  protected void hasPrevMove( boolean enable )
  {
    _backButton.setEnabled( enable );
    _startButton.setEnabled( enable );
  }

  private Vector<GameLeaf> getLeaves() { return myLeaves; }

  private void setMoveIndex( int i ) { _moveIndex = i; }
  private int getMoveIndex() { return _moveIndex; }

  private Rules getRules() { return _rules; }
  private void setRules( Rules rules ) { _rules = rules; }

  private void setToolbar( ToolbarPanel tb ) { _toolbar = tb; }
  private ToolbarPanel getToolbar() { return _toolbar; }

  private void setInfoPanel( InfoPanel panel ) { _infoPanel = panel; }
  private InfoPanel getInfoPanel() { return _infoPanel; }

  private TextArea getCommentArea()
  {
    if( this.commentArea == null )
    {
      this.commentArea = createCommentArea();
      this.commentArea.setEditable( false );
    }

    return this.commentArea;
  }

  /**
   * Returns the comment area required by this frame. Subclasses can override
   * this to use a different type of text area.
   */
  protected TextArea createCommentArea()
  {
    return new TextArea( "", 12, 15, TextArea.SCROLLBARS_VERTICAL_ONLY );
  }

  /**
   * Updates the comment area with some text, but only if it differs because
   * we don't want no steenkin' flicker.
   */
  private void setCommentArea( String text ) {
    if( !text.equals( getCommentArea().getText() ) ) {
      getCommentArea().setText( text );
      getCommentArea().setCaretPosition( 0 );
    }
  }

  /**
   * Returns the URL that will reference the given file (usually from
   * the relative path of an applet).
   *
   * @param file The file name (including directories) to append to the
   * web page URL that hosts the applet.
   */
  private URL getURL( String file )
    throws IOException, MalformedURLException {
    URL url = getCodeBase();

    return file.startsWith( "/" ) ?
      new URL( url.getProtocol(), url.getHost(), url.getPort(), file ) :
      new URL( url, file );
  }

  public void paint( java.awt.Graphics g ) {
    // Without this line, sometimes the info panel does not appear.
    //
    getInfoPanel().repaint();
  }
}

