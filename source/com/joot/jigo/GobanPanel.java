/*
 * Copyright (C) 2001 by Dave Jarvis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.
 * Online at: http://www.gnu.org/copyleft/gpl.html
 */

package com.joot.jigo;

import java.awt.Image;
import java.awt.Panel;

import java.io.IOException;

import java.net.URL;

import com.joot.jigo.Goban;
import com.joot.jigo.JiGoApplet;

/**
 * The frame that houses a Goban. This is a useful wrapper class which is
 * used by most (if not all) JiGo applets (Designer, Replayer, Guesser,
 * Life and Death, and such).
 */
public abstract class GobanPanel extends Panel
{
	private JiGoApplet applet;
	private Goban goban;

	public GobanPanel( JiGoApplet applet )
	{
		setJiGoApplet( applet );
		initGUI();
	}

  protected Image loadImage( String path )
    throws IOException, InterruptedException
  {
    return getJiGoApplet().loadImage( path );
  }

  /**
   * Returns the applet parameter defined by the given string. This will
   * never return null.
   *
   * @return The value of an applet parameter, or "" for none.
   */
  protected String getParam( String param )
  {
    String result = getJiGoApplet().getParameter( param );

    return result == null ? "" : result.trim();
  }

  protected int getParam( String param, int defaultValue )
  {
    return getJiGoApplet().getParam( param, defaultValue );
  }

  /**
   * Returns the applet parameter defined by the given string, in boolean
   * form.
   */
  protected boolean getBooleanParam( String param, boolean defaultValue )
  {
    return getJiGoApplet().getBooleanParam( param, defaultValue );
  }

  /**
   * Returns the URL where this applet is running.
   */
  protected URL getCodeBase()
  {
    return getJiGoApplet().getCodeBase();
  }

  /**
   * Returns the document where this applet is embedded.
   */
  protected URL getDocumentBase()
  {
    return getJiGoApplet().getDocumentBase();
  }

  /**
   * Returns the white stone, usually associated with a graphics image.
   */
  protected WhiteStone getWhiteStone()
  {
    return getJiGoApplet().getWhiteStone();
  }

  /**
   * Returns the black stone, usually associated with a graphics image.
   */
  protected BlackStone getBlackStone()
  {
    return getJiGoApplet().getBlackStone();
  }

  /**
   * Creates a new goban based on the size of the white stone.
   */
  protected Goban createGoban() {
    JiGoApplet applet = getJiGoApplet();

    return new Goban(
      applet.getBoardSize(), applet.getBGImage(), applet.getWhiteStone() );
  }

	/**
	 * Each subclass must know how to layout its internal components, including
	 * the Goban itself. Each subclass must handle its size, resizing
	 * ability, and packing.
	 */
	protected abstract void initGUI();

  /**
   * Returns the goban associated with this panel. This will create the goban
   * if one does not already exist; never returns null.
   */
	protected Goban getGoban() {
    if( this.goban == null ) {
      this.goban = createGoban();
    }

    return this.goban;
  }

	private JiGoApplet getJiGoApplet() { return this.applet; }
	private void setJiGoApplet( JiGoApplet applet ) { this.applet = applet; }
}

