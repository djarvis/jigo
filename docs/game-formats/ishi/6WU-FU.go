EVENT
WHITE FunGo
BLACK Wulu:5
RESULT B+15.5
BOARDSIZE 19
DATE 2000-11-06
KOMI 8.5
RULES SST
COM
Black Rank: 9k
ENDCOM
B 1 P16
W 2 D4
B 3 C16
W 4 Q4
B 5 R6
W 6 O4
B 7 S4
W 8 R3
B 9 R9
W 10 K3
B 11 Q13
W 12 R11
B 13 E16
W 14 P11
B 15 P9
W 16 N11
B 17 R16
W 18 K17
B 19 C6
W 20 C8
B 21 C3
W 22 D3
B 23 C4
W 24 D5
B 25 B6
W 26 E7
B 27 M17
W 28 G4
B 29 H16
W 30 K15
B 31 M15
W 32 K13
B 33 K10
W 34 L11
B 35 C11
W 36 K11
B 37 J10
W 38 L10
B 39 C2
W 40 D2
B 41 L8
W 42 J11
B 43 H10
W 44 H11
B 45 G10
W 46 G11
B 47 H18
W 48 S13
B 49 H7
W 50 E10
B 51 D12
W 52 F11
B 53 J14
W 54 K14
B 55 S2
W 56 R2
B 57 S3
W 58 J15
B 59 K6
W 60 L5
B 61 B5
W 62 F8
B 63 S14
W 64 R13
B 65 R14
W 66 O8
B 67 Q12
W 68 Q11
B 69 M7
W 70 N6
B 71 J8
W 72 N9
B 73 H5
W 74 H4
B 75 J5
W 76 G6
B 77 O10
W 78 O11
B 79 H15
W 80 Q7
B 81 R7
W 82 C1
B 83 B1
W 84 D1
B 85 B2
W 86 E13
B 87 D13
W 88 E14
B 89 D14
W 90 L6
B 91 L9
W 92 G8
B 93 L7
W 94 H6
B 95 Q8
W 96 O17
B 97 N10
W 98 N7
B 99 M18
W 100 O16
B 101 O15
W 102 R18
B 103 P17
W 104 P18
B 105 S17
W 106 Q17
B 107 Q16
W 108 S18
B 109 M10
W 110 M9
B 111 M11
W 112 M12
B 113 T18
W 114 O13
B 115 P14
W 116 N16
B 117 M16
W 118 N15
B 119 J19
W 120 O14
B 121 P15
W 122 P13
B 123 N14
W 124 M14
B 125 O18
W 126 N13
B 127 N18
W 128 G14
B 129 J6
W 130 H8
B 131 H9
W 132 J7
B 133 K7
W 134 K9
B 135 H14
W 136 C10
B 137 S10
W 138 B10
B 139 B11
W 140 A11
B 141 A12
W 142 A10
B 143 R12
W 144 B12
B 145 S11
W 146 F17
B 147 F16
W 148 G17
B 149 H17
W 150 B13
B 151 G16
W 152 B15
B 153 B16
W 154 Q6
B 155 D15
W 156 R5
B 157 S5
W 158 D11
B 159 R1
W 160 Q1
B 161 O9
W 162 N8
B 163 C12
W 164 S1
B 165 E12
W 166 G13
B 167 K16
W 168 J16
B 169 A16
W 170 D6
B 171 B14
W 172 A14
B 173 C14
W 174 A15
B 175 L16
W 176 J17
B 177 B8
W 178 C7
B 179 B7
W 180 B9
B 181 N17
W 182 N14
B 183 K18
W 184 P12
B 185 L4
W 186 J4
B 187 L3
W 188 M4
B 189 M6
W 190 M5
B 191 C13
W 192 A13
B 193 C15
W 194 A12
B 195 H12
W 196 H13
B 197 C18
W 198 P8
B 199 G7
W 200 G5
B 201 F3
W 202 P10
B 203 Q9
W 204 R4
B 205 G2
W 206 E11
B 207 H3
W 208 S12
B 209 Q14
W 210 J18
B 211 K2
W 212 M3
B 213 L2
W 214 J3
B 215 J2
W 216 G3
B 217 H2
W 218 F1
B 219 F2
W 220 M2
B 221 F4
W 222 L18
B 223 F6
W 224 L17
B 225 F7
W 226 Q19
B 227 R17
W 228 Q18
B 229 O19
W 230 M19
B 231 S19
W 232 L15
B 233 M1
W 234 N1
B 235 L1
W 236 K19
B 237 H19
W 238 F10
B 239 K5
W 240 G9
B 241 J9
W 242 O2
B 243 E8
W 244 F9
B 245 D9
W 246 E6
B 247 F5
W 248 F12
B 249 D8
W 250 F15
B 251 E2
W 252 E1
B 253 G1
W 254 J13
B 255 E3
W 256 S8
B 257 R8
W 258 R10
B 259 S9
W 260 G15
B 261 E4
W 262 D7
B 263 E5
W 264 C9
B 265 A8
W 266 E15
B 267 C5
W 268 M8
B 269 A9
W 270 D10
B 271 T2
W 272 Q10
B 273 T1
W 274 R1
B 275 PASS
W 276 PASS
