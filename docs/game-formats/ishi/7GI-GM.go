EVENT
WHITE GoeMate level 9
BLACK GoIntellect
RESULT B+23
BOARDSIZE 19
DATE 2000-11-06
PLACE Guiyang
KOMI 8.5
TIMELIMIT 60
RULES SST
COM
Game Name: Round 7
ENDCOM
B 1 C16
W 2 C4
B 3 R16
W 4 R4
B 5 E17
W 6 P17
B 7 R13
W 8 E3
B 9 L17
W 10 P3
B 11 N16
W 12 R18
B 13 Q10
W 14 N18
B 15 P16
W 16 Q17
B 17 Q7
W 18 C10
B 19 C13
W 20 K4
B 21 C8
W 22 E10
B 23 D9
W 24 D10
B 25 C6
W 26 E5
B 27 E9
W 28 E13
B 29 D12
W 30 E12
B 31 F9
W 32 H12
B 33 H9
W 34 S17
B 35 F4
W 36 E4
B 37 H16
W 38 G15
B 39 F11
W 40 E11
B 41 F12
W 42 F13
B 43 H15
W 44 G13
B 45 H14
W 46 K12
B 47 J11
W 48 J12
B 49 K14
W 50 G6
B 51 G10
W 52 C15
B 53 D15
W 54 K11
B 55 B15
W 56 M13
B 57 O13
W 58 M11
B 59 K9
W 60 M9
B 61 L10
W 62 M10
B 63 L7
W 64 O9
B 65 F16
W 66 O7
B 67 N6
W 68 O6
B 69 N4
W 70 M6
B 71 N7
W 72 M7
B 73 P8
W 74 N8
B 75 N5
W 76 P5
B 77 G14
W 78 L6
B 79 S16
W 80 E7
B 81 B5
W 82 B4
B 83 K7
W 84 J10
B 85 B9
W 86 J9
B 87 B10
W 88 J8
B 89 H11
W 90 M17
B 91 K10
W 92 L18
B 93 K8
W 94 H8
B 95 K17
W 96 H19
B 97 K18
W 98 K19
B 99 B12
W 100 G8
B 101 J7
W 102 R6
B 103 G9
W 104 S8
B 105 S10
W 106 O8
B 107 M14
W 108 M16
B 109 F18
W 110 Q16
B 111 O17
W 112 O18
B 113 J18
W 114 J19
B 115 Q15
W 116 P15
B 117 Q14
W 118 O16
B 119 O15
W 120 N15
B 121 P16
W 122 L4
B 123 P14
W 124 O11
B 125 N12
W 126 R11
B 127 Q11
W 128 M12
B 129 R10
W 130 C5
B 131 B6
W 132 A5
B 133 A6
W 134 A4
B 135 G18
W 136 L15
B 137 M15
W 138 N14
B 139 L14
W 140 L16
B 141 N13
W 142 P12
B 143 R17
W 144 S18
B 145 D6
W 146 E6
B 147 R8
W 148 S7
B 149 N2
W 150 K2
B 151 P2
W 152 Q2
B 153 O3
W 154 M4
B 155 Q3
W 156 R3
B 157 P4
W 158 R2
B 159 Q4
W 160 O1
B 161 P1
W 162 O5
B 163 Q5
W 164 Q6
B 165 R5
W 166 S5
B 167 O4
W 168 N1
B 169 M2
W 170 T16
B 171 S15
W 172 T15
B 173 S14
W 174 C11
B 175 T14
W 176 T17
B 177 L2
W 178 L1
B 179 M1
W 180 J2
B 181 Q1
W 182 R1
B 183 S9
W 184 K1
B 185 M5
W 186 L5
B 187 D5
W 188 B11
B 189 B7
W 190 A11
B 191 A8
W 192 C12
B 193 B13
W 194 D13
B 195 N11
W 196 N10
B 197 D4
W 198 D3
B 199 H18
W 200 G19
B 201 F19
W 202 L19
B 203 H5
W 204 Q12
B 205 R12
W 206 L3
B 207 T8
W 208 T7
B 209 T9
W 210 N17
B 211 O14
W 212 O16
B 213 N15
W 214 P15
B 215 P11
W 216 O12
B 217 P10
W 218 O10
B 219 F8
W 220 F7
B 221 H7
W 222 G7
B 223 D7
W 224 P16
B 225 K15
W 226 A12
B 227 A13
W 228 G12
B 229 G11
W 230 P9
B 231 Q9
W 232 P7
B 233 R7
W 234 Q8
B 235 M19
W 236 M18
B 237 P8
W 238 F14
B 239 F15
W 240 Q8
B 241 B3
W 242 C3
B 243 P8
W 244 B2
B 245 Q8
W 246 D14
B 247 C14
W 248 K16
B 249 J16
W 250 L13
B 251 L8
W 252 M8
B 253 J5
W 254 A10
B 255 G4
W 256 J13
B 257 J14
W 258 E15
B 259 E16
W 260 C9
B 261 E8
W 262 A9
B 263 B8
W 264 T5
B 265 M3
W 266 E14
B 267 K13
W 268 Q13
B 269 L11
W 270 L12
B 271 H13
W 272 P13
B 273 F10
W 274 H10
B 275 D8
COM
All the dame have been filled.  At this point, Whits leads by
43.5 points.
ENDCOM
W 276 PASS
B 277 H6
W 278 PASS
B 279 F3
W 280 PASS
B 281 H3
W 282 K6
B 283 H2
W 284 J4
B 285 J3
W 286 K3
B 287 E2
W 288 G2
B 289 D2
W 290 H1
B 291 F2
W 292 F5
B 293 C2
W 294 PASS
B 295 F1
W 296 G1
B 297 C1
W 298 PASS
B 299 A2
W 300 A3
B 301 B1
W 302 A1
B 303 G3
W 304 A2
B 305 J1
W 306 H1
B 307 G1
W 308 G5
B 309 H4
W 310 J1
B 311 G16
W 312 L9
B 313 S11
W 314 J6
B 315 K5
W 316 PASS
B 317 O2
W 318 PASS
B 319 PASS
W 320 PASS
