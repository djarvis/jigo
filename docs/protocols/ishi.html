<html><head>

 <title>Ishi format</title><meta name="description" content="Specification of 'Ishi' format">
 <meta name="resource-type" content="game">
 <meta name="distribution" content="world">
 <meta name="keywords" content="baduk, weiqi, boardgame, record, game record, format, Ishi"></head>
<body bgcolor="#fff0a4">

<a name="top">Various</a> formats are used for recording Go games on computers.
One of the most popular is "Ishi" format.
The following specification of it was provided by David Fotland.

<h1>Ishi format</h1>

Standard Format for Go Data Files
<p>
Specification
<br>
17 March 1990

</p><h2>Contents</h2>

<ol>
<a href="#1"><li>Standard Format</li></a>
<a href="#2"><li>Definitions</li></a>
<a href="#3"><li>General Discussion</li></a>
<a href="#4"><li>Detailed File Structure</li></a>
  <ol>
  <a href="#4.1"><li>Files</li></a>
  <a href="#4.2"><li>Events</li></a>
  <a href="#4.3"><li>Moves</li></a>
  <a href="#4.4"><li>Diagrams</li></a>
  </ol>
<a href="#5"><li>Keywords</li></a>
  <ol>
  <a href="#5.1"><li>List of Keywords</li></a>
  <a href="#5.2"><li>Header Keywords</li></a>
  <a href="#5.3"><li>Notation for Keyword Specs</li></a>
  <a href="#5.4"><li>Keyword Specifications</li></a>
  </ol>
<a href="#6"><li>General Error Conditions</li></a>
<a href="#7"><li>Sample Valid-Format Data</li></a>
</ol>

<a name="1"><h2>1. Standard Format</h2></a>

The Standard Format for Go data provides a means for storing and transferring data that represents Go games, 
Go problems, joseki dictionaries, diagrams, and other Go-related information, in a way that is reasonably 
independent of machines, operating systems and application programs.
<p>
To support the Standard fully, an application program must be able to recognize and interpret correctly all 
data signalled by the keywords listed and described in Section 5.
</p><p>
An application that only reads Standard Format files, and does not also write such files, must be able only 
to recognize and interpret the keywords of interest to it. An application that only writes Standard Format 
files can write a minimum subset of keywords, sufficient to capture the information it generates.
</p><p>
An application that both fully reads and fully writes Standard Format files must be able to recognize and 
store all data signalled by the keywords described in Section 5, and write all that data into an output 
file without adding to, losing, or modifying data in the file in any significant way. Not significant are 
white space, case outside of commentary and board marks associated with commentary, and order except where 
obvious or where specifically required.

<a name="2"><h2>2. Definitions</h2></a>

</p><dl><dt>current event</dt><dd>the event most recently defined in the data file
</dd><dt>current move</dt><dd>the most recent Black or White move in the current sequence within the current event
</dd><dt>event</dt><dd>a game, or problem, or joseki, or other similar block of conceptually-related G0 data
</dd><dt>keyword</dt><dd>a string of valid characters with no blanks; signals that data of some particular type follows
</dd><dt>sequence</dt><dd>a series of consecutive moves NOT including any variations on those moves; can be in the main 
  game/problem or follow a variation on some other move; every sequence is terminated either by an EVENT, or 
  by an ENDVAR, or by the end of the data file
</dd><dt>setup</dt><dd>non-indexed moves/stones including handicap stones, problem-situation stones, and stones 
  associated directly only to diagrams
</dd><dt>SF</dt><dd>Standard Format for Go data
</dd><dt>valid characters</dt><dd>characters in the ASCII decimal range 32 to 126 inclusive, plus tab (ASCII 9) and the
  carriage-return-line-feed pair (ASCII 13 and 10 consecutively)
</dd><dt>white space</dt><dd>blank spaces and tabs (ASCII decimal 32 and 9 resp.)
</dd></dl>

<a name="3"><h2>3. General Discussion</h2></a>

Data files in Standard Format (SF) are line-oriented, keyword-driven, and white-space-delimited. That is:
<ol>
<li>The main unit of reading/writing in an SF file is the line: a series of valid characters terminated 
  by the ASCII carriage-return-line-feed pair.
</li><li>Each line in an SF file either begins after white space with a keyword, or is part of a block of lines 
  set off by the keyword pair COM/ENDCOM (commentary) or by the pair  USER/ENDUSER (user-defined data).
</li><li>Any pair of data fields - keywords, plus other fields that are required to follow them or can optionally 
  follow them - can be separated from each other by any amount of white space (spaces and/or tabs) but by at 
  least one space or tab. A keyword can have any number of spaces or tabs before it on its file line.
</li></ol>
Empty lines can appear anywhere in an SF file. They have no bearing on the data in the file except within 
commentary blocks, wherein the application decides how to handle them.
<p>
An SF file is not required to have any data in it: a completely empty file is a valid-format file.
</p><p>
Valid characters in an SF file are those in the ASCII range 32 to 126 inclusive, plus the tab character 
(ASCII 9) and the carriage-return-line-feed pair (ASCII 13 and 10 consecutively). All other characters can 
be ignored by applications.
</p><p>
Case is irrelevant in SF files except within commentary blocks and for explicit characters that represent 
board marks (see the section on Notation). Outside of board marks and commentary, all of upper case, lower 
case, and mixed case are accepted and treated as equivalent for keywords, move locations, special-character 
designations, etc.
</p><p>
No assumptions are made in SF files about evenness or oddness for move numbers in relation to player's color. 
Black's moves can be even-numbered and White's can be odd-numbered, or vice-versa for both colors. No 
assumptions are made about alternation of play between Black and White.

<a name="4"><h2>4. Detailed File Structure</h2></a>

<a name="4.1"><h3>4.1  Files</h3></a>

Each Standard Format (SF) file contains one or more events. An event can be a game, a problem, a joseki, 
a special diagram, or some other block of Go-related data. Each event occupies a distinct, contiguous block 
of lines in the file.
</p><p>
An empty SF file consists only of the default implicit event with no attached data. A file with no events 
specifically defined via the EVENT keyword, but with moves or other data, has an implicit first event to 
which the data is associated.
</p><p>
An SF file looks like this:
</p><pre>             Event 1
             Event 2
             .
             .
             .
             Event n
</pre>

<a name="4.2"><h3>4.2  Events</h3></a>

Each event has a single, square board size between 2x2 and 19x19 inclusive, with default 19x19 unless otherwise 
specified. Each event contains zero or more of the following (but no more than one tree of moves and diagrams):
<ul>
<li>remarks in the file that apply to the event and are typically seen only by a person viewing the file directly;
</li><li>header records such as player names and ranks, date, source, place, handicap, komi, result, etc.;
</li><li>unnumbered Black and White setup stones, for example handicap stones or problem-situation stones;
</li><li>commentary that applies to the event as a whole;
</li><li>blocks of user-defined data that apply to the event as a whole;
</li><li>board marks - letters or special characters or shapes that will be displayed on the empty board at the start 
  of the event, before any setup stones or moves, and only then;
</li><li>a "tree" of numbered Black and White moves that represent the flow of play in the event, plus any number of 
  diagrams interspersed with the moves; the moves and move-related diagrams in an event follow all other data that 
  applies to the event as a whole.
</li></ul>

An event's structure in a file looks like this:
<pre>        Remarks
        Event definition (EVENT keyword)
        Event header data
        Event commentary and marks
        Event setup stones
        Event user data
        Tree of moves and diagrams
</pre>

The remarks if any precede the event definition in the file. The event definition line precedes all the remaining 
lines in the event. All header lines, commentary blocks, marks, setup stones, and blocks of user data may occur in 
any order among themselves, subject to those keywords' own rules, but all must precede the tree of moves and 
diagrams if any.

<a name="4.3"><h3>4.3  Moves</h3></a>

Each move in an event is defined by a player color, a move number that is greater than zero, and the board position 
for the move or  an indication that the move was a pass or tenuki. Each move is either a move in the main flow of 
play, or a variation on such a move, or a move within a sequence that follows a variation upon another move. 
Variations may be nested to any "depth": moves within variation sequences may have "deeper" variations on them.
<p>
A move is defined either as a setup stone (SETUP keyword) or as a regularly-played Black or White play on the board 
(B or W keyword respectively).
</p><p>
A move has associated with it zero or more of the following:
</p><ul>
<li>remarks in the file that apply to the move and are typically seen only by a person viewing the file directly;
</li><li>a list of the prisoners taken when this move is played;
</li><li>commentary that applies to the move;
</li><li>board marks - letters or special characters or shapes that normally relate to the commentary and will be 
  displayed on the board when the move itself becomes displayed as the last of the current sequence, and only then;
</li><li>blocks of user-defined data that apply to the move.
</li></ul>

A move's structure in a file looks like this:
<pre>        Remarks
        Move definition (SETUP or B or W keyword)
        Prisoners
        Marks
        Commentary
        User data
        Variations on the move
</pre>
Remarks if any precede the move's definition. Prisoner lines, commentary blocks, marks and user data all follow the 
move definition line and may occur in any order among themselves. Variations on the move must follow all other move data.

<a name="4.4"><h3>4.4  Diagrams</h3></a>

Each diagram occupies a specific place in the tree of moves, variations and diagrams associated with an event.
<p>
A diagram is defined by a board region to be displayed when the diagram is shown. Unless otherwise specified, 
the region is the same as the parent event's board size. The region can be any rectangular subarea of any board 
between 2x2 and 19x19 in size;
</p><p>
Each diagram contains zero or more of the following:
</p><ul>
<li>remarks in the file that apply to the diagram and are typically seen only by a person viewing the file directly;
</li><li>a list of Black and/or White setup stones that will be displayed only when the diagram is displayed; these stones
  have no bearing on the flow of play that precedes or follows the diagram;
</li><li>a list of stones/moves to hide when the diagram is displayed; i.e., to remove temporarily from the display
  when the diagram is shown and to restore to view when the diagram is taken down;
</li><li>commentary that applies to the diagram;
</li><li>board marks - letters or special characters or shapes that normally relate to the commentary and will be displayed 
  on the board when the diagram itself becomes displayed, and only then; these marks can apply to regular moves in the
  sequence and/or to the diagram's setup stones;
</li><li>blocks of user-defined data that apply to the diagram.
</li></ul>

A diagram's structure in a file looks like this:
<pre>        Remarks
        Diagram definition (DIAGRAM keyword)
        Setup stones
        Hide moves/stones
        Marks
        Commentary
        User data
</pre>

Remarks if any precede the diagram's definition. Setup lines, hide-stone lines, commentary blocks, marks and user 
data all follow the diagram definition line and may occur in any order among themselves.

<a name="5"><h2>5. Keywords</h2></a>

<a name="5.1"><h3>5.1  List of Keywords</h3></a>
<pre>  Event keyword

     EVENT      begin next event: game, problem, joseki, etc.


  Header keywords per EVENT

     ANALYSIS   person(s) who provided the commentary
     BLACK      Black player's name, rank etc.
     DATE       date(s) on which game was played
     HANDICAP   handicap count (informational only)
     KOMI       komi
     PLACE      location(s) where game was played
     RECORDER   person(s) who recorded the data
     RESULT     result of the game
     RULES      Japanese (the default) or Chinese
     SOURCE     source for data: book, journal, etc.
     TIMELIMIT  time limits for one or both players
     WHITE      White player's name, rank etc.

  Data keywords per EVENT

     B          next Black move(s)
     BOARDSIZE  size of square board for event
     COM        begin a commentary block
     DIAGRAM    begin definition of a diagram
     ENDCOM     end a commentary block
     ENDUSER    end a user-defined block
     ENDVAR     end a variation sequence
     HIDE       stones/moves to hide in a diagram
     MARK       board markers related to commentary
     PRISONER   prisoners taken by current move
     REMARK     file-writer's remark on event, move, diagram
     SETUP      setup B/W stones for an event or diagram
     UNMARK     remove some previously-defined MARKs
     USER       begin a user-defined block
     VAR        begin a variation sequence
     W          next White move(s)
</pre>

<a name="5.2"><h3>5.2  Header Keywords</h3></a>

Header keywords represent informational data about the current event. Each header line consists of a keyword 
optionally followed by free-form text that represents the header data. This text may contain any amount of 
white space.
<p>
Each header keyword can be repeated within an event. In the case of generally textual data such as source, 
place, analysis, etc., the total data is the sum from all the repeated lines. In the case of normally once-only 
data such as Black player, komi, etc., the last line encountered should be taken as the correct data. An
application may treat multiple occurences of once-only data lines as an error condition but is not required to 
do so.
</p><p>
A note on handicaps: Exact board locations for a given number of handicap stones are usually ambiguous. For this 
reason, handicap stones are placed in specific locations using the SETUP keyword. HANDICAP keyword data is 
informational and does not necessarily have to match the actual setup.
</p><p>
Below are valid examples of header keywords.
</p><pre>     Analysis Jim Kerwin, professional 1-dan
     ANALYSIS     Kato 9 Dan
     BLACK   Honinbo Shusaku
     black     Alan Chen (9 years old)
     Black
     Date 8 March 1990
     Date      3-21-48
     date November 21-24, 1847
     DATE  Dec 9, 1938 and Mar 3-4, 1939
     Date    June 3 and 7, 1958
     HANDICAP 0
     Handicap 7
     komi 5
     Komi  3 1/2
     KOMI 4.5
     Place
     PLACE  Kyoto (April) and Tokyo (May)
     place    US Go Congress, Denver
     Recorder Ishi Press
     RECORDER     Phil Straus (playing Black)
     RESULT White wins by resignation
     result  Black by 3
     Result Black resigns at move 186
     RULES Japanese
     Rules CHINESE
     Rules
     SOURCE  "Invincible: The Games of Shusaku"
     SOURCE  John Powers, Kiseido Publishing Co., Tokyo 1982
     Timelimit    1 hour
     WHITE James Redmond 5-dan (made 6-dan after this game)
</pre>

<a name="5.3"><h3>5.3. Notation for Keyword Specifications</h3></a>
<pre>     
     &lt;&gt;      nonrepeatable required data field with no
             embedded white space

     &lt;&gt;*     repeatable required data field, each occurence of
             which contains no embedded white space

     []      nonrepeatable optional data field that may have
             embedded white space if so indicated

     []*     repeatable optional data field, each occurence of
             which may contain white space if so indicated

     n       positive integer

     n-n     sequence of positive integers increasing from the
             first number n to second, inclusive: e.g., 21-29

     loc     board location of form cn, where the character c,
             in the (case-insensitive) set a b c ... not
             including i, represents the column starting from
             the left, and the number n represents the row
             starting from the bottom; e.g. A3 r17 F6

     cc      one or two characters in the standard ASCII range
             33 decimal to 126 decimal inclusive; the blank
             character ASCII 32 is not included; if the first
             character is \ then c, d, s or t as the second
             character indicates a special character (see next
             notation)

     \C      special marker character, under DOS outside the
             ASCII range 32-126; case-insensitive; one of

                \d       diamond    ASCII 4
                \t       triangle   ASCII 30
                \c       check mark ASCII 251
                \s       square     ASCII 9


     |       "or"; precedes next in a series of alternate
             entries in a data field; e.g. &lt;#n | loc&gt;
</pre>

<a name="5.4"><h3>5.4  Specifications for Non-Header Keywords</h3></a>
<a href="#B">B</a> |
<a href="#BOARDSIZE">BOARDSIZE</a> |
<a href="#COM">COM</a> |
<a href="#ENDCOM">ENDCOM</a> |
<a href="#DIAGRAM">DIAGRAM</a> |
<a href="#ENDUSER">ENDUSER</a> |
<a href="#ENDVAR">ENDVAR</a> |
<a href="#EVENT">EVENT</a> |
<a href="#HIDE">HIDE</a> |
<a href="#MARK">MARK</a> |
<a href="#PRISONER">PRISONER</a> |
<a href="#REMARK">REMARK</a> |
<a href="#SETUP">SETUP</a> |
<a href="#UNMARK">UNMARK</a> |
<a href="#USER">USER</a> |
<a href="#VAR">VAR</a> |
<a href="#W">W</a> 
<hr>
<pre><a name="B">B &lt;n&gt; <loc |="" pass=""></loc></a>

     Next move by Black.

     The move number &lt;n&gt; must be greater than 0.

     Consecutive B lines in a file are not required to have
     consecutive move numbers, though applications may choose to
     treat nonconsecutive numbers as an error condition.

     Examples
        B 17 q3
        B 21 pass
</pre>
<hr>
<pre><a name="BOARDSIZE">BOARDSIZE &lt;n&gt;</a>

     Size of current event's square board, between 2 and 19
     inclusive. If no boardsize is indicated for an event, 19x19
     is assumed.

     Examples
        BOARDSIZE 2
        BOARDSIZE 9
        BOARDSIZE 13
</pre>
<hr>
<pre><a name="COM">COM [title data]</a>

     Begin block of commentary on current event, move or diagram.

     If there is no current move or previously-defined setup
     stones in the current event, apply commentary to the event
     as a whole.

     All other data after the COM keyword on the line is treated
     as "title" data for the comment that follows. Title data may
     contain white space.

     The next keyword line in an SF file after a COM line must be
     an <a href="#ENDCOM">ENDCOM</a> line. COM blocks cannot be nested. All lines
     between a COM and an ENDCOM are treated as commentary text.

     Examples

        COM
        COM  JK's notes \c + !
        COM The famous "ear-reddening" move
</pre>
<hr>
<pre><a name="ENDCOM">ENDCOM</a>

     End block of commentary.

     A line starting with ENDCOM must follow each <a href="#COM">COM</a> line. All
     lines between the COM and ENDCOM are treated as commentary
     text.

     Example

        ENDCOM
</pre>
<hr>
<pre><a name="DIAGRAM">DIAGRAM [&lt;nXn&gt; | &lt;loc loc&gt;]</a>

     Begin definition of a diagram and specify its board region.

     The region of a diagram is any rectangular subarea of any
     board of size 2x2 through 19x19. If no region is indicated,
     a diagram occupies the full BOARDSIZE of the current event.
     A full-board diagram of a certain size is given by nXn where
     n is in the range 2 to 19.

     If a diagram's size is different from the current event's
     BOARDSIZE, it is up to the application to decide whether or
     not to clear the board's display, or to put up a separate
     window with the diagram, or whatever.

     Diagram title and commentary data is contained in COM/ENDCOM
     blocks as with regular B and W moves.

     See also the <a href="#HIDE">HIDE</a> keyword discussion.

     Examples
        DIAGRAM          (use current event boardsize)
        DIAGRAM 19x19    (full 19x19 board)
        DIAGRAM a19 t1   (full 19x19 board)
        DIAGRAM 9X9      (full 9x9 board)
        DIAGRAM a1 j9    (9x9 lower-left corner of board)
        DIAGRAM b5 j12
        DIAGRAM b12 j5   (same region as previous example)
</pre>
<hr>
<pre><a name="ENDUSER">ENDUSER</a>

     End a block of user-defined data.

     Must be preceded in the file by a <a href="#USER">USER</a> line.

     Example

        ENDUSER
</pre>
<hr>
<pre><a name="ENDVAR">ENDVAR</a>

     End a variation sequence.

     Must be preceded in the file by a <a href="#VAR">VAR</a> line.

     Example

        ENDVAR
</pre>
<hr>
<pre><a name="EVENT">EVENT [event title]</a>

     Start the next game/problem/joseki/diagram/etc.

     An EVENT line terminates the current sequence if any and
     terminates the current event if any.

     All other data on the line after the EVENT keyword is
     treated as title commentary on the event. Such title data
     may contain white space.

     Examples

        EVENT Problem 6
        EVENT Joseki for 3-4 point attacked from 5-4 point
        EVENT Honinbo 1989 Final
</pre>
<hr>
<pre><a name="HIDE">HIDE [loc | #n | #n-n]*</a>

     For a DIAGRAM, temporarily remove from display the indicated
     stones, which are specified either by board location or by
     move numbers in order to support removal of regularly played
     moves as well as handicap/setup stones.

     See the syntax discussion under <a href="#MARK">MARK</a>.

     Examples
        HIDE #21
        HIDE #3-9 q9 R11 #17
</pre>
<hr>
<pre><a name="MARK">MARK [ALL | loc | #n | #n-n | \C@loc | \C@#n | \C@#n-n |</a>
                           cc@loc | cc@#n | cc@#n-n]*

     Board marks (highlights) attached to the current event,
     move or diagram. If no move or diagram exists in the current
     event then the marks apply to the empty board.
     Both kinds of indicator for move locations - move number or
     stone location - are supported because handicap/setup stones
     can be marked and because move numbers can be ambiguous
     within a variation sequence: does the number apply to the
     move within the variation, or to a move with the same number
     in a higher-level variation or in the main game?

     Board marks are instructions to a program to display or 
     undisplay special letters or shapes on the board, usually 
     in conjunction with commentary. The sequence of mark 
     keylines is important: a read-write application must 
     preserve the sequence in which MARKs and UNMARKs are 
     defined. For example, the below lines in the order 
     presented cause moves 1-5, 11, and 21-33 to have their 
     move numbers displayed:

             MARK #1-33
             UNMARK #6-20
             MARK #11

     A different order would result in a different display.


  Data fields:

     ALL
        show move numbers on all visible numbered stones (does
            not show numbers on setup or handicap stones)
        example  MARK All

     loc
        show move number on stone at this location
        example  MARK Q8

     #n
        show number on most recent move with this number
        example  MARK #21 #233

     #n-n
        show move numbers on a range of moves
        example  MARK #21-28  #221-233

     \C@#n
        special character on specified move number
        example  MARK \s@#221 \t@#18

     \C@#n-n
        special character on all of specified range of moves
        example  MARK \t@#21-30

     \C@loc
        special character at board location; can be at a stone
        example  MARK \t@Q5 \S@R1 \T@m3

     cc@loc
        one or two characters at a location; case is retained;
           can be at a stone
        example  MARK a@R3 B0@o9 +@A6 X@r7

     cc@#n
        place one or two characters on a numbered move
        example  MARK    aa@#33      X@#102

     cc@#n-n

        place one or two characters on a range of moves
        example  MARK    x@#81-90  ..@#320-323
</pre>
<hr>
<pre><a name="PRISONER">PRISONER [#n | loc]*</a>

     Prisoner(s) taken by current move.

     Both forms of move/stone indicator are supported - move
     index and board location - because handicap and setup stones
     can be captured.

     Examples
        PRISONER #22 Q9 F3 #28
        PRISONER #25 #3 #11 #33
</pre>
<hr>
<pre><a name="REMARK">REMARK [free-form text]</a>

     A remark in a file is normally meant for view only by a
     person who is directly reading or editing the file.

     Each remark applies to the next <a href="#EVENT">EVENT</a>, <a href="#SETUP">SETUP</a>, <a href="#B">B</a> or <a href="#W">W</a> move,
     or <a href="#DIAGRAM">DIAGRAM</a> in the file. Each of these keyword types can have
     any number of remarks preceding it in a file.

     Examples
        REMARK file test.go
        REMARK Jack - are the next 3 lines OK?
        REMARK Diagram 3 of 9
</pre>
<hr>
<pre><a name="SETUP">SETUP [&lt;B | W&gt; [loc]*]*</a>

     Place unnumbered setup stones for an event or diagram,
     including handicap stones for a game. In general, all setup
     stones in an event or diagram are considered one generalized
     "move": they are all displayed or undisplayed as a unit in a
     single step.

     An event or diagram can have any number of SETUP lines.
     Either <a href="#B">B</a> or <a href="#W">W</a> must precede any list of setup stones to
     indicate their color. Multiple sets of different-colored
     setup stones can follow the SETUP keyword on a line.

     Only the first SETUP line in a series of setup lines can
     have an associated <a href="#REMARK">REMARK</a>. Only the last of a series of
     SETUP lines can have <a href="#MARK">MARK</a>s, <a href="#UNMARK">UNMARK</a>s, commentary or user-
     defined data.

     Examples
        SETUP B d4 q4 d16 q16
        SETUP W a6 B a7 R11 J9 W b5 B M4
</pre>
<hr>
<pre><a name="UNMARK">UNMARK [ALL | loc | #n | #n-n]*</a>

     Board marks attached to the current event, move or diagram.
     If no move or diagram exists in the current event then the
     unmarks apply to the empty board. An UNMARK removes a MARK
     previously specified. A series of MARK and UNMARK keywords
     can be used to build Boolean operations on the marks to be
     displayed.

     See the discussion under <a href="#MARK">MARK</a> for details.

     Examples
        UNMARK All
        UNMARK Q8 #21 r3 #39-43
</pre>
<hr>
<pre><a name="USER">USER [user-defined data of any format whatsoever]</a>

     Begin a block of user-defined data.

     This line, and all lines after it and up to and including
     the next <a href="#ENDUSER">ENDUSER</a> line, define a block of user data.

     USER/ENDUSER blocks may not be nested

     Examples
        USER
        USER      Ishi inventory
        USER      Printed diagram #4 of 8
</pre>
<hr>
<pre><a name="VAR">VAR [cc]</a>

     Begin a variation sequence.

     The variation in question applies to the current move, 
     which must be a <a href="#B">B</a> or <a href="#W">W</a> keyword-prefixed move.

     The optional one- or two-character marker may be placed 
     by a display application at the variation's board location 
     as a visual cue, or the character(s) may represent 
     sequencing numbers or letters with no special display 
     significance.

     Example
        VAR
        VAR b
        VAR 10
</pre>
<hr>
<pre><a name="W">W &lt;n&gt; &lt;loc | PASS&gt;</a>

     Next move by White.

     The move number &lt;n&gt; must be greater than 0.

     See the discussion under keyword <a href="#B">B</a> for more details.

     Examples
        W 38 R19
        w  99      pass
</pre>

<a name="6"><h2>6. General Error Conditions</h2></a>

A keyword line that is missing required data is in error.
<p>
Duplicate or out-of-sequence move numbers within a sequence usually implies an error condition. For example, "B 12 Q3" 
followed by "B 11 R5" or "B 12 R8" probably implies an error. In certain cases an application may choose to accept 
out-of-sequence moves. In all cases, however, duplicate moves indicate errors.
</p><p>
In general, a missing move index implies an error condition. For example, if Black 19 is followed by B 21, a White move 
is probably missing. This may be an acceptable condition for certain application; it is up to the application to decide.
</p><p>
Certain keyword fields may have, after their required and/or optional data, further data on the line. Most applications 
will ignore such extra data. It is NOT necessarily an error condition for extra data to be present. For example, the 
ENDVAR keyword has no further required or optional data; a line such as "ENDVAR back to main game" should not generate 
an error, but a program that reads a file with such a line is not expected to write the extra data to an output file. 
If the specification for a keyword would cause extra data to be parsed, and if that data has incorrect format, than an 
error condition would however be indicated. For example, the line "B 22 Q3 R7" should be seen as an error.

<a name="7"><h2>7. Sample Valid-Format Data</h2></a>

The below data is valid-format with respect to the Standard. Nearly all of the keywords are represented in this data, 
and most data-field variations for the keywords are represented.
</p><pre>Remark         testspec.go
Remark         Three-event SF file for illustration and testing
event
B 1 R16
W 2 D17
B 3 Q3
W 4 P17
B 5 C4
W 6 E3
B 7 Q5
W 8 C6
B 9 D6
W 10 D7
B 11 E6
W 12 E7
B 13 E4
W 14 D3
B 15 C7
W 16 C8
B 17 B7
W 18 F7
B 19 B8
W 20 F4
B 21 F6
W 22 C9
B 23 G7
W 24 G8
B 25 H6
W 26 J3
B 27 E9
W 28 B9
B 29 F8
W 30 D8
B 31 G9
W 32 H8
B 33 H9
W 34 J8
B 35 J9
W 36 K8
B 37 D11
W 38 C12
B 39 C11
W 40 B11
B 41 B12
W 42 B13
B 43 D12
W 44 A12
PRISONER  B12
B 45 C13
W 46 C14
B 47 B12
PRISONER C12
W 48 F10
B 49 F9
W 50 C12
PRISONER  B12
B 51 D13
W 52 B6
B 53 B5
W 54 A8
B 55 C5
W 56 A7
PRISONER  B8  #17  C7
B 57 B12
PRISONER  C12
W 58 G5


Event Yose Problem 7
Source Page 164 of "Basic Techniques of Go"
Source By Haruyama and Nagahara
Source Ishi Press, Berkeley-Tokyo, 1969
Black Haruyama 6-dan
White Nagahara 4-dan
Komi   0
Boardsize 10
Timelimit 30 minutes each
Handicap 1
Result Black wins by 1
Rules Japanese
Analysis The authors
Recorder W. Lobb
Date 8 March 1990
Place Concord MA
Setup w e10 f9 j9 k9 b8 f8 g8 h8 k8 b7 c6 h6 k6
Setup b B9 C9 D9 E9 A8 C8 E8 J8 D7 E7 F7 G7 H7 J7 K7 D6 J6
Setup w C5 G5 H5 J5 K5 B4 C4 F4 G4 a3 b3 e3 f3 a2 c2 d2 e2 b2
Setup b d5 f5 d4 e4 h4 j4 k4 c3 d3 g3 j3 f2 g2 h2
Com Yose Problem 7.
How should Black play so as to win by one point?
Endcom
B 1 h9
W 2 h10
b 3 f10
Mark #1-2
com
Black 1 and 3 are kiri (cut) and horikomi tesuji. The meaning of
these moves will become clear in a moment.
endcom
w 4 g10
PRISONER #3
b 5 b6
COM \d
Black 5 is the atekomi tesuji and is worth 1 point with
sente for Black.
ENDCOM
w 6 a6
b 7 c7
w 8 b5
PRISONER b6
b 9 f1
w 10 f6
b 11 E5
w 12 G6
b 13 E1
w 14 D1
b 15 D10
w 16   g9
Prisoner h9
b 17   a9
w 18   a7
b 19   f10
prisoner E10
w 20   j2
b 21   k2
w 22   E10
prisoner #19
b 23   A5
w 24   A4
prisoner #23
b 25   F10
PRISONer E10
User Print-Diagram 1 of 10
   MARK All
   numbase -22
   partial c1 k8
   show 12 28
   Ko information goes here
Enduser
w 26   J1
b 27   H1
w 28   E10
Prisoner #25
b 29   j10
w 30 k10
prisoner #29
b 31 f10
PRISONER e10
COM
After Black takes ko at 31, White has no more ko threats. Black
wins by 1 point, having 15 points against White's 14.
ENDCOM

EVENT   Problem 1 from Ishigure
COM
Adapted from page 111 of "In the Beginning"
By Ikuro Ishigure, Ishi Press 1973
ENDCOM
BOARDSIZE 19
SETUP B R16 C4 R5 P17 O5  W E17 Q3 E3 Q8 P5 P4
SETUP W O6 N6 Q10  B P6 Q6 O7  W O4 N4 L3  B P8 O3 N5 M5
COM Problem 1
Black to play.
ENDCOM
B 1 Q12
MARK 10@R12 8@Q13 8@R13 8@C17 8@C16 8@D15 \S@q10
MARK \t@Q8 8@L5 7@P9 7@J17 7@J16 7@K16
COM \D \c
Black 1 is the best move and scores 10.

The focal point of the game as it now stands is the unsettled
position on the right side. Black should make a pincer play
either at 1 or at '10', attacking the white stones marked \T and
\s while forming territory in front of his shimari.

If you chose to play one line higher, at one of the points
marked 8 in the upper right, you can take credit for finding the
right direction of play, but you are being too timid.
ENDCOM
   VAR 8
   B 1 M7
   COM         Score: 8
   ENDCOM
   W 2 R13
   MARK m7
   COM
   Black 1, capturing two stones and tying Black's forces
   together, is an important point, but White 2 makes White's
   position safe, too, and robs Black of territory on the right
   side.
   ENDCOM
      VAR
      W 2 p7
      MARK M7
      COM
      This is not good for White.
      ENDCOM
      USER Some user data here.
      More user data.
      Last line of user data.
      ENDUSER
      ENDVAR
   ENDVAR

   VAR 8
   B 1 D5
   COM Score: 8
   ENDCOM
   W 2 M6
   B 3 L5
   W 4 L6
   B 5 K5
   W 6 L8
   MARK ALL
   UNMARK #6
   MARK \t@L8
   COM
   If Black plays on the left side, 1 looks best, but then White
   will play 2, 4 and \t, and be one move ahead on the right side
   of where he comes out in the main sequence.
   ENDCOM
   ENDVAR

   VAR 7
   B 1 Q9
   COM Score: 7
   ENDCOM
   W 2 R9
   B 3 P9
   W 4 r13
   MARK #1 r9 p9 X@r10
   COM
   If Black plays at 1 and 3, White will extend to 4, with
   nothing to fear from Black's cutting at X.
   ENDCOM
   ENDVAR

W 2 M6
   VAR
   W 2 P9
   B 3 L5
   MARK p9
   COM
   If White answers Black 1 directly by playing, say, 2, Black
   will capture at 3. White, his three stones on the right side
   sandwiched between two strong black positions, faces a dreary
   prospect. White cannot play this way; the fight in the main
   sequence is inevitable.
   ENDCOM
   ENDVAR
B 3 L5
W 4 L6
B 5 K5
W 6 L8
MARK all a@k6
COM
After Black 1, White should push out into the center with 2 and
4, then jump to 6 (or, if he prefers, keep pushing at 'a'). This
starts a four-way fight between two weak black groups and two
weak white ones. It will be difficult, but Black has already
staked out his claim on the best ground by playing 1.
ENDCOM
  VAR
  DIAGRAM
  HIDE q12 r16 p17 e17 c4 e3 l5 k5 l6 m6
  MARK 1@q8 2@o5 3@p5 4@p6 5@p4 6@q6 7@o6 8@o7 9@n6 10@p8
  MARK 11@q10 12@o3 13@o4 14@n5 15@n4 16@m5 17@l3
  COM
  For your reference, the position in the lower right
  corner arose from this pincer joseki.
  ENDCOM
  ENDVAR
</pre>

<hr>

This page is part of the 
<font size="+2">B</font>ritish <font size="+2">G</font>o <font size="+2">A</font>ssociation
<a href="http://www.britgo.org/index.html">web site.</a>
However the material on it was supplied by David Fotland.
<p>
<basefont size="-2">
  <a href="#top">Back to top</a>


</p></body></html>